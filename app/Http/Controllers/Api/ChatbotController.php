<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Chatbot;
use App\Models\SuggestedMsg;
use Illuminate\Http\Request;

class ChatbotController extends Controller
{
    public function config($id)
    {
        $chatbot = Chatbot::with('meta')->where('chatbot_id', $id)->first();
        if(!$chatbot)
        {
            return abort(404);
        }

        $messages = SuggestedMsg::where([
            ['chatbot_id', $chatbot->id],
            ['status', 'active']
        ])->select('id', 'title', 'prompt')->get();

        $meta = json_decode($chatbot->meta->data, true);
        $meta['buttonIcon'] = asset('/').'icon/'.$chatbot->icon;
        $meta['messages'] = $messages;
        return response()->json($meta);
    }
}
