<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Plan;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Inertia\Inertia;
use Illuminate\Support\Facades\Config;


class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $global = json_decode(Option::where('key', 'global_settings')->first()->value);
        $privacy = Option::where('key', 'privacy')->first();
        return Inertia::render('Auth/Register', [
            'logo' => $settings->site_logo,
            'global' => $global,
            'isShowAgree' => json_decode($privacy->value)->showLoginPage
        ]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:'.User::class,
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $free_plan = Plan::where('price', 0)->first();
        if($free_plan)
        {
            $plan_id = $free_plan->id;

            $info = json_decode($free_plan->data);

            $data = [
                'word_limit' => $info->word_limit,
                'templates_limit' => $info->templates_limit,
                'chatbot_limit' => $info->chatbot_limit,
                'ai_templates' => $info->ai_templates,
                'ai_chatbot' => $info->ai_chatbot,
                'ai_images' => $info->ai_images,
                'access_to_gpt3' => $info->access_to_gpt3,
                'accept_to_gpt4' => $info->accept_to_gpt4,
                'ai_speech_to_text' => $info->ai_speech_to_text,
                'ai_code' => $info->ai_code,
                'image_limit' => $info->image_limit,
                'speech_to_text_limit' => $info->speech_to_text_limit,
                'audio_file_size' => $info->audio_file_size,
                'trail_days' => $info->trail_days,
                'accept_trail' => $free_plan->accept_trail,
                'use_word_limit' => 0,
                'use_image_limit' => 0,
                'use_speech_to_text_limit' => 0
            ];

            if($free_plan->duration_type == 'monthly')
            {
                $date = 30;
            }else{
                $date = 365;
            }

            if($free_plan->accept_trail)
            {
                $date = $info->trail_days;
            }

            $date = Carbon::now()->addDays($date);

        }else {
            $plan_id = null;
            $data = [
                'word_limit' => 0,
                'templates_limit' => 0,
                'chatbot_limit' => 0,
                'ai_templates' => 0,
                'ai_chatbot' => 0,
                'ai_images' => 0,
                'access_to_gpt3' => 1,
                'accept_to_gpt4' => 0,
                'ai_speech_to_text' => 0,
                'ai_code' => 0,
                'image_limit' => 0,
                'speech_to_text_limit' => 0,
                'audio_file_size' => 0,
                'trail_days' => 0,
                'accept_trail' => 0,
                'use_word_limit' => 0,
                'use_image_limit' => 0,
                'use_speech_to_text_limit' => 0
            ];
            $date = null;
        }

        $user = new User();
        $user->user_type = 'user';
        $user->plan_id = $plan_id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->data = json_encode($data);
        $user->will_expire = $date;
        $user->save();

        event(new Registered($user));

        Auth::login($user);

        if (Config::get('auth.verification')) {
            $user->sendEmailVerificationNotification();
            return redirect('/verify-email');
        }

        return redirect(RouteServiceProvider::HOME);
    }
}
