<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Conversation;
use App\Models\Option;
use App\Models\Template;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;
use Orhanerday\OpenAi\OpenAi;

class AiChatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $info = json_decode($user->data);

        if ($info->ai_chatbot != 1) {
            return abort(404);
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        if ($info->chatbot_limit == -1) {
            $aiChats = Template::where([
                ['type', 'AiChat'],
                ['status', 'approved']
            ])->latest()->paginate(20);
        } else {
            $aiChats['data'] = Template::where([
                ['type', 'AiChat'],
                ['status', 'approved']
            ])->latest()->take($info->chatbot_limit)->get();
        }

        return Inertia::render('User/Chats', [
            'logo' => $settings->site_logo,
            'aiChats' => $aiChats
        ]);
    }

    public function newConversation(Request $request)
    {
        $template = Template::findOrFail($request->template_id);
        $info = json_decode($template->data);

        $data = [
            [
                'message' => $info->message,
                'type' => 'bot',
                'created_at' => Carbon::now()
            ]
        ];

        $conversation = new Conversation();
        $conversation->user_id = Auth::User()->id;
        $conversation->template_id = $request->template_id;
        $conversation->title = $template->title;
        $conversation->data = json_encode($data);
        $conversation->save();

        return response()->json($conversation);
    }

    public function sendMessage(Request $request)
    {
        $conversation = Conversation::findOrFail($request->conversation_id);
        $info = json_decode($conversation->data);

        $user = Auth::User();
        $userInfo = json_decode($user->data);

        $newMessage = [
            'message' => $request->content,
            'type' => 'bot',
            'created_at' => Carbon::now()
        ];

        $info[] = $newMessage;

        $conversation->data = json_encode($info);
        $conversation->save();

        $token_array = gpt_encode($request->content);
        $total_tokens = count($token_array);

        $userInfo->use_word_limit = ($userInfo->use_word_limit + $total_tokens);
        $user->data = json_encode($userInfo);
        $user->save();

        return response()->json('Success');
    }

    public function messageGets(Request $request)
    {
        $conversation = Conversation::select('id', 'title', 'data')->findOrFail($request->id);
        return response()->json($conversation);
    }

    public function conversationRename(Request $request)
    {
        $conversation = Conversation::findOrFail($request->id);
        $conversation->title = $request->title;
        $conversation->save();

        return response()->json($conversation);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $template = Template::where([
            ['slug', $slug],
            ['type', 'AiChat']
        ])->first();

        $conversations = Conversation::where([
            ['user_id', Auth::User()->id],
            ['template_id', $template->id]
        ])->latest()->paginate(20);

        if ($conversations->count() <= 0) {
            $info = json_decode($template->data);

            $data = [
                [
                    'message' => $info->message,
                    'type' => 'bot',
                    'created_at' => Carbon::now()
                ]
            ];

            $conversation = new Conversation();
            $conversation->user_id = Auth::User()->id;
            $conversation->template_id = $template->id;
            $conversation->title = $template->title;
            $conversation->data = json_encode($data);
            $conversation->save();
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $conversations = Conversation::where([
            ['user_id', Auth::User()->id],
            ['template_id', $template->id]
        ])->latest()->paginate(20);



        return Inertia::render('User/ChatShow', [
            'logo' => $settings->site_logo,
            'chat' => $template,
            'conversations' => $conversations
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function conversationDelete(Request $request)
    {
        $conversation = Conversation::findOrFail($request->id);
        $conversation->delete();

        return response()->json('Deleted');
    }

    public function aiGenerate(Request $request)
    {
        $conversation = Conversation::findOrFail($request->conversation_id);
        $info = json_decode($conversation->data);

        $user = Auth::User();
        $userInfo = json_decode($user->data);

        $openAi = json_decode(Option::where('key', 'openai')->first()->value);

        if ($userInfo->word_limit == -1) {
        } else {
            if ($userInfo->word_limit < $userInfo->use_word_limit) {
                return response()->json('Your Word limit is exceeded. Please Upgrade Your Plan.', 400);
            }
        }

        $newMessage = [
            'message' => $request->message,
            'type' => 'user',
            'created_at' => Carbon::now()
        ];

        // Append the new message to the existing data
        $info[] = $newMessage;

        $conversation->data = json_encode($info);
        $conversation->save();

        $template = Template::findOrFail($request->template_id);
        $templateInfo = json_decode($template->data);
        $prompt = 'You will act as a ' . $template->title . ". User Will ask question and you will give him correct answer. You can't give any answer outside of " . $template->title . ' topic. If user question is outside of topic then say, "I am not Sure. Or Something similar. Try to give short answer to user. "';

        $prompt .= $templateInfo->prompt;

        $prompt .= 'Q: ' . $request->message;
        $prompt .= 'A: ?';

        header("Content-type: text/event-stream");
        header("Cache-Control: no-cache");
        header('Connection: keep-alive');
        header('X-Accel-Buffering: no');
        ob_end_flush();

        $openai = new OpenAi(env('OPENAI_API_KEY'));

        if ($openAi->openai_model == 'gpt-3.5-turbo') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => 1.0,
                'max_tokens' => 800,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);


                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "event: stop\n";
                    echo "data: " . json_encode(["content" => $error]) . "\n\n";
                    flush();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-3.5-turbo-16k') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo-16k',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => 1.0,
                'max_tokens' => 800,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);
               // dd($data);
                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    //var_dump($deltas);
                 //   flush();
                  //  echo die;

                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-4') {
            $openai->chat([
                'model' => 'gpt-4',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => 1.0,
                'max_tokens' => 800,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);

                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }
                //    $return_content =  str_replace("\n\n", '\\n\\n', $content) ;

                   echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        }
    }
}
