<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Orhanerday\OpenAi\OpenAi;

class AiCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $info = json_decode($user->data);

        if ($info->ai_code != 1) {
            return abort(404);
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/AiCode', [
            'logo' => $settings->site_logo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    protected function generateSlug($name)
    {
        $slug = Str::slug($name);
        $check_document = Document::where('slug', $slug)->first();
        if ($check_document) {
            return $slug . Str::random(20);
        } else {
            return $slug;
        }
    }

    public function generate(Request $request)
    {


        $user = Auth::User();
        $info = json_decode($user->data);
        $openAi = json_decode(Option::where('key', 'openai')->first()->value);

        if ($info->word_limit == -1) {
        } else {
            if ($info->word_limit < $info->use_word_limit) {
                echo "event: stop\n";
                echo "data: Your Word limit is exceeded. Please Upgrade Your Plan.\n\n";
                flush();
            }
        }

        header("Content-type: text/event-stream");
        header("Cache-Control: no-cache");
        header('Connection: keep-alive');
        header('X-Accel-Buffering: no');
        ob_end_flush();

        $prompt = '';

        $prompt .= 'Write me code using ' . $request->language . '. ';

        if ($request->level) {
            $prompt .= 'The coding level will be ' . $request->level . '. ';
        }

        $prompt .= 'The code prompt is: ' . $request->prompt;



        $openai = new OpenAi(env('OPENAI_API_KEY'));

        if ($openAi->openai_model == 'gpt-3.5-turbo') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'max_tokens' => 1200,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);


                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "event: stop\n";
                    echo "data: " . json_encode(["content" => $error]) . "\n\n";
                    flush();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-3.5-turbo-16k') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo-16k',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'max_tokens' => 1200,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);

                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-4') {
            $openai->chat([
                'model' => 'gpt-4',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'max_tokens' => 1200,
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);

                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
