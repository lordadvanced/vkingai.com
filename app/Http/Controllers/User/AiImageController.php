<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Option;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;
use Illuminate\Support\Str;

class AiImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $info = json_decode($user->data);

        if($info->ai_images != 1)
        {
            return abort(404);
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $images = Document::where([
            ['user_id', Auth::User()->id],
            ['type', 'aiImage']
        ])->latest()->paginate(20);

        return Inertia::render('User/AiImages', [
            'logo' => $settings->site_logo,
            'images' => $images
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    protected function generateSlug($name)
    {
        $slug = Str::slug($name);
        $check_document = Document::where('slug', $slug)->first();
        if($check_document)
        {
            return $slug . Str::random(20);
        }else {
            return $slug;
        }
    }



    public function aiImageGenerate(Request $request)
    {
        $prompt = '';

        $user = Auth::User();
        $info = json_decode($user->data);

        if($info->image_limit == -1)
        {

        }else {
            if($info->image_limit <= $info->use_image_limit)
            {
                return response()->json('Your Image limit is exceeded. Please Upgrade Your Plan.', 400);
            }
        }


        if($request->imageStyle)
        {
            $prompt .= 'The Image Style will be '. $request->imageStyle . '.';
        }

        if($request->mood)
        {
            $prompt .= 'The Image Mood will be '. $request->mood . '. ';
        }

        $prompt .= 'The Image prompt is: '. $request->prompt;

        $response = Http::timeout(120)->withHeaders([
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.env('OPENAI_API_KEY').''
        ])->post('https://api.openai.com/v1/images/generations', [
            'prompt' => $request->prompt,
            'n' => intval($request->count),
            'size' => $request->resolution
        ]);


        if(isset($response['data']))
        {
            $documentLists = [];
            foreach ($response['data'] as $key => $value) {

                // Here, need to upload Image to 'uploads/ai-image/' From $value->url

                 // Get the image URL from the form
                $imageUrl = $value['url'];

                // Get the Content-Type header of the image
                $headers = get_headers($imageUrl, 1);
                $contentType = $headers['Content-Type'];

                // Extract the file extension from the Content-Type header
                $extension = explode('/', $contentType)[1];

                // Generate a unique filename
                $fileName = Str::slug(pathinfo($imageUrl, PATHINFO_FILENAME)) . '-' . uniqid() . '.' . $extension;

                // Download the image
                $imageData = file_get_contents($imageUrl);

                // Define the destination path
                $uploadPath = public_path('uploads/ai-image/') . $fileName;

                // Move the downloaded image to the desired location
                if (file_put_contents($uploadPath, $imageData)) {

                } else {
                    return response()->json(['error' => 'Failed to upload image.'], 401);
                }

                $data = [
                    'orginal_url' => $value['url'],
                    'image' => '/uploads/ai-image/'. $fileName,
                    'size' => $request->resolution
                ];

                $document = new Document();
                $document->title = $request->prompt;
                $document->user_id = Auth::user()->id;
                $document->slug = $this->generateSlug($request->prompt);
                $document->type = 'aiImage';
                $document->used_token = intval($request->count);
                $document->data = json_encode($data);
                $document->save();

                $documentLists[] = $document;

            }

            $info->use_image_limit = ($info->use_image_limit + intval($request->count));
            $user->data = json_encode($info);
            $user->save();

            return $documentLists;
        }else {
            return response()->json($response['error']['message'], 400);
        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $document = Document::findOrFail($request->id);

        if(!$document)
        {
            return abort(404);
        }

        $info = json_decode($document->data);


        if(file_exists(public_path($info->image)))
        {
            unlink(public_path($info->image));
        }

        $document->delete();

        return response()->json('Deleted');
    }
}
