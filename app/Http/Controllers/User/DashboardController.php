<?php

namespace App\Http\Controllers\User;

use App\Classes\Chatbot as ClassesChatbot;
use App\Http\Controllers\Controller;
use App\Models\Chatbot;
use App\Models\Document;
use App\Models\Lang;
use App\Models\Message;
use App\Models\Option;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;
use Carbon\Carbon;
use Carbon\CarbonPeriod;


class DashboardController extends Controller
{
    public function index(Request $request)
    {
        $currentMonth = Carbon::now()->month;

        // Calculate totalDocuments for the current month
        $totalDocumentsCurrentMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $currentMonth)
            ->whereIn('type', ['aiwrite', 'aiCode', 'aiSpeechToText'])
            ->count();

        $totalContentsCurrentMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $currentMonth)
            ->whereIn('type', ['aiwrite'])
            ->count();

        // Calculate totalDocuments for the previous month
        $previousMonth = Carbon::now()->subMonth()->month;
        $totalDocumentsPreviousMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $previousMonth)
            ->whereIn('type', ['aiwrite', 'aiCode', 'aiSpeechToText'])
            ->count();

        // Calculate totalImages for the current month
        $totalImagesCurrentMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $currentMonth)
            ->whereIn('type', ['aiImage'])
            ->count();

        // Calculate totalImages for the previous month
        $totalImagesPreviousMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $previousMonth)
            ->whereIn('type', ['aiImage'])
            ->count();

        // Calculate totalAiCode for the current month
        $totalAiCodeCurrentMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $currentMonth)
            ->whereIn('type', ['aiCode'])
            ->count();

        $totalAiSpeechToTextCurrentMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $currentMonth)
            ->whereIn('type', ['aiSpeechToText'])
            ->count();

        // Calculate totalAiCode for the previous month
        $totalAiCodePreviousMonth = Document::where('user_id', Auth::User()->id)->whereMonth('created_at', $previousMonth)
            ->whereIn('type', ['aiCode'])
            ->count();

        // Calculate the percentage change for totalDocuments
        if ($totalDocumentsPreviousMonth != 0) {
            $percentageChangeDocuments = round((($totalDocumentsCurrentMonth - $totalDocumentsPreviousMonth) / $totalDocumentsPreviousMonth) * 100, 2);
        } else {
            $percentageChangeDocuments = 100.00; // Rounded to 2 decimal places
        }

        // Calculate the percentage change for totalImages
        if ($totalImagesPreviousMonth != 0) {
            $percentageChangeImages = round((($totalImagesCurrentMonth - $totalImagesPreviousMonth) / $totalImagesPreviousMonth) * 100, 2);
        } else {
            $percentageChangeImages = 100.00; // Rounded to 2 decimal places
        }

        // Calculate the percentage change for totalAiCode
        if ($totalAiCodePreviousMonth != 0) {
            $percentageChangeAiCode = round((($totalAiCodeCurrentMonth - $totalAiCodePreviousMonth) / $totalAiCodePreviousMonth) * 100, 2);
        } else {
            $percentageChangeAiCode = 100.00; // Rounded to 2 decimal places
        }

        // Determine whether the changes are positive, negative, or no change
        $changeStatusDocuments = $percentageChangeDocuments > 0 ? 'positive' : ($percentageChangeDocuments < 0 ? 'negative' : 'no change');
        $changeStatusImages = $percentageChangeImages > 0 ? 'positive' : ($percentageChangeImages < 0 ? 'negative' : 'no change');
        $changeStatusAiCode = $percentageChangeAiCode > 0 ? 'positive' : ($percentageChangeAiCode < 0 ? 'negative' : 'no change');

        // Get the last day of the current month using Carbon
        $currentMonth = Carbon::now();
        $lastDayOfMonth = $currentMonth->endOfMonth()->day;

        // Initialize the data array
        $data = [];

        // Loop through each day of the month
        for ($day = 1; $day <= $lastDayOfMonth; $day++) {
            // Query the database to get the total 'used_token' for 'Documents', 'Images', 'Code', and 'aiSpeechToText' types for the current day
            $documentsUsedToken = (int) Document::where('user_id', Auth::User()->id)->where('type', 'aiwrite')
                ->whereDay('created_at', $day)
                ->whereMonth('created_at', $currentMonth->month)
                ->whereYear('created_at', $currentMonth->year)
                ->sum('used_token');

            $imagesUsedToken = (int) Document::where('user_id', Auth::User()->id)->where('type', 'aiImage')
                ->whereDay('created_at', $day)
                ->whereMonth('created_at', $currentMonth->month)
                ->whereYear('created_at', $currentMonth->year)
                ->sum('used_token');

            $codeUsedToken = (int) Document::where('user_id', Auth::User()->id)->where('type', 'AiCode')
                ->whereDay('created_at', $day)
                ->whereMonth('created_at', $currentMonth->month)
                ->whereYear('created_at', $currentMonth->year)
                ->sum('used_token');

            $speechToTextUsedToken = (int) Document::where('user_id', Auth::User()->id)->where('type', 'aiSpeechToText')
                ->whereDay('created_at', $day)
                ->whereMonth('created_at', $currentMonth->month)
                ->whereYear('created_at', $currentMonth->year)
                ->sum('used_token');

            // Add data for the day to the data array
            $data[] = [
                'name' => $day,
                'AiWrite' => $documentsUsedToken,
                'Images' => $imagesUsedToken,
                'Code' => $codeUsedToken,
                'aiSpeechToText' => $speechToTextUsedToken,
            ];
        }

        $reactJsChatData = $data;

        $totalHistory = [
            $totalContentsCurrentMonth,
            $totalImagesCurrentMonth,
            $totalAiCodeCurrentMonth,
            $totalAiSpeechToTextCurrentMonth
        ];

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $documentTypes = ['aiwrite', 'aiCode', 'aiSpeechToText'];
        $documents = Document::where('user_id', Auth::User()->id)->with('template')->whereIn('type', $documentTypes)
            ->where('user_id', Auth::User()->id)
            ->latest()
            ->paginate(20);

        $recentDocuments = Document::where('user_id', Auth::User()->id)->with('template')
            ->where('type', 'aiwrite')
            ->select('id', 'template_id')
            ->latest()
            ->take(4)
            ->get()
            ->unique('template.id')
            ->map(function ($document) {
                return [
                    'id' => $document->id,
                    'template' => $document->template,
                ];
            })
            ->values() // Reset the array keys to be sequential
            ->toArray();


        return Inertia::render('User/Dashboard', [
            'logo' => $settings->site_logo,
            'documents' => $documents,
            'totalDocumentsCurrentMonth' => $totalDocumentsCurrentMonth,
            'totalDocumentsPreviousMonth' => $totalDocumentsPreviousMonth,
            'percentageChangeDocuments' => $percentageChangeDocuments,
            'changeStatusDocuments' => $changeStatusDocuments,
            'totalImagesCurrentMonth' => $totalImagesCurrentMonth,
            'totalImagesPreviousMonth' => $totalImagesPreviousMonth,
            'percentageChangeImages' => $percentageChangeImages,
            'changeStatusImages' => $changeStatusImages,
            'totalAiCodeCurrentMonth' => $totalAiCodeCurrentMonth,
            'totalAiCodePreviousMonth' => $totalAiCodePreviousMonth,
            'percentageChangeAiCode' => $percentageChangeAiCode,
            'changeStatusAiCode' => $changeStatusAiCode,
            'reactJsChatData' => $reactJsChatData,
            'totalHistory' => $totalHistory,
            'recentDocuments' => $recentDocuments,
            'pagination' => [
                'currentPage' => $documents->currentPage(),
                'lastPage' => $documents->lastPage(),
                'prev_page_url' => $documents->previousPageUrl(),
                'next_page_url' => $documents->nextPageUrl(),
                'form' => $documents->firstItem(),
                'to' => $documents->lastItem(),
                'total' => $documents->total(),
                'per_page' => $documents->perPage()
            ],
        ]);

    }

    public function switchBot(Request $request)
    {
        $chatbot = Chatbot::where('chatbot_id', $request->chatbot_id)->first();
        if($chatbot){
            $request->session()->put('currentBot', [
                'id' => $chatbot->chatbot_id,
                'name' => $chatbot->name,
                'profile' => $chatbot->profile,
                'icon' => $chatbot->icon,
                'data_types' => $chatbot->data_types,
                'status' => $chatbot->status,
                'visibility' => $chatbot->is_public
            ]);
            $user = Auth::User();
            $user->chatbot_id = $request->chatbot_id;
            $user->save();

            return redirect()->route('user.dashboard');
        }else{
            return abort(404);
        }

    }
}
