<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Getway;
use App\Models\Option;
use App\Models\Plan;
use App\Models\Transcation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class BillingController extends Controller
{
    public function index()
    {
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $currency = Option::where('key', 'currency_symbol')->first();
        $transactions = Transcation::latest()->where('user_id', Auth::User()->id)->paginate(20);
        return Inertia::render('User/Billing', [
            'transactions' => $transactions,
            'currency' => $currency->value,
            'logo' => $settings->site_logo
        ]);
    }

    public function payment($id)
    {
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $plan = Plan::findOrFail($id);
        Session::put('plan_id', $plan->id);

        $gateways = Getway::all();
        $currency = Option::where('key', 'currency_symbol')->first();
        $data = [
            'url' => asset('/'),
            'STRIPE_PUBLISHABLE_KEY' => env('STRIPE_PUBLISHABLE_KEY'),
            'PAYPAL_CLIENT_ID' => env('PAYPAL_CLIENT_ID')
        ];
        $paypal = Getway::findOrFail(1);
        $stripe = Getway::findOrFail(2);
        $invoice = Option::where('key', 'invoice')->first();
        $invoiceTax = json_decode($invoice->value)->tax;

        $gateway = Getway::where('status', 'approved')->first();

        $tax = $plan->price / 100 * $invoiceTax;
        $total = $plan->price + $paypal->charge + $tax;
        Session::put('price', $total);

        return Inertia::render('User/SelectPayment', [
            'url' => asset('/'),
            'gateways' => $gateways,
            'data' => $data,
            'plan' => $plan,
            'logo' => $settings->site_logo,
            'currency' => $currency->value,
            'paypal_charge' => $paypal->charge,
            'tax' => $tax,
            'tax_percentage' => $invoiceTax,
            'stripe_charge' => $stripe->charge,
            'gatewayId' => $gateway->id ?? null,
            'STRIPE_PUBLISHABLE_KEY' => env('STRIPE_PUBLISHABLE_KEY')
        ]);
    }

    public function success(Request $request)
    {

        if (!Session::has('plan_id')) {
            return abort(404);
        }



        $plan_id = Session::get('plan_id');

        $plan = Plan::findOrFail($plan_id);
        $paypal = Getway::findOrFail(1);

        $info = json_decode($plan->data);

        $invoice = Option::where('key', 'invoice')->first();
        $invoiceTax = json_decode($invoice->value)->tax;

        $tax = $plan->price / 100 * $invoiceTax;

        $total = round(($plan->price + $paypal->charge + $tax), 2);

        $trx_data = [
            'plan_name' => $plan->name,
            'charge' => $paypal->charge,
            'tax' => $tax,
            'duration' => $plan->duration_type,
            'price' => $plan->price
        ];

        if ($request->status) {
            $transaction = new Transcation();
            $transaction->trx_id = Str::random(10);
            $transaction->user_id = Auth::User()->id;
            $transaction->payment_method = $request->method ?? '';
            $transaction->amount = $total ?? 0;
            $transaction->data = json_encode($trx_data);
            $transaction->status = 'approved';
            $transaction->save();
        }

        $data = [
            'word_limit' => $info->word_limit,
            'templates_limit' => $info->templates_limit,
            'chatbot_limit' => $info->chatbot_limit,
            'ai_templates' => $info->ai_templates,
            'ai_chatbot' => $info->ai_chatbot,
            'ai_images' => $info->ai_images,
            'access_to_gpt3' => $info->access_to_gpt3,
            'accept_to_gpt4' => $info->accept_to_gpt4,
            'ai_speech_to_text' => $info->ai_speech_to_text,
            'ai_code' => $info->ai_code,
            'image_limit' => $info->image_limit,
            'speech_to_text_limit' => $info->speech_to_text_limit,
            'audio_file_size' => $info->audio_file_size,
            'trail_days' => $info->trail_days,
            'accept_trail' => $plan->accept_trail,
            'use_word_limit' => 0,
            'use_image_limit' => 0,
            'use_speech_to_text_limit' => 0
        ];

        if ($plan->duration_type == 'monthly') {
            $date = 30;
        } else {
            $date = 365;
        }

        if ($plan->accept_trail) {
            $date = $info->trail_days;
        }

        $user = Auth::User();
        $user->plan_id = $plan->id;
        $user->will_expire = Carbon::now()->addDays($date);
        $user->data = $data;
        $user->save();

        Session::forget('price');
        Session::forget('plan_id');

        return redirect()->route('user.payment.history');
    }

    public function failed()
    {
        return redirect('user/plan')->withErrors('Your payment has failed. Please try again or contact us for assistance. We apologize for any inconvenience.');
    }

    public function cancel()
    {
        return redirect()->back()->withErrors('Payment cancelled. Please contact us if you have any issues or concerns.');
    }
}
