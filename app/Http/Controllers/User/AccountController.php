<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Option;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Auth;
use Hash;
use Illuminate\Support\Str;


class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $user = Auth::User();
        return Inertia::render('User/Account', [
            'user' => $user,
            'logo' => $settings->site_logo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::User();

        if($request->hasFile('profile'))
        {
            $request->validate([
                'profile' => 'required|image'
            ]);

            $image = $request->file('profile');
            $imageName = time() .'_'. Str::random(10) . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('profile/img'), $imageName);
            $imageName = '/profile/img/'.$imageName;
        }else{
            $imageName = $user->profile;
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->profile = $imageName;


        if($request->previousPassword)
        {
            if(Hash::check($request->previousPassword, $user->password)){

                $user->password = Hash::make($request->password);
                $user->save();
                return 'success';

            }else{
                return response()->json(['currentPassword' => 'The Current Password Is Invalid'], 401);
            }
        }

        $user->save();
        return 'success';

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
