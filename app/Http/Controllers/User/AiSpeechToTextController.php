<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Option;
use App\Models\Transcribe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class AiSpeechToTextController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $info = json_decode($user->data);

        if ($info->ai_speech_to_text != 1) {
            return abort(404);
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/SpeechToText', [
            'logo' => $settings->site_logo
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    protected function generateSlug($name)
    {
        $slug = Str::slug($name);
        $check_document = Document::where('slug', $slug)->first();
        if ($check_document) {
            return $slug . Str::random(20);
        } else {
            return $slug;
        }
    }


    public function generate(Request $request)
    {
        if ($request->hasFile('file')) {
            $file = $request->file('file');

            $user = Auth::user(); // Use 'user' instead of 'User'
            $info = json_decode($user->data);

            // Calculate the maximum file size in kilobytes
            $maxFileSizeKB = $info->audio_file_size * 1024 * 1024;

            // return $maxFileSizeKB;

            if ($file->getSize() >= $maxFileSizeKB) {
                return response()->json('File size exceeds the maximum limit of ' . $info->audio_file_size . 'MB.', 400);
            }


            if ($file->getSize() >= (25 * 1024 * 1024)) {
                return response()->json('File size exceeds the maximum limit of 25MB.', 400);
            }

            $client = new \GuzzleHttp\Client();

            if ($info->speech_to_text_limit == -1) {
            } else {
                if ($info->speech_to_text_limit < $info->use_speech_to_text_limit) {
                    return response()->json('Your Speech To Text limit is exceeded. Please Upgrade Your Plan.', 400);
                }
            }

            $currentDate = date('Y-m-d');

            $filename = time() . "." . $file->getClientOriginalExtension();

            $public_path = 'public/uploads/speechtotext/' . $currentDate . "/";
            if (!Storage::exists('public/uploads/speechtotext/' . $currentDate . "/")) {
                Storage::makeDirectory('public/uploads/speechtotext/' . $currentDate . "/");
            }

            $file->storeAs($public_path, $filename);
            // $url = config('app.url')."uploads/speechtotext/" . $currentDate . '/' . $filename; 
            $url = config('app.url') . Storage::url('uploads/speechtotext/' . $currentDate . '/' . $filename);
            $transcribe = new Transcribe();
            $transcribe->file_name =  $filename;
            $transcribe->path = $file->storeAs($public_path, $filename);
            $transcribe->url =  $url;
            $transcribe->language = "English";
            $transcribe->status =  0;
            $transcribe->save();

            if ($request->languageInput != $request->languageOutput) {
                $data = array(
                    "language" => $request->languageOutput,
                    "url" => "",
                    "filepath" =>  $url,
                    "task" => "translate"
                );
            } else {
                $data = array(
                    "language" => $request->languageOutput,
                    "url" => "",
                    "filepath" =>  $url,
                    "task" => "transcribe"
                );
            }

          //  dd($data);


            try {
                $response = $client->get('http://103.143.142.111:8000/transcribe/', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . env('OPENAI_API_KEY'),
                        'Content-Type' => 'application/json',
                    ],
                    'json' => $data
                ]);
    
                $responseBody = $response->getBody()->getContents();
                $jsonResponse = json_decode($responseBody, true);
              //  dd($jsonResponse);
                $data = [
                    'text' => $jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt']
                ];

                $document = new Document();
                $document->title = 'Ai Speech To Txt';
                $document->user_id = Auth::user()->id;
                $document->slug = $this->generateSlug('Ai Code Generator');
                $document->type = 'aiSpeechToText';
                $document->used_token = str_word_count($jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt']);
                $document->data = json_encode($data);
                $document->save();

                $info->use_speech_to_text_limit = ($info->use_speech_to_text_limit + 1);
                $user->data = json_encode($info);
                $user->save();

                return response()->json([
                    'text' => $jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt'],
                    'documentId' => $document->id
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                // Handle the exception, maybe log it
                $response = json_decode($e->getResponse()->getBody(), true);
                return response()->json($response['error']['message'], 400);
            }
        } else if ($request->url) {

            $user = Auth::user(); // Use 'user' instead of 'User'
            $info = json_decode($user->data);
            $client = new \GuzzleHttp\Client();

            if ($info->speech_to_text_limit == -1) {
            } else {
                if ($info->speech_to_text_limit < $info->use_speech_to_text_limit) {
                    return response()->json('Your Speech To Text limit is exceeded. Please Upgrade Your Plan.', 400);
                }
            }

            $currentDate = date('Y-m-d');

            $filename = time();
            $transcribe = new Transcribe();
            $transcribe->file_name =  $filename;
            $transcribe->path = "";
            $transcribe->url = $request->url;
            $transcribe->language = $request->languageOutput;
            $transcribe->status =  0;
            $transcribe->save();
          //  dd( $transcribe);
             $data = [];
            if ($request->languageInput != $request->languageOutput) {
                if (isset($request->url)) {
                    $data = array(
                        "language" => $request->languageOutput,
                        "url" => $request->url,
                        "filepath" =>  "",
                        "task" => "translate"
                    );
                }
            } else {
                if (isset($request->url)) {
                    $data = array(
                        "language" => $request->languageOutput,
                        "url" => $request->url,
                        "filepath" =>  "",
                        "task" => "transcribe"
                    );
                }
            }
          //  dd($data);




            try {
                $response = $client->get('http://103.143.142.111:8000/transcribe/', [
                    'headers' => [
                        'Authorization' => 'Bearer ' . env('OPENAI_API_KEY'),
                        'Content-Type' => 'application/json',
                    ],
                    'json' => $data
                ]);

                $responseBody = $response->getBody()->getContents();
                $jsonResponse = json_decode($responseBody, true);
                echo $response->getStatusCode();
                //dd($jsonResponse);
                $data = [
                    'text' => $jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt']
                ];

                $document = new Document();
                $document->title = 'Ai Speech To Txt';
                $document->user_id = Auth::user()->id;
                $document->slug = $this->generateSlug('Ai Code Generator');
                $document->type = 'aiSpeechToText';
                $document->used_token = str_word_count($jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt']);
                $document->data = json_encode($data);
                $document->save();

                $info->use_speech_to_text_limit = ($info->use_speech_to_text_limit + 1);
                $user->data = json_encode($info);
                $user->save();

                return response()->json([
                    'text' => $jsonResponse['transcription'] . "<br>" . $jsonResponse['webvtt'],
                    'documentId' => $document->id
                ]);
            } catch (\GuzzleHttp\Exception\ClientException $e) {
                // Handle the exception, maybe log it
                $response = json_decode($e->getResponse()->getBody(), true);
                return response()->json($response['error']['message'], 400);
            }
        } else {
            return response()->json("File Not Exists", 400);
        }
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
