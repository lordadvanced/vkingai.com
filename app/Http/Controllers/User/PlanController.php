<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Plan;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use Session;

class PlanController extends Controller
{
    public function index()
    {

        $monthlyPlans = Plan::where([
            ['status', 'approved'],
            ['duration_type', 'monthly']
        ])->get();

        $yearlyPlans = Plan::where([
            ['status', 'approved'],
            ['duration_type', 'yearly']
        ])->get();

        $user = Auth::User();

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $currency = Option::where('key', 'currency_symbol')->first();

        return Inertia::render('User/Plan', [
            'monthlyPlans' => $monthlyPlans,
            'yearlyPlans' => $yearlyPlans,
            'user' => $user,
            'logo' => $settings->site_logo,
            'appName' => env('APP_NAME'),
            'currency' => $currency->value
        ]);
    }

    public function check(){

        $user = Auth::user();
        $data = json_decode($user->data);

        if($user->will_expire <= Carbon::now())
        {
            return response()->json(['errors' => "We noticed that your subscription has expired. If you'd like to continue using our service, please renew your subscription."], 401);
        }

        if($data->current_word >= $data->wordCount)
        {
            return response()->json(['errors' => "You have exceeded the usage limit for your subscription. Please upgrade to continue using our service."], 401);
        }
    }

    public function subscribe($id)
    {
        $plan = Plan::findOrFail($id);
        $info = json_decode($plan->data);
        $user = Auth::User();

        $data = [
            'word_limit' => $info->word_limit,
            'templates_limit' => $info->templates_limit,
            'chatbot_limit' => $info->chatbot_limit,
            'ai_templates' => $info->ai_templates,
            'ai_chatbot' => $info->ai_chatbot,
            'ai_images' => $info->ai_images,
            'access_to_gpt3' => $info->access_to_gpt3,
            'accept_to_gpt4' => $info->accept_to_gpt4,
            'ai_speech_to_text' => $info->ai_speech_to_text,
            'ai_code' => $info->ai_code,
            'image_limit' => $info->image_limit,
            'speech_to_text_limit' => $info->speech_to_text_limit,
            'audio_file_size' => $info->audio_file_size,
            'trail_days' => $info->trail_days,
            'accept_trail' => $plan->accept_trail,
            'use_word_limit' => 0,
            'use_image_limit' => 0,
            'use_speech_to_text_limit' => 0
        ];

        if($plan->duration_type == 'monthly')
        {
            $date = 30;
        }else{
            $date = 365;
        }

        if($plan->accept_trail)
        {
            $date = $info->trail_days;
        }

        $user = Auth::User();
        $user->plan_id = $id;
        $user->will_expire = Carbon::now()->addDays($date);
        $user->data = $data;
        $user->save();

        Session::forget('price');
        Session::forget('plan_id');

        return redirect()->route('user.dashboard');
    }
}
