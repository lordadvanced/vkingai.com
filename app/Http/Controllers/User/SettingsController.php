<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Chatbot;
use App\Models\Option;
use App\Models\Plan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
        $appName = env('APP_NAME');
        $user = Auth::User();
        $plan = Plan::where('id', $user->plan_id)->first();

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/Settings', [
            'appName' => $appName,
            'planName' => $plan->name,
            'user' => $user,
            'logo' => $settings->site_logo
        ]);
    }

    public function information()
    {
        $user = Auth::User();
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/SettingsUpdate', [
            'user' => $user,
            'logo' => $settings->site_logo
        ]);
    }
}
