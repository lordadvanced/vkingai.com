<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Document;
use App\Models\Lang;
use App\Models\Option;
use App\Models\Template;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Inertia\Inertia;
use Illuminate\Support\Str;
use Svg\Tag\Rect;
use Orhanerday\OpenAi\OpenAi;


class TemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::User();
        $info = json_decode($user->data);

        if ($info->ai_templates != 1) {
            return abort(404);
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $categories = Category::with('templates')->where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();
        return Inertia::render('User/Templates', [
            'logo' => $settings->site_logo,
            'categories' => $categories
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function templatesData(Request $request)
    {
        $categories = Category::with('templates')->where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();
        $uniqueTemplates = [];

        foreach ($categories as $key => $category) {
            foreach ($category->templates as $template) {
                // Check if the template ID is already in the list of unique templates
                if (!array_key_exists($template->id, $uniqueTemplates)) {
                    // If not, add it to the list
                    $uniqueTemplates[$template->id] = $template;
                }
            }
        }

        // Convert the associative array of unique templates back to a simple array
        $uniqueTemplates = array_values($uniqueTemplates);

        $user = Auth::User();
        if ($user->templates_limit == -1) {
            return $uniqueTemplates;
        } else {
            $firstTwoTemplates = array_slice($uniqueTemplates, 0, $user->templates_limit);
            return $firstTwoTemplates;
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    public function favorite(Request $request)
    {
        $template = Template::findOrFail($request->id);
        if ($template->is_favorite === 1) {
            $template->is_favorite = 0;
        } else {
            $template->is_favorite = 1;
        }
        $template->save();
        return 'Success';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $template = Template::where('slug', $slug)->first();
        if (!$template) {
            return abort(404);
        }
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $languages = Lang::where('status', 'approved')->get();

        return Inertia::render('User/TemplateShow', [
            'template' => $template,
            'logo' => $settings->site_logo,
            'languages' => $languages
        ]);
    }

    protected function generateSlug($name)
    {
        $slug = Str::slug($name);
        $check_document = Document::where('slug', $slug)->first();
        if ($check_document) {
            return $slug . Str::random(20);
        } else {
            return $slug;
        }
    }


    public function aiGenerate(Request $request)
    {
        $prompt = 'Please create content in the language of your choice: ' . $request->language . 'The content should reflect the tone you specified as ' . $request->tone . '. Your prompt is: ' . $request->prompt . 'Content Will be Write ' . $request->result . 'times.';

        $user = Auth::User();
        $info = json_decode($user->data);
        $openAi = json_decode(Option::where('key', 'openai')->first()->value);

        if ($info->word_limit == -1) {
        } else {
            if ($info->word_limit < $info->use_word_limit) {

                echo "event: stop\n";
                echo "data: Your Word limit is exceeded. Please Upgrade Your Plan.\n\n";
                flush();
            }
        }

        header("Content-type: text/event-stream");
        header("Cache-Control: no-cache");
        header('Connection: keep-alive');
        header('X-Accel-Buffering: no');
        ob_end_flush();

        $openai = new OpenAi(env('OPENAI_API_KEY'));

        if ($openAi->openai_model == 'gpt-3.5-turbo') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => intval($request->creativity),
                'max_tokens' => intval($request->length),
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);


                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "event: stop\n";
                    echo "data: " . json_encode(["content" => $error]) . "\n\n";
                    flush();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-3.5-turbo-16k') {
            $openai->chat([
                'model' => 'gpt-3.5-turbo-16k',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => intval($request->creativity),
                'max_tokens' => intval($request->length),
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);

                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        } else if ($openAi->openai_model == 'gpt-4') {
            $openai->chat([
                'model' => 'gpt-4',
                'messages' => [
                    [
                        "role" => "system",
                        "content" => "You are a helpful assistant."
                    ],
                    [
                        "role" => "user",
                        "content" => $prompt
                    ],
                ],
                'temperature' => intval($request->creativity),
                'max_tokens' => intval($request->length),
                'frequency_penalty' => 0,
                'presence_penalty' => 0,
                'stream' => true
            ], function ($ch, $data) {

                $json = json_decode($data);

                if (isset($json->error)) {
                    $error  = $json->error->message;
                    $error .= " (" . $json->error->code . ")";
                    $error  = "`" . trim($error) . "`";

                    echo "data: " . json_encode(["content" => $error]) . "\n\n";

                    echo "event: stop\n";
                    echo "data: stopped\n\n";

                    flush();
                    die();
                }

                $deltas = explode("\n", $data);

                // echo $deltas;
                foreach ($deltas as $key => $delta) {
                    if (strpos($delta, "data: ") !== 0) {
                        continue;
                    }

                    $json = json_decode(substr($delta, 6));

                    if (isset($json->choices[0]->delta)) {
                        $content = $json->choices[0]->delta->content ?? '';
                    } elseif (isset($json->error->message)) {
                        $content = $json->error->message;
                    } elseif (trim($delta) == "data: [DONE]") {
                        $content = '[DONE]';
                    } else {
                        $content = "";
                    }

                    echo "data: " . str_replace("\n", '\\n', $content)  . "\n\n";
                    flush();
                }

                if (connection_aborted()) {
                    return 0;
                }


                return strlen($data);
            });

            // Send a stop event
            echo "event: stop\n";
            echo "data: stopped\n";
        }
    }

    public function storeDocument(Request $request)
    {
        $user = Auth::User();
        $info = json_decode($user->data);
        if ($request->template) {
            $template = Template::findOrFail($request->template);
            $title = $template->title;
            $templateId = $template->id;
            $data = $request->content;
        } else {
            $title = 'Ai Code Generator';
            $templateId = null;
            $data = [
                'code' => $request->content
            ];
        }

        $token_array = gpt_encode($request->content);
        $totalTokens = count($token_array);

        $document = new Document();
        $document->user_id = Auth::User()->id;
        if (!$request->template) {
            $document->title = 'Ai Code Generator';
        }
        $document->slug = $this->generateSlug($title);
        $document->template_id = $templateId;
        $document->type = $request->type;

        $document->used_token = $totalTokens;
        if ($request->template) {
            $document->data = $data;
        } else {
            $document->data = json_encode($data);
        }
        $document->save();

        $info->use_word_limit = ($info->use_word_limit + $totalTokens);
        $user->data = json_encode($info);
        $user->save();

        return response()->json([
            'documentId' => $document->id
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
