<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class DocumentController extends Controller
{

    public function index()
    {
        $documentTypes = ['aiwrite', 'aiCode', 'aiSpeechToText'];
        $documents = Document::with('template')->whereIn('type', $documentTypes)
            ->where('user_id', Auth::User()->id)
            ->latest()
            ->paginate(20);

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/Document', [
            'logo' => $settings->site_logo,
            'documents' => $documents,
            'pagination' => [
                'currentPage' => $documents->currentPage(),
                'lastPage' => $documents->lastPage(),
                'prev_page_url' => $documents->previousPageUrl(),
                'next_page_url' => $documents->nextPageUrl(),
                'form' => $documents->firstItem(),
                'to' => $documents->lastItem(),
                'total' => $documents->total(),
                'per_page' => $documents->perPage()
            ],
        ]);
    }



    public function rename(Request $request)
    {
        $document = Document::findOrFail($request->id);
        if(!$document)
        {
            return abort(404);
        }

        $document->title = $request->title;
        $document->save();

        return response()->json('Saved');
    }

    public function show($slug)
    {
        $document = Document::with('template')->where('slug', $slug)->first();
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/DocumentShow', [
            'logo' => $settings->site_logo,
            'documentInfo' => $document
        ]);
    }

    public function update(Request $request)
    {
        $document = Document::findOrFail($request->id);
        if(!$document)
        {
            return abort(404);
        }

        $document->data = $request->data;
        $document->save();

        return response()->json('Saved');
    }

    public function bookmark(Request $request)
    {
        $document = Document::findOrFail($request->id);
        if($document->is_bookmark === 1)
        {
            $document->is_bookmark = 0;
        }else {
            $document->is_bookmark = 1;
        }
        $document->save();

        return response()->json('Bookmarked');
    }

    public function delete(Request $request)
    {
        Document::find($request->id)->delete();
        return response()->json('Deleted');
    }

}
