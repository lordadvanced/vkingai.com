<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Support;
use App\Models\SupportMeta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Inertia\Inertia;

class SupportController extends Controller
{
    public function index()
    {
        $openSupports = Support::where([
            ['user_id', Auth::id()],
            ['status', 'open']
        ])->latest()->paginate(10);
        $closeSupports = Support::where([
            ['user_id', Auth::id()],
            ['status', 'close']
        ])->latest()->paginate(10);

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);


        return Inertia::render('User/Support', [
            'openSupports' => $openSupports,
            'closeSupports' => $closeSupports,
            'logo' => $settings->site_logo,
            'openPagination' => [
                'currentPage' => $openSupports->currentPage(),
                'lastPage' => $openSupports->lastPage(),
                'prev_page_url' => $openSupports->previousPageUrl(),
                'next_page_url' => $openSupports->nextPageUrl(),
                'form' => $openSupports->firstItem(),
                'to' => $openSupports->lastItem(),
                'total' => $openSupports->total(),
                'per_page' => $openSupports->perPage()
            ],
            'closePagination' => [
                'currentPage' => $closeSupports->currentPage(),
                'lastPage' => $closeSupports->lastPage(),
                'prev_page_url' => $closeSupports->previousPageUrl(),
                'next_page_url' => $closeSupports->nextPageUrl(),
                'form' => $closeSupports->firstItem(),
                'to' => $closeSupports->lastItem(),
                'total' => $closeSupports->total(),
                'per_page' => $closeSupports->perPage()
            ]
        ]);
    }

    public function store(Request $request)
    {
        $support = new Support();
        $support->ticket_no = '#'.Str::random(10);
        $support->title = $request->title;
        $support->user_id = Auth::id();
        $support->status = 'open';
        $support->save();

        $supportMeta = new SupportMeta();
        $supportMeta->support_id = $support->id;
        $supportMeta->type = 'user';
        $supportMeta->message = $request->description;
        $supportMeta->save();

        return response()->json('Success');

    }

    public function send(Request $request)
    {
        $message = new SupportMeta();
        $message->support_id = $request->id;
        $message->message = $request->message;
        $message->type = 'user';
        $message->save();

        return response()->json('success');
    }

    public function view($id)
    {
        $support = Support::findOrFail($id);
        $messages = SupportMeta::where([
            ['support_id', $id]
        ])->latest()->paginate(40);
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('User/SupportView', [
            'support' => $support,
            'messages' => $messages,
            'logo' => $settings->site_logo
        ]);
    }
}
