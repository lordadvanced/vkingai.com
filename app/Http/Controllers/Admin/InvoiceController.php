<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Option;
use App\Models\Transcation;
use Illuminate\Http\Request;
use PDF;

class InvoiceController extends Controller
{
    public function downloadInvoice($id)
    {
        $transcation = Transcation::findOrFail($id);
        $currency = Option::where('key', 'currency_symbol')->first();
        $logo = Option::where('key', 'site_settings')->first();
        $info = json_decode($transcation->data);

        // Pass the variables to the view
        $data = [
            'invoiceId' => $transcation->trx_id,
            'payment_method' => $transcation->payment_method,
            'amount' => $transcation->amount,
            'created_at' => $transcation->created_at->toDateString(),
            'currency' => $currency->value,
            'planName' => json_decode($transcation->data)->plan_name ?? '',
            'duration' => json_decode($transcation->data)->duration ?? '',
            'logo' => json_decode($logo->value)->site_logo,
            'info' => $info
        ];

        // Generate the PDF from the Blade view
        $pdf = PDF::loadView('invoice.index', $data);

        // Set the filename for the downloaded PDF
        $filename = 'invoice.pdf';

        // You can set additional headers if necessary (e.g., content-type, content-disposition)
        $headers = [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
        ];

        // Output the PDF as a response for download
        return $pdf->download($filename, $headers);
    }

}
