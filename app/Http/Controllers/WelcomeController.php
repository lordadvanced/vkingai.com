<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Lang;
use App\Models\Menu;
use App\Models\Option;
use App\Models\Plan;
use App\Models\Template;
use App\Models\Term;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Pixamo\Installer\Installer;


class WelcomeController extends Controller
{
    public function index()
    {
        if(Installer::isActive())
        {
            return redirect('install');
        }

        $monthlyPlans = Plan::where([
            ['status', 'approved'],
            ['duration_type', 'monthly']
        ])->get();

        $yearlyPlans = Plan::where([
            ['status', 'approved'],
            ['duration_type', 'yearly']
        ])->get();

        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');

        $seo_home = Term::with('seometa')->where([
            ['slug', 'home'],
            ['type', 'seo']
        ])->first();

        $services = Term::with('featureMeta')->where([
            ['type', 'features'],
            ['status', 'approved']
        ])->latest()->take(6)->get();

        $currency = Option::where('key', 'currency_symbol')->first();

        $testimonials = Term::with('testimonialMeta')->where([
            ['type', 'testimonial'],
            ['status', 'approved']
        ])->get();

        $faqs = Term::with('faqMeta')->where([
            ['type', 'faq'],
            ['status', 'approved']
        ])->get();

        $templates = Template::where([
            ['type', 'aiwrite'],
            ['status', 'approved']
        ])->take(6)->get();


        $allTitles = [];

        foreach ($templates as $template) {
            $allTitles[] = $template->title;
            $allTitles[] = 1000;
        }

        $templatesCount = Template::where([
            ['type', 'aiwrite'],
            ['status', 'approved']
        ])->count();

        $categories = Category::with('templates')->where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();

        return Inertia::render('Welcome',[
            'monthlyPlans' => $monthlyPlans,
            'yearlyPlans' => $yearlyPlans,
            'hero' => $hero,
            'settings' => $settings,
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'seo_home' => $seo_home,
            'features' => $services,
            'currency' => $currency->value,
            'appName' => env('APP_NAME'),
            'testimonials' => $testimonials,
            'faqs' => $faqs,
            'templates' => $templates,
            'templatesCount' => $templatesCount,
            'allTitles' => $allTitles,
            'categories' => $categories
        ]);
    }

    public function page($slug)
    {
        $page = Term::with('pagemeta')->where([
            ['type', 'page'],
            ['status', 'approved'],
            ['slug', $slug]
        ])->first();

        if(!$page)
        {
            return abort(404);
        }

        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        return Inertia::render('Pageshow', [
            'settings' => $settings,
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'hero' => $hero,
            'page' => $page
        ]);
    }

    public function langLists()
    {
        $langs = Lang::where('status', 'approved')->get();
        return response()->json($langs);
    }

    public function templates()
    {
        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');

        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $categories = Category::with('templates')->where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();
        $faqs = Term::with('faqMeta')->where([
            ['type', 'faq'],
            ['status', 'approved']
        ])->get();

        return Inertia::render('Templates', [
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'categories' => $categories,
            'settings' => $settings,
            'hero' => $hero,
            'faqs' => $faqs
        ]);

    }
}
