<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Option;
use App\Models\Term;
use Illuminate\Http\Request;
use Inertia\Inertia;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        $allTags = [];

        $searchQuery = $request->search;

        if ($request->search) {
            // Search for blogs and retrieve tags
            $blogs = Term::with('blogmeta')
                ->where([
                    ['status', 'approved'],
                    ['type', 'blog']
                ])
                ->where(function ($query) use ($searchQuery) {
                    $query->where('name', 'like', '%' . $searchQuery . '%');
                })
                ->paginate(9);


        }elseif($request->category)
        {
            $category = Category::findOrFail($request->category);

            $blogs = Term::whereHas('categories', function ($query) use ($category) {
                $query->where('categories.id', $category->id); // Specify the table name for 'id'
            })->paginate(9);

            // Extract and combine tags
            foreach ($blogs as $blog) {
                $tagsArray = json_decode($blog->blogmeta->value, true);
                if (is_array($tagsArray) && !empty($tagsArray['tags'])) {
                    $allTags = array_merge($allTags, $tagsArray['tags']);
                }
            }
            $allTags = array_unique($allTags);
        }elseif($request->tags)
        {

            $selectedTags = explode(',', $request->tags); // Assuming tags are comma-separated in the request

            // Initialize a query builder for blogs
            $query = Term::query();

            // Loop through each selected tag and filter blogs that have that tag
            foreach ($selectedTags as $tag) {
                $query->whereHas('blogmeta', function ($query) use ($tag) {
                    $query->where('value', 'LIKE', '%' . $tag . '%');
                });
            }

            // Paginate the filtered query
            $blogs = $query->paginate(9);

            // Extract and combine tags
            $allTags = $blogs->pluck('blogmeta.tags')->flatten()->unique()->all();

        }
        else {
            // Retrieve all blogs and their tags
            $blogs = Term::with('blogmeta', 'user')
                ->where([
                    ['status', 'approved'],
                    ['type', 'blog']
                ])
                ->paginate(9);
        }

        $seo_blog = Term::with('seometa')->where([
            ['slug', 'blogs'],
            ['type', 'seo']
        ])->first();

        $tagsBlogs = Term::with('blogmeta', 'user')
                ->where([
                    ['status', 'approved'],
                    ['type', 'blog']
                ])
                ->get();

        // Extract and combine tags
        foreach ($tagsBlogs as $blog) {
            $tagsArray = json_decode($blog->blogmeta->value, true);

            if (is_array($tagsArray) && !empty($tagsArray['tags'])) {
                $allTags = array_merge($allTags, $tagsArray['tags']);
            }
        }

        $allTags = array_unique($allTags);

        $recent_blogs = Term::with('blogmeta', 'user')
        ->where([
            ['status', 'approved'],
            ['type', 'blog']
        ])
        ->take(4)->latest()->get();

        $categories = Category::with('blogs')->where([
            ['status', 'approved'],
            ['type', 'category']
        ])->get();

        return Inertia::render('Blogs',[
            'blogs' => $blogs,
            'settings' => $settings,
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'hero' => $hero,
            'seo_blog' => $seo_blog,
            'categories' => $categories,
            'allTags' => array_values($allTags),
            'recent_blogs' => $recent_blogs
        ]);
    }

    public function show($slug)
    {
        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');

        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $blog = Term::with('blogmeta', 'categories')->where([
            ['slug', $slug],
            ['type', 'blog'],
            ['status', 'approved']
        ])->first();

        if(!$blog)
        {
            return abort(404);
        }

        $info = json_decode($blog->blogmeta->value);
        $blogTags = implode(', ', $info->tags ?? []);

        $allTags = [];

        $tagsBlogs = Term::with('blogmeta', 'user')
                ->where([
                    ['status', 'approved'],
                    ['type', 'blog']
                ])
                ->get();

        // Extract and combine tags
        foreach ($tagsBlogs as $tagblog) {
            $tagsArray = json_decode($tagblog->blogmeta->value, true);

            if (is_array($tagsArray) && !empty($tagsArray['tags'])) {
                $allTags = array_merge($allTags, $tagsArray['tags']);
            }
        }

        $allTags = array_unique($allTags);

        $recent_blogs = Term::with('blogmeta', 'user')
        ->where([
            ['status', 'approved'],
            ['type', 'blog']
        ])
        ->take(4)->latest()->get();

        $categories = Category::with('blogs')->where([
            ['status', 'approved'],
            ['type', 'category']
        ])->get();


        $blogs = Term::where([
            ['type', 'blog'],
            ['status', 'approved']
        ])
        ->orderBy('created_at', 'desc') // Adjust the ordering criterion as needed
        ->get();

        // Find the current blog
        $currentBlog = $blog;

        // Determine the index of the current blog
        $currentBlogIndex = $blogs->search(function ($blog) use ($currentBlog) {
            return $blog->id === $currentBlog->id;
        });

        // Calculate the index for the "Previous" and "Next" blogs
        $previousBlogIndex = $currentBlogIndex - 1;
        $nextBlogIndex = $currentBlogIndex + 1;

        // Get the "Previous" and "Next" blogs based on their indices
        $previousBlog = ($previousBlogIndex >= 0) ? $blogs->get($previousBlogIndex) : null;
        $nextBlog = ($nextBlogIndex < $blogs->count()) ? $blogs->get($nextBlogIndex) : null;

        return Inertia::render('Blogdetails',[
            'blog' => $blog,
            'url' => asset('/'),
            'settings' => $settings,
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'hero' => $hero,
            'categories' => $categories,
            'allTags' => array_values($allTags),
            'recent_blogs' => $recent_blogs,
            'blogTags' => $blogTags,
            'previousBlog' => $previousBlog,
            'nextBlog' => $nextBlog
        ]);
    }
}
