<?php

namespace App\Http\Controllers;

use App\Models\Option;
use Illuminate\Http\Request;
use Inertia\Inertia;

class PrivacyController extends Controller
{
    public function privacyPolicy()
    {
        $privacy = Option::where('key', 'privacy')->first();
        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        return Inertia::render('Privacy', [
            'content' => json_decode($privacy->value)->privacy_policy,
            'locate' => app()->getLocale(),
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'settings' => $settings,
            'hero' => $hero
        ]);
    }

    public function termsCondition()
    {
        $privacy = Option::where('key', 'privacy')->first();
        $menuitems = menu('header');

        $footer_first_menuitems = menu('footer_first');
        $footer_second_menuitems = menu('footer_second');
        $footer_third_menuitems = menu('footer_third');
        $footer_four_menuitems = menu('footer_four');
        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);
        $hero = json_decode(Option::where('key','hero_data')->first()->value);

        return Inertia::render('Terms', [
            'content' => json_decode($privacy->value)->terms_of_condition,
            'locate' => app()->getLocale(),
            'menuitems' => $menuitems,
            'footer_first_menuitems' => $footer_first_menuitems,
            'footer_second_menuitems' => $footer_second_menuitems,
            'footer_third_menuitems' => $footer_third_menuitems,
            'footer_four_menuitems' => $footer_four_menuitems,
            'settings' => $settings,
            'hero' => $hero
        ]);
    }

}
