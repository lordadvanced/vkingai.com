<?php

namespace App\Http\Livewire\Admin\Affiliate;

use Livewire\Component;

class Index extends Component
{
    public function render()
    {
        return view('livewire.admin.affiliate.index');
    }
}
