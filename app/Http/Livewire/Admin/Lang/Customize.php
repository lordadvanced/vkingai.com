<?php

namespace App\Http\Livewire\Admin\Lang;

use App\Models\Lang;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Customize extends Component
{

    public $langs = [], $lang_data = [], $lang_code, $data;

    use LivewireAlert;

    public function mount($id)
    {
        $lang = Lang::find($id);
        $this->lang_code = $lang->code;
        $lang_file = file_get_contents(public_path('translations/' . $lang->code . '/global.json'));
        $this->langs = json_decode($lang_file, true);
    }

    public function customize()
    {

        file_put_contents(public_path('translations/' . $this->lang_code . '/global.json'), json_encode($this->langs, JSON_PRETTY_PRINT));

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return true;
    }

    public function render()
    {
        return view('livewire.admin.lang.customize');
    }
}
