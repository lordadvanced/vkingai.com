<?php

namespace App\Http\Livewire\Admin\Lang;

use App\Models\Lang;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use File;

class Create extends Component
{

    use LivewireAlert;

    public $lang = 'en', $description, $languages, $status, $lang_name = 'English';

    public function mount()
    {
        $langs_file = file_get_contents(resource_path('json/languages.json'));
        $langs = json_decode($langs_file, true);
        $this->languages = $langs;
    }


    public function store()
    {
        $langs_file = file_get_contents(resource_path('json/languages.json'));
        $langs = json_decode($langs_file, true);

        foreach ($langs as $key => $value) {
            if ($value['code'] == $this->lang) {
                $this->lang_name = $value['name'];
            }
        }

        $lang_check = Lang::where('code', $this->lang)->first();


        if ($lang_check) {
            $this->alert('error', "It's Already Exists!", [
                'position' => 'top-end',
                'timer' => 3000,
                'toast' => true,
                'timerProgressBar' => true,
                'text' => '',
            ]);

            return back();
        }

        $translate_file = file_get_contents(public_path('translations/translate.json'));
        $translate_file_data = json_decode($translate_file, true);
        if (!file_exists(public_path('translations/' . $this->lang))) {
            mkdir(public_path('translations/' . $this->lang));
        }
        file_put_contents(public_path('translations/' . $this->lang . '/global.json'), json_encode($translate_file_data, JSON_PRETTY_PRINT));

        $path = public_path('translations/languages.json');

        if (File::exists($path)) {
            $languages = json_decode(File::get($path), true);

            // Check if the language code already exists
            if (!in_array($this->lang, $languages)) {
                // Add the new language code
                $languages[] = $this->lang;

                // Save the updated array back to the file
                File::put($path, json_encode($languages));
            }
        }

        $lang = new Lang();
        $lang->name = $this->lang_name;
        $lang->code = $this->lang ?? 'en';
        $lang->status = $this->status ?? 'approved';
        $lang->save();

        return redirect()->route('admin.lang.index');
    }



    public function render()
    {
        return view('livewire.admin.lang.create');
    }
}
