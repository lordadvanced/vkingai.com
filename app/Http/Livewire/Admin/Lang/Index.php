<?php

namespace App\Http\Livewire\Admin\Lang;

use App\Models\Lang;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public function change_status()
    {
        if($this->status)
        {
            if($this->status == 'delete')
            {
                foreach($this->checklistId as $value)
                {

                    $lang = Lang::find($value);
                    $jsonFilePath = resource_path('lang/languages.json');
                    $data = json_decode(file_get_contents($jsonFilePath), true);
                    $objectToRemove = $lang->code;
                    $indexToRemove = array_search($objectToRemove, $data, true);
                    if ($indexToRemove !== false) {
                        array_splice($data, $indexToRemove, 1);
                        $updatedJsonData = json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
                        if (file_put_contents($jsonFilePath, $updatedJsonData) !== false) {
                            $filePath = resource_path('lang/'.$lang->code.'.json');
                            if (file_exists($filePath)) {
                                unlink($filePath);
                            }
                        } else {
                            // Handle the error in case of writing failure
                        }
                    } else {
                        // Handle the case where the object to remove is not found in the array
                    }

                    $lang->delete();

                    $this->checklistId = [];
                }
            }else{
                foreach($this->checklistId as $value)
                {
                    $page = Lang::find($value);
                    $page->status = $this->status;
                    $page->save();
                    $this->checklistId = [];
                }
            }
        }
    }

    public $status;
    public $checklistId = [];
    public $search;
    public $filter_status;

    public function render()
    {
        $searchTerm = '%'.$this->search.'%';
        if($this->filter_status)
        {
            if($this->filter_status == 'all')
            {
                $langs = Lang::where([
                    ['name','like', $searchTerm]
                ])->latest()->paginate(20);
            }else{
                $langs = Lang::where([
                    ['name','like', $searchTerm],
                    ['status', $this->filter_status]
                ])->latest()->paginate(20);
            }

        }else{
            $langs = Lang::where([
                ['name','like', $searchTerm]
            ])->latest()->paginate(20);
        }

        return view('livewire.admin.lang.index',[
            'langs' => $langs
        ]);
    }
}
