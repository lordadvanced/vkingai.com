<?php

namespace App\Http\Livewire\Admin\Lang;

use App\Models\Lang;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;
use File;

class Edit extends Component
{

    use LivewireAlert;

    public $lang, $status, $lang_code, $lang_name, $language, $languages;

    public function mount($id)
    {

        $lang = Lang::find($id);
        $this->language = $lang;
        $this->lang = $lang->code;
        $this->lang_code = $lang->code;
        $this->status = $lang->status;

        $langs_file = file_get_contents(resource_path('json/languages.json'));
        $langs = json_decode($langs_file, true);
        $this->languages = $langs;
    }

    public function update($id)
    {
        $langs_file = file_get_contents(resource_path('json/languages.json'));
        $langs = json_decode($langs_file, true);

        foreach ($langs as $key => $value) {
            if ($value['code'] == $this->lang) {
                $this->lang_name = $value['name'];
            }
        }

        $lang_check = Lang::where('code', $this->lang)->first();
        $lang = Lang::find($id);

        if ($lang_check) {
            if ($lang_check->id != $lang->id) {
                $this->alert('error', "It's Already Exists!", [
                    'position' => 'top-end',
                    'timer' => 3000,
                    'toast' => true,
                    'timerProgressBar' => true,
                    'text' => '',
                ]);

                return back();
            }
        }

        $translate_file = file_get_contents(public_path('translations/translate.json'));
        $translate_file_data = json_decode($translate_file, true);
        if (!file_exists(public_path('translations/' . $this->lang))) {
            mkdir(public_path('translations/' . $this->lang));
        }
        file_put_contents(public_path('translations/' . $this->lang . '/global.json'), json_encode($translate_file_data, JSON_PRETTY_PRINT));

        $path = public_path('translations/languages.json');

        // Check if the file exists
        if (File::exists($path)) {
            // Read the existing languages from the file
            $languages = json_decode(File::get($path), true);

            // Check if the selected language code is already in the list
            if (!in_array($this->lang, $languages)) {
                // If not, remove the existing language code (if any) and add the new one
                $languages = array_diff($languages, [$this->lang]);
                $languages[] = $this->lang;

                // Save the updated array back to the file
                File::put($path, json_encode($languages));
            }
        }

        $lang->name = $this->lang_name ?? $lang->name;
        $lang->code = $this->lang ?? $lang->code;
        $lang->status = $this->status ?? $lang->status;
        $lang->save();

        return redirect()->route('admin.lang.index');
    }

    public function render()
    {
        return view('livewire.admin.lang.edit');
    }
}
