<?php

namespace App\Http\Livewire\Admin\Category;

use App\Models\Category;
use App\Models\Categorymeta;
use Livewire\Component;
use Illuminate\Support\Str;
use Auth;
use Livewire\WithFileUploads;

class Create extends Component
{

    public $name, $parent, $status, $category_image;

    protected $rules = [
      'name' => 'required',
    ];

    public function store()
    {

        $this->validate();

        $slug = Str::slug($this->name);
        $category_check = Category::where('slug',$slug)->first();
        if($category_check)
        {
            $slug = Str::slug($this->name).Str::random(10);
        }

        $category = new Category();
        $category->name = $this->name;
        $category->slug = $slug;
        $category->type = 'category';
        $category->status = $this->status ?? 'approved';
        $category->save();

        return redirect()->route('admin.category.index');
    }

    public function render()
    {
        $categories = Category::where([
            ['type','category']
        ])->get();

        return view('livewire.admin.category.create',[
            'categories' => $categories
        ]);
    }
}
