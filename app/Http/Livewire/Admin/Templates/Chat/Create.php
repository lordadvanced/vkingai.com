<?php

namespace App\Http\Livewire\Admin\Templates\Chat;

use App\Models\Template;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Create extends Component
{

    public $name, $role, $profile, $message, $prompt, $status = 'approved';

    use WithFileUploads;

    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Template::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'profile' => 'required|image',
            'message' => 'required',
            'prompt' => 'required'
        ]);

        if($this->profile)
        {
            $directory = public_path('uploads/chat/profile');
            $tempPath = $this->profile->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->profile->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/chat/profile/'.$this->profile->hashName();
            rename($tempPath, $destinationPath);
            $profileName = '/uploads/chat/profile/'.$this->profile->hashName().'.'.$extension;
        }else {
            $profileName = 'default.png';
        }

        $data = [
            'role' => $this->role,
            'profile' => $profileName,
            'message' => $this->message,
            'prompt' => $this->prompt,
        ];

        $template = new Template();
        $template->title = $this->name;
        $template->slug = $this->slug($this->name);
        $template->type = 'aiChat';
        $template->data = json_encode($data);
        $template->status = $this->status;
        $template->save();

        return redirect()->route('admin.chat.templates.index');
    }

    public function render()
    {
        return view('livewire.admin.templates.chat.create');
    }
}
