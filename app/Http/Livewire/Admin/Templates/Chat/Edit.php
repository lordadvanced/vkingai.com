<?php

namespace App\Http\Livewire\Admin\Templates\Chat;

use App\Models\Template;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Edit extends Component
{

    public $name, $role, $profile, $message, $prompt, $status = 'approved';
    public $aichat;

    use WithFileUploads;

    public function mount($id)
    {
        $template = Template::findOrFail($id);
        $info = json_decode($template->data);
        $this->aichat = $template;

        $this->fill([
            'name' => $template->title,
            'role' => $info->role,
            'message' => $info->message,
            'prompt' => $info->prompt
        ]);

        $this->status = $template->status;
    }

    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Template::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }



    public function update($id)
    {
        $this->validate([
            'name' => 'required',
            'message' => 'required',
            'prompt' => 'required'
        ]);

        $template = Template::findOrFail($id);
        $info = json_decode($template->data);

        if($this->profile)
        {
            $directory = public_path('uploads/chat/profile');
            $tempPath = $this->profile->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->profile->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/chat/profile/'.$this->profile->hashName();
            rename($tempPath, $destinationPath);
            $profileName = '/uploads/chat/profile/'.$this->profile->hashName().'.'.$extension;
        }else {
            $profileName = $info->profile;
        }

        $data = [
            'role' => $this->role,
            'profile' => $profileName,
            'message' => $this->message,
            'prompt' => $this->prompt,
        ];


        $template->title = $this->name;
        $template->type = 'aiChat';
        $template->data = json_encode($data);
        $template->status = $this->status;
        $template->save();

        return redirect()->route('admin.chat.templates.index');
    }

    public function render()
    {
        return view('livewire.admin.templates.chat.edit');
    }
}
