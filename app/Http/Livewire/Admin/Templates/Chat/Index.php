<?php

namespace App\Http\Livewire\Admin\Templates\Chat;

use App\Models\Template;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    public $status;
    public $checklistId = [];
    public $search;
    public $filter_status;


    protected $paginationTheme = 'bootstrap';

    public function change_status()
    {
        if($this->status)
        {
            if($this->status == 'delete')
            {
                foreach($this->checklistId as $value)
                {
                    Template::find($value)->delete();
                    $this->checklistId = [];
                }
            }else{
                foreach($this->checklistId as $value)
                {
                    $page = Template::find($value);
                    $page->status = $this->status;
                    $page->save();
                    $this->checklistId = [];
                }
            }
        }
    }



    public function render()
    {
        $searchTerm = '%'.$this->search.'%';
        if($this->filter_status)
        {
            if($this->filter_status == 'all')
            {
                $chatTemplates = Template::where([
                    ['type','AiChat'],
                    ['title','like', $searchTerm]
                ])->latest()->paginate(20);
            }else{
                $chatTemplates = Template::where([
                    ['type','AiChat'],
                    ['title','like', $searchTerm],
                    ['status', $this->filter_status]
                ])->latest()->paginate(20);
            }

        }else{
            $chatTemplates = Template::where([
                ['type','AiChat'],
                ['title','like', $searchTerm]
            ])->latest()->paginate(20);
        }

        return view('livewire.admin.templates.chat.index', [
            'chatTemplates' => $chatTemplates
        ]);

    }
}
