<?php

namespace App\Http\Livewire\Admin\Templates\Category;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    use WithPagination;

    public $status;
    public $checklistId = [];
    public $search;
    public $filter_status;


    protected $paginationTheme = 'bootstrap';

    public function change_status()
    {
        if($this->status)
        {
            if($this->status == 'delete')
            {
                foreach($this->checklistId as $value)
                {
                    Category::find($value)->delete();
                    $this->checklistId = [];
                }
            }else{
                foreach($this->checklistId as $value)
                {
                    $page = Category::find($value);
                    $page->status = $this->status;
                    $page->save();
                    $this->checklistId = [];
                }
            }
        }
    }



    public function render()
    {
        $searchCategory = '%'.$this->search.'%';
        if($this->filter_status)
        {
            if($this->filter_status == 'all')
            {
                $templatesCategory = Category::where([
                    ['type','templatesCategory'],
                    ['name','like', $searchCategory]
                ])->latest()->paginate(20);
            }else{
                $templatesCategory = Category::where([
                    ['type','templatesCategory'],
                    ['name','like', $searchCategory],
                    ['status', $this->filter_status]
                ])->latest()->paginate(20);
            }

        }else{
            $templatesCategory = Category::where([
                ['type','templatesCategory'],
                ['name','like', $searchCategory]
            ])->latest()->paginate(20);
        }

        return view('livewire.admin.templates.category.index',[
            'templatesCategory' => $templatesCategory
        ]);
    }
}
