<?php

namespace App\Http\Livewire\Admin\Templates\Category;

use App\Models\Category;
use Livewire\Component;
use Illuminate\Support\Str;

class Edit extends Component
{
    public $category;
    public $name, $short_description, $status = 'approved';

    public function mount($id)
    {
        $category = Category::findOrFail($id);
        $this->category = $category;

        $this->fill([
            'name' => $category->name,
            'short_description' => $category->data
        ]);

        $this->status = $category->status;
    }

    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Category::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }

    public function update($id)
    {
        $this->validate([
            'name' => 'required'
        ]);

        $category = Category::findOrFail($id);
        $category->name = $this->name;
        $category->slug = $this->slug($this->name);
        $category->data = $this->short_description;
        $category->status = $this->status;
        $category->save();

        return redirect()->route('admin.templates.category');

    }

    public function render()
    {
        return view('livewire.admin.templates.category.edit');
    }
}
