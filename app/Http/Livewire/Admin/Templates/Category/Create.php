<?php

namespace App\Http\Livewire\Admin\Templates\Category;

use App\Models\Category;
use Livewire\Component;
use Illuminate\Support\Str;

class Create extends Component
{

    public $name, $slug, $short_description, $status = 'approved';


    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Category::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }


    public function store()
    {
        $this->validate([
            'name' => 'required',
        ]);

        $category = new Category();
        $category->name = $this->name;
        $category->slug = $this->slug($this->name);
        $category->data = $this->short_description;
        $category->status = $this->status;
        $category->save();

        return redirect()->route('admin.templates.category');
    }

    public function render()
    {
        return view('livewire.admin.templates.category.create');
    }
}
