<?php

namespace App\Http\Livewire\Admin\Templates\Custom;

use App\Models\Category;
use App\Models\Template;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Edit extends Component
{
    public $name, $slug, $description, $templateIcon;
    public $custom_prompt;
    public $created_inputs = [];
    public $input_groups = [''];
    public $categories;
    public $category = [];
    public $status = 'approved';
    public $template;

    protected $listeners = ['categoryUpdate'];

    use WithFileUploads;

    public function categoryUpdate($data)
    {
        $this->category = $data;
    }

    public function mount($id)
    {
        $categories = Category::where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();
        $this->categories = $categories;

        $template = Template::with('categories')->findOrFail($id);
        $info = json_decode($template->data);

        $this->template = $template;
        $this->fill([
            'name' => $template->title,
            'description' => $info->description,
            'custom_prompt' => $info->custom_prompt
        ]);

        $this->input_groups = $info->input_groups;
        $this->created_inputs = $info->created_inputs;

        $this->status = $template->status;

        $this->category =  $template->categories->pluck('id')->toArray();

    }


    protected function slugify($text)
    {
        // Replace spaces with hyphens and convert to lowercase
        return '##'.strtolower(str_replace(' ', '-', $text)). '##';
    }

    public function inputNameUpdated($key)
    {
        $value = $this->input_groups[$key]['input_name'];
        // $this->updatedInputName($value, $key);
        $slug = $this->slugify($value);

        // Update the $created_inputs array
        $this->created_inputs[$key] = $slug;
        $this->input_groups[$key]['input_unique_name'] = $slug;
        $this->input_groups[$key]['input_type'] = $this->input_groups[$key]['input_type'] ?? 'input';
        $this->input_groups[$key]['input_description'] = $this->input_groups[$key]['input_description'] ?? '';

    }

    public function addGroup()
    {
        $this->input_groups[] = '';
    }

    public function removeGroup($index)
    {
        unset($this->input_groups[$index]); // Remove the social link at the specified index
        $this->input_groups = array_values($this->input_groups); // Reindex the array after removal
        unset($this->created_inputs[$index]); // Remove the social link at the specified index
        $this->created_inputs = array_values($this->created_inputs);
    }

    public function addInPrompt($item)
    {
        $this->custom_prompt .= $item; // Append the selected item to the custom_prompt
    }

    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Template::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }

    public function update($id)
    {
        $this->validate([
            'name' => 'required',
            'description' => 'required',
            'custom_prompt' => 'required'
        ]);

        if (empty($this->category)) {
           return $this->addError('category', 'The category field is required.');
        }

        $template = Template::findOrFail($id);
        $info = json_decode($template->data);

        if($this->templateIcon)
        {
            $directory = public_path('uploads/templates');
            $tempPath = $this->templateIcon->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }
            $extension = $this->templateIcon->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/templates/'.$this->templateIcon->hashName();
            rename($tempPath, $destinationPath);
            $image = '/uploads/templates/'.$this->templateIcon->hashName().'.'.$extension;
        }else {
            $image = $info->image;
        }

        $data = [
            'image' => $image,
            'description' => $this->description,
            'input_groups' => array_filter($this->input_groups),
            'created_inputs' => $this->created_inputs,
            'custom_prompt' => $this->custom_prompt
        ];


        $template->title = $this->name;
        $template->data = json_encode($data);
        $template->status = $this->status;
        $template->save();

        $template->categories()->sync($this->category);

        return redirect()->route('admin.custom.templates');
    }

    public function render()
    {
        return view('livewire.admin.templates.custom.edit');
    }
}
