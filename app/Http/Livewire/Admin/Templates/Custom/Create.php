<?php

namespace App\Http\Livewire\Admin\Templates\Custom;

use App\Models\Category;
use App\Models\Template;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Create extends Component
{

    public $name, $slug, $description, $templateIcon;
    public $custom_prompt;
    public $created_inputs = [];
    public $input_groups = [''];
    public $categories;
    public $category = [];
    public $status = 'approved';

    protected $listeners = ['categoryUpdate'];

    use WithFileUploads;

    public function categoryUpdate($data)
    {
        $this->category = $data;
    }

    public function mount()
    {
        $categories = Category::where([
            ['status', 'approved'],
            ['type', 'templatesCategory']
        ])->get();
        $this->categories = $categories;
    }


    protected function slugify($text)
    {
        // Replace spaces with hyphens and convert to lowercase
        return '##'.strtolower(str_replace(' ', '-', $text)). '##';
    }

    public function inputNameUpdated($key)
    {
        $value = $this->input_groups[$key]['input_name'];
        // $this->updatedInputName($value, $key);
        $slug = $this->slugify($value);

        // Update the $created_inputs array
        $this->created_inputs[$key] = $slug;
        $this->input_groups[$key]['input_unique_name'] = $slug;
        $this->input_groups[$key]['input_type'] = $this->input_groups[$key]['input_type'] ?? 'input';
        $this->input_groups[$key]['input_description'] = $this->input_groups[$key]['input_description'] ?? '';

    }

    public function addGroup()
    {
        $this->input_groups[] = '';
    }

    public function removeGroup($index)
    {
        unset($this->input_groups[$index]); // Remove the social link at the specified index
        $this->input_groups = array_values($this->input_groups); // Reindex the array after removal
        unset($this->created_inputs[$index]); // Remove the social link at the specified index
        $this->created_inputs = array_values($this->created_inputs);
    }

    public function addInPrompt($item)
    {
        $this->custom_prompt .= $item; // Append the selected item to the custom_prompt
    }

    private function slug($name)
    {
        $slug = Str::slug($name);
        $check_slug = Template::where('slug', $slug)->first();
        if($check_slug)
        {
            return $slug.Str::random(10);
        }

        return $slug;

    }

    public function store()
    {
        $this->validate([
            'name' => 'required',
            'description' => 'required',
            'templateIcon' => 'required|image',
            'custom_prompt' => 'required'
        ]);

        if (empty($this->category)) {
            return $this->addError('category', 'The category field is required.');
        }

        if($this->templateIcon)
        {
            $directory = public_path('uploads/templates');
            $tempPath = $this->templateIcon->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }
            $extension = $this->templateIcon->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/templates/'.$this->templateIcon->hashName();
            rename($tempPath, $destinationPath);
            $image = '/uploads/templates/'.$this->templateIcon->hashName().'.'.$extension;
        }else {
            $image = 'default.png';
        }

        $data = [
            'image' => $image,
            'description' => $this->description,
            'input_groups' => array_filter($this->input_groups),
            'created_inputs' => $this->created_inputs,
            'custom_prompt' => $this->custom_prompt
        ];

        $template = new Template();
        $template->title = $this->name;
        $template->slug = $this->slug($this->name);
        $template->data = json_encode($data);
        $template->status = $this->status;
        $template->template_type = 'custom';
        $template->save();

        $template->categories()->attach($this->category);

        return redirect()->route('admin.custom.templates');
    }


    public function render()
    {
        return view('livewire.admin.templates.custom.create');
    }
}
