<?php

namespace App\Http\Livewire\Admin\Templates\BuildIn;

use App\Models\Template;
use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{
    use WithPagination;

    protected $paginationTheme = 'bootstrap';

    public function change_status()
    {
        if($this->status)
        {
            if($this->status == 'delete')
            {
                foreach($this->checklistId as $value)
                {
                    Template::find($value)->delete();
                    $this->checklistId = [];
                }
            }else{
                foreach($this->checklistId as $value)
                {
                    $page = Template::find($value);
                    $page->status = $this->status;
                    $page->save();
                    $this->checklistId = [];
                }
            }
        }
    }

    public $status;
    public $checklistId = [];
    public $search;
    public $filter_status;

    public function render()
    {
        $searchTerm = '%'.$this->search.'%';
        if($this->filter_status)
        {
            if($this->filter_status == 'all')
            {
                $templates = Template::where([
                    ['title','like', $searchTerm],
                    ['type', 'aiwrite'],
                    ['template_type', 'buildIn']
                ])->latest()->paginate(20);
            }else{
                $templates = Template::where([
                    ['title','like', $searchTerm],
                    ['type', 'aiwrite'],
                    ['template_type', 'buildIn'],
                    ['status', $this->filter_status]
                ])->latest()->paginate(20);
            }

        }else{
            $templates = Template::where([
                ['title','like', $searchTerm],
                ['type', 'aiwrite'],
                ['template_type', 'buildIn']
            ])->latest()->paginate(20);
        }

        return view('livewire.admin.templates.build-in.index',[
            'templates' => $templates
        ]);
    }
}
