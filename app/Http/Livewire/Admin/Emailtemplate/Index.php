<?php

namespace App\Http\Livewire\Admin\Emailtemplate;

use Livewire\Component;
use Livewire\WithPagination;

class Index extends Component
{

    public $templates;

    public $search;

    public function updatingSearch()
    {
        $jsonContents = file_get_contents(resource_path('json/emailTemplates.json'));
        $allTemplates = json_decode($jsonContents, true);

        if (!empty($this->search)) {
            // Filter the templates based on the search string
            $this->templates = array_filter($allTemplates, function ($template) {
                return stripos($template['title'], $this->search) !== false
                    || stripos($template['subject'], $this->search) !== false;
            });
        } else {
            // If search is empty, show all templates
            $this->templates = $allTemplates;
        }
    }

    public function mount()
    {
        $jsonContents = file_get_contents(resource_path('json/emailTemplates.json'));
        $this->templates = json_decode($jsonContents, true);
    }

    public function render()
    {

        return view('livewire.admin.emailtemplate.index');
    }
}
