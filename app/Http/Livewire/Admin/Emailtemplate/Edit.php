<?php

namespace App\Http\Livewire\Admin\Emailtemplate;

use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Edit extends Component
{
    public $template;
    public $content;

    use LivewireAlert;

    public function mount($id){
        $jsonContents = file_get_contents(resource_path('json/emailTemplates.json'));
        $templates = json_decode($jsonContents, true);
        foreach ($templates as $key => $value) {
            if($value['title'] == $id){
                $this->template = $value;
            }
        }

        $htmlContents = file_get_contents(resource_path($this->template['root']));
        $this->content = $htmlContents;
    }


    public function update()
    {
        $filePath = resource_path($this->template['root']);

        // Check if the file is writable (permission to edit)
        if (is_writable($filePath)) {
            // Proceed with updating the file
            file_put_contents($filePath, $this->content);

            $this->alert('success', 'Successfully Updated', [
                'position' => 'top-end',
                'timer' => 3000,
                'toast' => true,
                'timerProgressBar' => true,
                'text' => '',
            ]);

            return redirect()->back();
        } else {
            // File is not writable, show an error message or handle the situation accordingly
            $this->alert('error', 'Permission Denied: Unable to edit the file. You need to set edit permission for this files: root/resources/ '.$this->template['root'], [
                'position' => 'top-end',
                'timer' => 3000,
                'toast' => true,
                'timerProgressBar' => true,
                'text' => '',
            ]);

            return redirect()->back();
        }
    }



    public function render()
    {
        return view('livewire.admin.emailtemplate.edit');
    }
}
