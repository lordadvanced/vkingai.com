<?php

namespace App\Http\Livewire\Admin\Blog;

use App\Models\Category;
use Livewire\Component;
use App\Models\Term;
use App\Models\Termmeta;
use DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;

class Edit extends Component
{

    public $name, $short_content, $description, $status, $blog, $image,$categories;
    public $category;
    public $tags;

    use WithFileUploads;

    public function mount($id)
    {
        $blog = Term::find($id);
        $data = json_decode($blog->blogmeta->value ?? '');
        $this->blog = $blog;
        $this->fill([
            'name' => $blog->name,
            'short_content' => $data->short_content ?? '',
            'description' => $data->description ?? ''
        ]);
        $categories = Category::where([
            ['status', 'approved'],
            ['type', 'category']
        ])->get();

        $this->categories = $categories;
        $this->category = $blog->categories[0]->id ?? '';
        $this->status = $blog->status;
        $this->tags = implode(', ', $data->tags ?? []);
    }

    public function update($id)
    {
        $this->validate([
            'name' => 'required',
            'short_content' => 'required',
            'description' => 'required'
        ]);

        // DB::beginTransaction();

        // try {

            $blog = Term::find($id);

            if($this->image)
            {
                $directory = public_path('uploads/blog');
                $tempPath = $this->image->getRealPath();
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0755, true);
                }

                $extension = $this->image->getClientOriginalExtension();
                $destinationPath = base_path().'/public/uploads/blog/'.$this->image->hashName();
                rename($tempPath, $destinationPath);
                $image = '/uploads/blog/'.$this->image->hashName().'.'.$extension;

            }else{
                $image = json_decode($blog->blogmeta->value)->preview;
            }


            $blog->name = $this->name ?? $blog->name;
            $blog->slug = Str::slug($this->name ?? $blog->name);
            $blog->status = $this->status ?? $blog->status;
            $blog->save();

            if($this->category)
            {
                $blog->categories()->sync($this->category);
            }


            $blogmeta = Termmeta::where('term_id',$blog->id)->first();
            $data = json_decode($blogmeta->value);

            $tagsArray = explode(', ', $this->tags);

            $data = [
                'short_content' => $this->short_content ?? $data->short_content,
                'description' => $this->description ?? $data->description,
                'preview' => $image,
                'tags' => $tagsArray
            ];

            $blogmeta->value = json_encode($data);
            $blogmeta->save();

        //     DB::commit();
        // } catch (\Exception $e) {
        //     DB::rollback();
        // }


        return redirect()->route('admin.blog.index');


    }

    public function render()
    {
        return view('livewire.admin.blog.edit');
    }
}
