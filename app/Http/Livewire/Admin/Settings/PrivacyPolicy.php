<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class PrivacyPolicy extends Component
{
    public $showLoginPage, $privacy_policy, $terms_of_condition;

    use LivewireAlert;

    public function getShowLoginPageProperty(){
        return !$this->showLoginPage;
    }

    public function mount()
    {

        $privacy = Option::where('key', 'privacy')->first();
        $info = json_decode($privacy->value ?? '');
        $this->fill([
            'privacy_policy' => $info->privacy_policy ?? '',
            'terms_of_condition' => $info->terms_of_condition ?? ''
        ]);

        $this->showLoginPage = $info->showLoginPage ?? false;

    }

    public function update()
    {
        $this->validate([
            'privacy_policy' => 'required',
            'terms_of_condition' => 'required',
        ]);

        $privacy = Option::where('key', 'privacy')->first();
        if(!$privacy)
        {
            $privacy = new Option();
            $privacy->key = 'privacy';
        }

        $data = [
            'showLoginPage' => $this->showLoginPage ?? false,
            'privacy_policy' => $this->privacy_policy ?? '',
            'terms_of_condition' => $this->terms_of_condition ?? ''
        ];

        $privacy->value = json_encode($data);
        $privacy->save();

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.settings.privacy-policy');
    }
}
