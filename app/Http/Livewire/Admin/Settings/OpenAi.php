<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Illuminate\Support\Facades\Artisan;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class OpenAi extends Component
{

    public $openai_api_secret, $openai_model, $lang, $creativity, $maximum_input_length, $maximum_output_length;
    public $countries;

    use LivewireAlert;

    public function mount()
    {
        $jsonContents = file_get_contents(resource_path('json/countrylists.json'));
        $this->countries = json_decode($jsonContents, true);
        $openai = Option::where('key', 'openai')->first();
        $info = json_decode($openai->value ?? '');
        $this->fill([
            'openai_api_secret' => env('OPENAI_API_KEY'),
            'maximum_input_length' => $info->maximum_input_length ?? '',
            'maximum_output_length' => $info->maximum_output_length ?? ''
        ]);

        $this->lang = $info->lang ?? '';
        $this->creativity = $info->creativity ?? '';
        $this->openai_model = $info->openai_model ?? '';


    }

    public function update()
    {
        $this->validate([
            'openai_api_secret' => 'required',
            'openai_model' => 'required',
            'lang' => 'required',
            'creativity' => 'required',
            'maximum_input_length' => 'required|numeric',
            'maximum_output_length' => 'required|numeric'
        ]);

        $openai = Option::where('key', 'openai')->first();
        if(!$openai)
        {
            $openai = new Option();
            $openai->key = 'openai';
        }

        Artisan::call("env:set OPENAI_API_KEY='".str_replace(' ', '', $this->openai_api_secret)."'");


        $data = [
            'openai_model' => $this->openai_model ?? '',
            'lang' => $this->lang ?? '',
            'creativity' => $this->creativity ?? '',
            'maximum_input_length' => $this->maximum_input_length ?? '',
            'maximum_output_length' => $this->maximum_output_length ?? ''
        ];

        $openai->value = json_encode($data);
        $openai->save();

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.settings.open-ai');
    }
}
