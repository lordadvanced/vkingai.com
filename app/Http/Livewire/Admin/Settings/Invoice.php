<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Invoice extends Component
{

    public $invoice_name, $invoice_address, $invoice_city, $invoice_state, $postal_code, $country, $tax;


    use LivewireAlert;

    public function mount(){

        $invoice = Option::where('key', 'invoice')->first();
        $info = json_decode($invoice->value);

        $this->fill([
            'invoice_name' => $info->invoice_name ?? '',
            'invoice_address' => $info->invoice_address ?? '',
            'invoice_city' => $info->invoice_city ?? '',
            'invoice_state' => $info->invoice_state ?? '',
            'postal_code' => $info->postal_code ?? '',
            'country' => $info->country ?? '',
            'tax' => $info->tax ?? ''
        ]);
    }

    public function update(){

        $this->validate([
            'invoice_name' => 'required',
            'invoice_address' => 'required',
            'invoice_city' => 'required',
            'invoice_state' => 'required',
            'postal_code' => 'required',
            'country' => 'required',
            'tax' => 'required'
        ]);

        $invoice = Option::where('key', 'invoice')->first();
        if(!$invoice){
            $invoice = new Option();
            $invoice->key = 'invoice';
        }

        $data = [
            'invoice_name' => $this->invoice_name ?? '',
            'invoice_address' => $this->invoice_address ?? '',
            'invoice_city' => $this->invoice_city ?? '',
            'invoice_state' => $this->invoice_state ?? '',
            'postal_code' => $this->postal_code ?? '',
            'country' => $this->country ?? '',
            'tax' => $this->tax ?? ''
        ];


        $invoice->value = json_encode($data);
        $invoice->save();

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);


        return redirect()->back();

    }


    public function render()
    {
        return view('livewire.admin.settings.invoice');
    }
}
