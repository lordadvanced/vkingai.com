<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Illuminate\Support\Facades\Artisan;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class General extends Component
{
    public $settings;
    public $emailVerification, $siteName, $siteUrl, $siteEmail, $country, $currency, $message_credit;
    public $countries;
    public $currencies;
    public $facebook, $google, $github;
    public $custom_css, $custom_js;
    public $FACEBOOK_CLIENT_ID, $FACEBOOK_CLIENT_SECRET;
    public $GOOGLE_CLIENT_ID, $GOOGLE_CLIENT_SECRET;
    public $GITHUB_CLIENT_ID, $GITHUB_CLIENT_SECRET;

    use LivewireAlert;

    public function getEmailVerificationProperty()
    {
        return !$this->emailVerification;
    }

    public function getFacebookProperty()
    {
        return !$this->facebook;
    }

    public function getGoogleProperty()
    {
        return !$this->google;
    }

    public function getGithubProperty()
    {
        return !$this->github;
    }


    public function mount()
    {
        $this->emailVerification = false;
        $jsonContents = file_get_contents(resource_path('json/countrylists.json'));
        $this->countries = json_decode($jsonContents, true);

        $currencyContents = file_get_contents(resource_path('json/currency.json'));
        $this->currencies = json_decode($currencyContents, true);

        $global_settings = Option::where('key', 'global_settings')->first();
        $info = json_decode($global_settings->value ?? '');
        $this->fill([
            'siteName' => env('APP_NAME'),
            'siteUrl' => env('APP_URL'),
            'siteEmail' => $info->siteEmail,
            'message_credit' => $info->free_message_credit,
            'custom_css' => $info->custom_css,
            'custom_js' => $info->custom_js,
            'FACEBOOK_CLIENT_ID' => env('FACEBOOK_CLIENT_ID'),
            'FACEBOOK_CLIENT_SECRET' => env('FACEBOOK_CLIENT_SECRET'),
            'GOOGLE_CLIENT_ID' => env('GOOGLE_CLIENT_ID'),
            'GOOGLE_CLIENT_SECRET' => env('GOOGLE_CLIENT_SECRET'),
            'GITHUB_CLIENT_ID' => env('GITHUB_CLIENT_ID'),
            'GITHUB_CLIENT_SECRET' => env('GITHUB_CLIENT_SECRET'),
        ]);

        $currency = Option::where('key', 'currency_symbol')->first();

        $this->facebook = $info->facebook;
        $this->google = $info->google;
        $this->github = $info->github;
        $this->country = $info->country;
        $this->currency = $currency->value;
        $this->emailVerification = $info->emailVerification;
    }

    public function update()
    {
        $this->validate([
            'siteName' => 'required',
            'siteUrl' => 'required',
            'siteEmail' => 'required',
            'message_credit' => 'required|numeric',
        ]);

        Artisan::call("env:set APP_NAME='".str_replace(' ', '', $this->siteName)."'");
        Artisan::call("env:set APP_URL='".str_replace(' ', '', $this->siteUrl)."'");
        if($this->emailVerification)
        {
            Artisan::call("env:set EMAIL_VERIFICATION='true'");
        }else {
            Artisan::call("env:set EMAIL_VERIFICATION='false'");
        }

        if($this->facebook)
        {
            $this->validate([
                'FACEBOOK_CLIENT_ID' => 'required',
                'FACEBOOK_CLIENT_SECRET' => 'required',
            ]);

            Artisan::call("env:set FACEBOOK_CLIENT_ID='".str_replace(' ', '', $this->FACEBOOK_CLIENT_ID)."'");
            Artisan::call("env:set FACEBOOK_CLIENT_SECRET='".str_replace(' ', '', $this->FACEBOOK_CLIENT_SECRET)."'");
        }

        if($this->google)
        {
            $this->validate([
                'GOOGLE_CLIENT_ID' => 'required',
                'GOOGLE_CLIENT_SECRET' => 'required',
            ]);

            Artisan::call("env:set GOOGLE_CLIENT_ID='".str_replace(' ', '', $this->GOOGLE_CLIENT_ID)."'");
            Artisan::call("env:set GOOGLE_CLIENT_SECRET='".str_replace(' ', '', $this->GOOGLE_CLIENT_SECRET)."'");
        }

        if($this->github)
        {
            $this->validate([
                'GITHUB_CLIENT_ID' => 'required',
                'GITHUB_CLIENT_SECRET' => 'required',
            ]);

            Artisan::call("env:set GITHUB_CLIENT_ID='".str_replace(' ', '', $this->GITHUB_CLIENT_ID)."'");
            Artisan::call("env:set GITHUB_CLIENT_SECRET='".str_replace(' ', '', $this->GITHUB_CLIENT_SECRET)."'");
        }

        $currency = Option::where('key', 'currency_symbol')->first();
        $currency->value = $this->currency;
        $currency->save();

        $data = [
            'emailVerification' => $this->emailVerification,
            'siteEmail' => $this->siteEmail,
            'free_message_credit' => $this->message_credit,
            'country' => $this->country,
            'facebook' => $this->facebook,
            'google' => $this->google,
            'github' => $this->github,
            'custom_css' => $this->custom_css,
            'custom_js' => $this->custom_js
        ];

        $global_settings = Option::where('key', 'global_settings')->first();
        if(!$global_settings){
            $global_settings = new Option();
            $global_settings->key = 'global_settings';
        }

        $global_settings->value = json_encode($data);
        $global_settings->save();

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.settings.general');
    }
}
