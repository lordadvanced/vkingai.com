<?php

namespace App\Http\Livewire\Admin\Settings;

use Livewire\Component;
use App\Models\Option;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;

class Hero extends Component
{
    public $hero_title;
    public $description, $hero_data;
    public $button_title;
    public $image;
    public $button_placeholder;
    public $templates_title;

    use WithFileUploads;

    use LivewireAlert;

    public $fileUploadStatus = true;

    public function updatingChatbotIcon($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedChatbotIcon()
    {
        if ($this->image && $this->image->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function mount()
    {
        $this->hero_data = Option::where('key','hero_data')->first();
        $value = json_decode($this->hero_data?->value);

        $this->fill([
            'hero_title' => $value->hero_title ?? '',
            'description' => $value->description ?? '',
            'button_title' => $value->button_title ?? '',
            'button_placeholder' => $value->button_placeholder ?? '',
            'templates_title' => $value->templates_title ?? '',
        ]);
    }

    public function update()
    {
        $this->validate([
            'hero_title' => 'required',
            'description' => 'required',
            'button_title' => 'required',
            'button_placeholder' => 'required',
            'templates_title' => 'required'
        ]);

        if($this->image)
        {
            $this->validate([
                'image' => 'required|image'
            ]);

            $directory = public_path('uploads/heroimage');
            $tempPath = $this->image->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->image->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/heroimage/'.$this->image->hashName();
            rename($tempPath, $destinationPath);
            $heroimage = '/uploads/heroimage/'.$this->image->hashName().'.'.$extension;
        }else {
            $heroimage = json_decode($this->hero_data->value)->image ?? '';
        }



        $data = [
            'hero_title' => $this->hero_title,
            'image' => $heroimage,
            'description' => $this->description,
            'button_title' => $this->button_title,
            'button_placeholder' => $this->button_placeholder,
            'templates_title' => $this->templates_title
        ];

        $hero_data_option = Option::where('key','hero_data')->first();

        if($hero_data_option)
        {
             $this->hero_data->value = json_encode($data);
             $this->hero_data->save();
        }else{
            $option = new Option();
            $option->key = 'hero_data';
            $option->value = json_encode($data);
            $option->save();

        }

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.settings.hero');
    }
}
