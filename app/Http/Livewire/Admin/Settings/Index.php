<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;
use Jantinnerezo\LivewireAlert\LivewireAlert;

class Index extends Component
{
    public $site_logo, $site_favicon, $settings, $copyright, $service_title, $service_des, $templates_title, $plan_title, $plan_des;
    public $testimonial_title;
    public $promotion_title, $promotion_sub_title, $promotion_icon, $promotion_button;
    public $footer_des;
    public $social_data = [];
    public $login_button_title, $login_button_url;
    public $register_button_title, $register_button_url;
    public $contact_subtitle, $contact_title, $contact_des, $contact_email, $contact_phone, $contact_location;

    use LivewireAlert;

    use WithFileUploads;

    public $fileUploadStatus = true;

    public function updatingSiteLogo($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedSiteLogo()
    {
        if ($this->site_logo && $this->site_logo->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function updatingSiteFavicon($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedSiteFavicon()
    {
        if ($this->site_favicon && $this->site_favicon->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function updatingHowImg($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedHowImg()
    {
        if ($this->how_img && $this->how_img->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function addLink()
    {
        $this->social_data[] = ''; // Add an empty string to the array
    }

    public function removeLink($index)
    {
        unset($this->social_data[$index]); // Remove the social link at the specified index
        $this->social_data = array_values($this->social_data); // Reindex the array after removal
    }

    public function mount()
    {
        $this->settings = Option::where('key', 'site_settings')->first();
        $value = json_decode($this->settings?->value);

        foreach ($value->social_data ?? [] as $key => $item) {
            $this->social_data[] = $item;
        }

        $this->fill([
            'copyright' => $value->copyright ?? '',
            'service_title' => $value->service_title ?? '',
            'service_des' => $value->service_des ?? '',
            'templates_title' => $value->templates_title ?? '',
            'plan_title' => $value->plan_title ?? '',
            'plan_des' => $value->plan_des ?? '',
            'testimonial_title' => $value->testimonial_title ?? '',
            'promotion_title' => $value->promotion_title ?? '',
            'promotion_sub_title' => $value->promotion_sub_title ?? '',
            'promotion_button' => $value->promotion_button ?? '',
            'footer_des' => $value->footer_des ?? '',
            'login_button_title' => $value->login_button_title ?? '',
            'login_button_url' => $value->login_button_url ?? '',
            'register_button_title' => $value->register_button_title ?? '',
            'register_button_url' => $value->register_button_url ?? '',
            'contact_subtitle' => $value->contact_subtitle ?? '',
            'contact_title' => $value->contact_title ?? '',
            'contact_des' => $value->contact_des ?? '',
            'contact_email' => $value->contact_email ?? '',
            'contact_phone' => $value->contact_phone ?? '',
            'contact_location' => $value->contact_location ?? '',
        ]);
    }

    public function update()
    {
        $this->validate([
            'copyright' => 'required',
        ]);

        if ($this->site_logo) {
            $this->validate([
                'site_logo' => 'required|image'
            ]);

            $directory = public_path('uploads/logo');
            $tempPath = $this->site_logo->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->site_logo->getClientOriginalExtension();
            $destinationPath = base_path() . '/public/uploads/logo/' . $this->site_logo->hashName();
            rename($tempPath, $destinationPath);
            $logo = '/uploads/logo/' . $this->site_logo->hashName() . '.' . $extension;
        } else {
            $logo = json_decode($this->settings->value)->site_logo;
        }

        if ($this->site_favicon) {
            $this->validate([
                'site_favicon' => 'required|image'
            ]);

            $directory = public_path('uploads/favicon');
            $tempPath = $this->site_favicon->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->site_favicon->getClientOriginalExtension();
            $destinationPath = base_path() . '/public/uploads/favicon/' . $this->site_favicon->hashName();
            rename($tempPath, $destinationPath);
            $favicon = '/uploads/favicon/' . $this->site_favicon->hashName() . '.' . $extension;
        } else {
            $favicon = json_decode($this->settings->value)->site_favicon;
        }

        if ($this->promotion_icon) {
            $this->validate([
                'promotion_icon' => 'required|image'
            ]);

            $directory = public_path('uploads/promotion_icon');
            $tempPath = $this->promotion_icon->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->promotion_icon->getClientOriginalExtension();
            $destinationPath = base_path() . '/public/uploads/promotion_icon/' . $this->promotion_icon->hashName();
            rename($tempPath, $destinationPath);
            $promotion_icon = '/uploads/promotion_icon/' . $this->promotion_icon->hashName() . '.' . $extension;
        } else {
            $promotion_icon = json_decode($this->settings->value)->promotion_icon;
        }

        $value = json_decode($this->settings?->value);

        $data = [
            'copyright' => $this->copyright,
            'site_logo' => $logo,
            'site_favicon' => $favicon,
            'promotion_icon' => $promotion_icon,
            'service_title' => $this->service_title ?? $value->service_title,
            'service_des' => $this->service_des ?? $value->service_des,
            'templates_title' => $this->templates_title ?? $value->templates_title,
            'plan_title' => $this->plan_title ?? $value->plan_title,
            'plan_des' => $this->plan_des ?? $value->plan_des,
            'testimonial_title' => $this->testimonial_title ?? $value->testimonial_title,
            'promotion_title' => $this->promotion_title ?? $value->promotion_title,
            'promotion_button' => $this->promotion_button ?? $value->promotion_button,
            'social_data' => $this->social_data ?? $value->social_data,
            'footer_des' => $this->footer_des ?? $value->footer_des,
            'login_button_title' => $this->login_button_title ?? $value->login_button_title,
            'login_button_url' => $this->login_button_url ?? $value->login_button_url,
            'register_button_title' => $this->register_button_title ?? $value->register_button_title,
            'register_button_url' => $this->register_button_url ?? $value->register_button_url,
            'promotion_sub_title' => $this->promotion_sub_title ?? $value->promotion_sub_title,
            'contact_subtitle' => $this->contact_subtitle ?? $value->contact_subtitle,
            'contact_title' => $this->contact_title ?? $value->contact_title,
            'contact_des' => $this->contact_des ?? $value->contact_des,
            'contact_email' => $this->contact_email ?? $value->contact_email,
            'contact_phone' => $this->contact_phone ?? $value->contact_phone,
            'contact_location' => $this->contact_location ?? $value->contact_location,
        ];

        $site_settings = Option::where('key', 'site_settings')->first();

        if ($site_settings) {
            $this->settings->key = 'site_settings';
            $this->settings->value = json_encode($data);
            $this->settings->save();
        } else {
            $option = new Option();
            $option->key = 'site_settings';
            $option->value = json_encode($data);
            $option->save();
        }

        $this->alert('success', 'Settings Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }

    public function render()
    {
        return view('livewire.admin.settings.index');
    }
}
