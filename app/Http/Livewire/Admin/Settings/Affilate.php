<?php

namespace App\Http\Livewire\Admin\Settings;

use App\Models\Option;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use Livewire\Component;

class Affilate extends Component
{
    public $affiliate_min_limit, $affiliate_comission;

    use LivewireAlert;

    public function mount()
    {

        $affiliate = Option::where('key', 'affiliate')->first();
        $info = json_decode($affiliate->value ?? '');
        $this->fill([
            'affiliate_min_limit' => $info->affiliate_min_limit ?? '',
            'affiliate_comission' => $info->affiliate_comission ?? ''
        ]);

    }

    public function update()
    {
        $this->validate([
            'affiliate_min_limit' => 'required',
            'affiliate_comission' => 'required',
        ]);

        $affiliate = Option::where('key', 'affiliate')->first();
        if(!$affiliate)
        {
            $affiliate = new Option();
            $affiliate->key = 'affiliate';
        }

        $data = [
            'affiliate_min_limit' => $this->affiliate_min_limit ?? '',
            'affiliate_comission' => $this->affiliate_comission ?? ''
        ];

        $affiliate->value = json_encode($data);
        $affiliate->save();

        $this->alert('success', 'Successfully Updated', [
            'position' => 'top-end',
            'timer' => 3000,
            'toast' => true,
            'timerProgressBar' => true,
            'text' => '',
        ]);

        return redirect()->back();
    }


    public function render()
    {
        return view('livewire.admin.settings.affilate');
    }
}
