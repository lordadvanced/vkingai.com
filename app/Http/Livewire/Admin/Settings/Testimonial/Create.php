<?php

namespace App\Http\Livewire\Admin\Settings\Testimonial;

use App\Models\Term;
use App\Models\Termmeta;
use Livewire\Component;
use Livewire\WithFileUploads;
use DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;

class Create extends Component
{

    public $message, $user_name, $status, $user_profile, $user_position, $user_rating = 1;

    use WithFileUploads;

    public $fileUploadStatus = true;

    public function updatingUserProfile($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedUserProfile()
    {
        if ($this->user_profile && $this->user_profile->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function store()
    {
        $this->validate([
            'message' => 'required',
            'user_name' => 'required',
            'user_profile' => 'required|image',
            'user_position' => 'required',
            'user_rating' => 'required'
        ]);

        DB::beginTransaction();

        try {

            if($this->user_profile)
            {


                $directory = public_path('uploads/testimonial');
                $tempPath = $this->user_profile->getRealPath();
                if (!File::exists($directory)) {
                    File::makeDirectory($directory, 0755, true);
                }

                $extension = $this->user_profile->getClientOriginalExtension();
                $destinationPath = base_path().'/public/uploads/testimonial/'.$this->user_profile->hashName();
                rename($tempPath, $destinationPath);
                $testimonialImg = '/uploads/testimonial/'.$this->user_profile->hashName().'.'.$extension;
            }else {
                $testimonialImg = 'default.png';
            }

            $page = new Term();
            $page->user_id = Auth::User()->id;
            $page->name = $this->user_name;
            $page->slug = Str::slug($this->user_name).Str::random(10);
            $page->status = $this->status ?? 'approved';
            $page->type = 'testimonial';
            $page->save();

            $data = [
                'message' => $this->message,
                'user_position' => $this->user_position,
                'user_profile' => $testimonialImg,
                'user_rating' => $this->user_rating
            ];

            $pagemeta = new Termmeta();
            $pagemeta->term_id = $page->id;
            $pagemeta->key = 'testimonial';
            $pagemeta->value = json_encode($data);
            $pagemeta->save();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }

        return redirect()->route('admin.testimonial.index');

    }

    public function render()
    {
        return view('livewire.admin.settings.testimonial.create');
    }
}
