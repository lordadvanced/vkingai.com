<?php

namespace App\Http\Livewire\Admin\Settings\Features;

use App\Models\Term;
use App\Models\Termmeta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Edit extends Component
{

    public $title, $image, $status;
    public $description;
    public $feature;

    use WithFileUploads;

    public $fileUploadStatus = true;

    public function updatingImage($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedImage()
    {
        if ($this->image && $this->image->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function mount($id)
    {
        $feature = Term::with('featureMeta')->find($id);
        $this->feature = $feature;
        $this->status = $feature->status;
        $this->fill([
            'description' => json_decode($feature->featureMeta->value)->description ?? '',
            'title' => $feature->name
        ]);
    }

    public function update($id)
    {

        if($this->image)
        {
            $this->validate([
                'image' => 'required|image'
            ]);

            $directory = public_path('uploads/features');
            $tempPath = $this->image->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->image->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/features/'.$this->image->hashName();
            rename($tempPath, $destinationPath);
            $featuresImg = '/uploads/features/'.$this->image->hashName().'.'.$extension;
        }else {
            $featuresImg = json_decode($this->feature->featureMeta->value)->image;
        }

        $features = Term::find($id);
        $features->name = $this->title;
        $features->status = $this->status ?? 'approved';
        $features->save();

        $data = [
            'image' => $featuresImg,
            'description' => $this->description
        ];

        $featuresMeta = Termmeta::where('term_id',$features->id)->first();
        $featuresMeta->value = json_encode($data);
        $featuresMeta->save();

        return redirect()->route('admin.features.index');
    }

    public function render()
    {
        return view('livewire.admin.settings.features.edit');
    }
}
