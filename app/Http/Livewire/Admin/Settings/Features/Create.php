<?php

namespace App\Http\Livewire\Admin\Settings\Features;

use App\Models\Term;
use App\Models\Termmeta;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Str;

class Create extends Component
{

    public $title, $image, $status;
    public $description;

    use WithFileUploads;

    public $fileUploadStatus = true;

    public function updatingImage($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedImage()
    {
        if ($this->image && $this->image->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function store()
    {
        $this->validate([
            'image' => 'required|image',
            'title' => 'required',
            'description' => 'required'
        ]);

        if($this->image)
        {
            $this->validate([
                'image' => 'required|image'
            ]);

            $directory = public_path('uploads/features');
            $tempPath = $this->image->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->image->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/features/'.$this->image->hashName();
            rename($tempPath, $destinationPath);
            $featuresImg = '/uploads/features/'.$this->image->hashName().'.'.$extension;
        }else {
            $featuresImg = 'default.png';
        }

        $features = new Term();
        $features->user_id = Auth::User()->id;
        $features->name = $this->title;
        $features->slug = Str::slug($this->title).Str::random(10);
        $features->status = $this->status ?? 'approved';
        $features->type = 'features';
        $features->save();

        $data = [
            'image' => $featuresImg,
            'description' => $this->description
        ];

        $featuresMeta = new Termmeta();
        $featuresMeta->term_id = $features->id;
        $featuresMeta->key = 'features';
        $featuresMeta->value = json_encode($data);
        $featuresMeta->save();

        return redirect()->route('admin.features.index');

    }

    public function render()
    {
        return view('livewire.admin.settings.features.create');
    }
}
