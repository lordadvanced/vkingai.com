<?php

namespace App\Http\Livewire\Admin\Settings\Faq;

use App\Models\Term;
use App\Models\Termmeta;
use Livewire\Component;

class Edit extends Component
{

    public $title, $status;
    public $description;
    public $faq;

    public function mount($id)
    {
        $faq = Term::with('faqMeta')->find($id);
        $this->faq = $faq;
        $this->status = $faq->status;
        $this->fill([
            'description' => json_decode($faq->faqMeta->value)->description ?? '',
            'title' => $faq->name
        ]);
    }

    public function update($id)
    {

        $faq = Term::find($id);
        $faq->name = $this->title;
        $faq->status = $this->status ?? 'approved';
        $faq->save();

        $data = [
            'description' => $this->description
        ];

        $faqMeta = Termmeta::where('term_id',$faq->id)->first();
        $faqMeta->value = json_encode($data);
        $faqMeta->save();

        return redirect()->route('admin.faq.index');
    }

    public function render()
    {
        return view('livewire.admin.settings.faq.edit');
    }
}
