<?php

namespace App\Http\Livewire\Admin\Settings\Faq;

use App\Models\Term;
use App\Models\Termmeta;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Illuminate\Support\Str;

class Create extends Component
{
    public $title, $status;
    public $description;


    public function store()
    {
        $this->validate([
            'title' => 'required',
            'description' => 'required'
        ]);

        $features = new Term();
        $features->user_id = Auth::User()->id;
        $features->name = $this->title;
        $features->slug = Str::slug($this->title).Str::random(10);
        $features->status = $this->status ?? 'approved';
        $features->type = 'faq';
        $features->save();

        $data = [
            'description' => $this->description
        ];

        $featuresMeta = new Termmeta();
        $featuresMeta->term_id = $features->id;
        $featuresMeta->key = 'faq';
        $featuresMeta->value = json_encode($data);
        $featuresMeta->save();

        return redirect()->route('admin.faq.index');

    }

    public function render()
    {
        return view('livewire.admin.settings.faq.create');
    }
}
