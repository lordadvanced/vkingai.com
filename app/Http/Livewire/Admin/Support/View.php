<?php

namespace App\Http\Livewire\Admin\Support;

use App\Models\Support;
use App\Models\SupportMeta;
use App\Models\User;
use Livewire\Component;

class View extends Component
{

    public $support;
    public $messages;
    public $user;
    public $comment;

    public function mount($id)
    {
        $support = Support::findOrFail($id);
        $this->support = $support;
        $messages = SupportMeta::where('support_id', $support->id)->get();
        $this->messages = $messages;
        $user = User::findOrFail($support->user_id);
        $this->user['name'] = $user->name;
        $this->user['profile'] = $user->profile;
    }

    public function store(){

        if(!$this->comment)
        {
            return redirect()->back()->withErrors(['comment' => 'The Message Filed is Required!']);
        }
        $supportMessage = new SupportMeta();
        $supportMessage->support_id = $this->support->id;
        $supportMessage->type = 'owner';
        $supportMessage->message = $this->comment;
        $supportMessage->save();

        return redirect()->route('admin.support.view', $this->support->id);


    }

    public function render()
    {
        return view('livewire.admin.support.view');
    }
}
