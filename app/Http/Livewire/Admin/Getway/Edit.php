<?php

namespace App\Http\Livewire\Admin\Getway;

use Livewire\Component;
use App\Models\Getway;
use Illuminate\Support\Facades\Artisan;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\File;


class Edit extends Component
{

    public $getway, $name, $rate, $charge, $namespace, $currency_name, $image_accept, $is_auto, $test_mode, $status, $phone_required, $instruction, $data = [];

    use WithFileUploads;

    public $logo;

    public $fileUploadStatus = true;

    public function updatingLogo($value)
    {
        $this->fileUploadStatus = false;
    }

    public function updatedLogo()
    {
        $this->fileUploadStatus = false;

        if ($this->logo && $this->logo->isValid()) {
            $this->fileUploadStatus = true;
        } else {
            $this->fileUploadStatus = false;
        }
    }

    public function mount($id)
    {

        $this->getway = Getway::find($id);

        $this->fill([
            'name' => $this->getway->name,
            'rate' => $this->getway->rate,
            'charge' => $this->getway->charge,
            'test_mode' => $this->getway->test_mode,
        ]);

        if(json_decode($this->getway->data) != null)
        {
            foreach(json_decode($this->getway->data) as $key => $value) {
                $this->data[$key] = str_replace(' ', '', $value);
            }
        }

        $this->status = $this->getway->status;
    }

    public function update()
    {
        $this->validate([
            'name' => 'required|string|max:255',
            'logo' => 'nullable|image|max:255',
            'charge' => 'required|integer',
            'test_mode' => 'required|boolean',
            'status' => 'required|string|max:255',
            'data' => 'nullable'
        ]);

        if($this->logo)
        {
            $directory = public_path('uploads/getway');
            $tempPath = $this->logo->getRealPath();
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0755, true);
            }

            $extension = $this->logo->getClientOriginalExtension();
            $destinationPath = base_path().'/public/uploads/getway/'.$this->logo->hashName();
            rename($tempPath, $destinationPath);
            $logo = '/uploads/getway/'.$this->logo->hashName().'.'.$extension;
        }else {
            $logo = $this->getway->logo;
        }

        $this->getway->update([
            'name' => $this->name,
            'logo' => $logo,
            'charge' => $this->charge,
            'test_mode' => $this->test_mode,
            'status' => $this->status,
            'data' => json_encode($this->data)
        ]);

        foreach($this->data as $key => $value) {
            Artisan::call("env:set ".$key."='".str_replace(' ', '', $value)."'");
        }

        return redirect()->route('admin.gateway.index');
    }

    public function render()
    {
        return view('livewire.admin.getway.edit');
    }
}
