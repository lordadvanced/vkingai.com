<?php

namespace App\Http\Livewire\Admin\User;

use DB;
use Hash;
use App\Models\User;
use Livewire\Component;
use App\Models\Usermeta;
use App\Models\Plan;
use Carbon\Carbon;

class Edit extends Component
{

    public $user, $name, $email, $status, $password, $password_confirmation;
    public $plans, $wordLimit, $current_plan;

    public function mount($id)
    {


        $user = User::find($id);
        $info = json_decode($user->data);

        $this->user = $user;
        $this->fill([
            'name' => $user->name,
            'email' => $user->email
        ]);

        $this->status = $user->status;
        $this->current_plan = $user->plan_id;
        $plans = Plan::where('status', 'approved')->latest()->get();
        $this->plans = $plans;
    }

    public function update($id)
    {


        $this->validate([
            'name' => 'required',
            'email' => 'unique:users,email,'.$this->user->id
        ]);

        if($this->password)
        {
            $this->validate([
                'password' => 'required|confirmed'
            ]);
        }


        $plan = Plan::where([
            ['status', 'approved'],
            ['id', $this->current_plan]
        ])->first();

        $info = json_decode($plan->data);

        $data = [
            'word_limit' => $info->word_limit,
            'templates_limit' => $info->templates_limit,
            'chatbot_limit' => $info->chatbot_limit,
            'ai_templates' => $info->ai_templates,
            'ai_chatbot' => $info->ai_chatbot,
            'ai_images' => $info->ai_images,
            'access_to_gpt3' => $info->access_to_gpt3,
            'accept_to_gpt4' => $info->accept_to_gpt4,
            'ai_speech_to_text' => $info->ai_speech_to_text,
            'ai_code' => $info->ai_code,
            'image_limit' => $info->image_limit,
            'speech_to_text_limit' => $info->speech_to_text_limit,
            'audio_file_size' => $info->audio_file_size,
            'trail_days' => $info->trail_days,
            'accept_trail' => $plan->accept_trail,
            'use_word_limit' => 0,
            'use_image_limit' => 0,
            'use_speech_to_text_limit' => 0
        ];

        if($plan->duration_type == 'monthly')
        {
            $date = 30;
        }else{
            $date = 365;
        }

        if($plan->accept_trail)
        {
            $date = $info->trail_days;
        }

        $date = Carbon::now()->addDays($date);

        $user = User::find($id);
        $user->name = $this->name ?? $user->name;
        $user->plan_id = $this->current_plan ?? $user->plan_id;
        $user->email = $this->email ?? $user->email;
        if($this->password)
        {
            $user->password = Hash::make($this->password);
        }
        $user->data = json_encode($data);
        $user->will_expire = $date;
        $user->status = $this->status ?? $user->status;
        $user->save();

        return redirect()->route('admin.user.index');
    }

    public function render()
    {
        return view('livewire.admin.user.edit');
    }
}
