<?php

namespace App\Http\Livewire\Admin\Plan;

use App\Models\Plan;
use Livewire\Component;
use Illuminate\Support\Str;

class Edit extends Component
{
    public $name, $plan, $price, $durationType = 'monthly', $status = 0, $accept_trail, $short_content;
    public $word_limit, $ai_templates = 1, $templates_limit;
    public $ai_chatbot = 1, $chatbot_limit;
    public $ai_images = 1, $image_limit;
    public $ai_speech_to_text = 1, $speech_to_text_limit, $audio_file_size;
    public $ai_code = 1;
    public $is_recommended = 0;
    public $trail_days;
    public $access_to_gpt3, $accept_to_gpt4;

    protected $rules = [
      'name' => 'required',
      'price' => 'required|numeric',
      'durationType' => 'required',
      'word_limit' => 'required|numeric',
      'status' => 'required'
    ];

    public function updatedAccessToGpt3($status)
    {
        if($status == 1)
        {
            $this->accept_to_gpt4 = 0;
        }else {
            $this->accept_to_gpt4 = 1;
        }
    }

    public function updatedAcceptToGpt4($status)
    {
        if($status == 1)
        {
            $this->access_to_gpt3 = 0;
        }else {
            $this->access_to_gpt3 = 1;
        }
    }


    public function mount($id)
    {
        $plan = Plan::find($id);
        $this->plan = $plan;
        $info = json_decode($plan->data);
        $this->fill([
            'name' => $plan->name,
            'word_limit' => $info->word_limit ?? '',
            'price' => $plan->price,
            'short_content' => $info->short_content ?? '',
            'chatbot_limit' => $info->chatbot_limit ?? '',
            'templates_limit' => $info->templates_limit ?? '',
            'image_limit' => $info->image_limit ?? '',
            'speech_to_text_limit' => $info->speech_to_text_limit ?? '',
            'audio_file_size' => $info->audio_file_size ?? '',
            'trail_days' => $info->trail_days ?? 0,
        ]);

        if($plan->status === 'approved')
        {
            $this->status = 1;
        }else{
            $this->status = 0;

        }
        if($info->is_recommended)
        {
            $this->is_recommended = 1;
        }else {
            $this->is_recommended = 0;
        }
        $this->durationType = $plan->duration_type;

        if($plan->accept_trail)
        {
            $this->accept_trail = 1;
        }else {
            $this->accept_trail = 0;
        }

        $this->ai_templates = $info->ai_templates ?? 0;
        $this->access_to_gpt3 = $info->access_to_gpt3 ?? 1;
        $this->accept_to_gpt4 = $info->accept_to_gpt4 ?? 0;
        $this->ai_chatbot = $info->ai_chatbot ?? 0;
        $this->ai_images = $info->ai_images ?? 0;
        $this->ai_speech_to_text = $info->ai_speech_to_text ?? 0;
        $this->ai_code = $info->ai_code ?? 0;

    }

    public function update($id)
    {
        $this->validate();

        if($this->ai_templates)
        {
            $this->validate([
                'templates_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_chatbot)
        {
            $this->validate([
                'chatbot_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_images)
        {
            $this->validate([
                'image_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_speech_to_text)
        {
            $this->validate([
                'speech_to_text_limit' => 'required|numeric',
                'audio_file_size' => 'required|numeric'
            ]);
        }

        $plan = Plan::find($id);
        $info = json_decode($plan->data);

        if($this->name)
        {
            $slug = Str::slug($this->name);
            if($plan->slug != $slug)
            {
                $plan_check = Plan::where('slug',$slug)->first();
                if($plan_check)
                {
                    $slug = Str::slug($this->name).Str::random(10);
                }
            }else{
                $slug = $plan->slug;
            }
        }


        $plan->name = $this->name ?? $plan->name;
        $plan->slug = $slug;
        $plan->price = $this->price ?? $plan->price;
        $plan->duration_type = $this->durationType ?? $plan->duration_type;
        if($this->accept_trail)
        {
            $this->validate([
                'trail_days' => 'required|numeric'
            ]);
            $plan->accept_trail = 1;
        }else {
            $plan->accept_trail = 0;
        }

        $data = [
            'word_limit' => $this->word_limit ?? $info->word_limit,
            'templates_limit' => $this->templates_limit ?? $info->templates_limit,
            'chatbot_limit' => $this->chatbot_limit ?? $info->chatbot_limit,
            'ai_templates' => $this->ai_templates ? 1 : 0,
            'ai_chatbot' => $this->ai_chatbot ? 1 : 0,
            'ai_images' => $this->ai_images ? 1 : 0,
            'access_to_gpt3' => $this->access_to_gpt3 ? 1 : 0,
            'accept_to_gpt4' => $this->accept_to_gpt4 ? 1 : 0,
            'ai_speech_to_text' => $this->ai_speech_to_text ? 1 : 0,
            'ai_code' => $this->ai_code ? 1 : 0,
            'image_limit' => $this->image_limit ?? 0,
            'short_content' => $this->short_content ?? $info->short_content,
            'speech_to_text_limit' => $this->speech_to_text_limit ?? $info->speech_to_text_limit,
            'audio_file_size' => $this->audio_file_size ?? $info->audio_file_size,
            'is_recommended' => $this->is_recommended ?? $info->is_recommended,
            'trail_days' => $this->trail_days ?? $info->trail_days,
        ];

        $plan->data = json_encode($data);
        if($this->status)
        {
            $plan->status = 'approved';
        }else{
            $plan->status = 'pending';
        }
        $plan->save();


        return redirect()->route('admin.plan.index');

    }

    public function render()
    {
        return view('livewire.admin.plan.edit');
    }
}
