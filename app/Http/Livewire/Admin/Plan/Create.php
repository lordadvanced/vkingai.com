<?php

namespace App\Http\Livewire\Admin\Plan;

use App\Models\Plan;
use Livewire\Component;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;


class Create extends Component
{
    public $name, $price, $durationType = 'monthly', $status = 0, $accept_trail, $short_content;
    public $word_limit, $ai_templates = 1, $templates_limit;
    public $ai_chatbot = 1, $chatbot_limit;
    public $ai_images = 1, $image_limit;
    public $ai_speech_to_text = 1, $speech_to_text_limit, $audio_file_size;
    public $ai_code = 1;
    public $is_recommended = 0;
    public $trail_days;
    public $access_to_gpt3, $accept_to_gpt4;

    protected $rules = [
      'name' => 'required',
      'price' => 'required|numeric',
      'durationType' => 'required',
      'word_limit' => 'required|numeric',
      'status' => 'required'
    ];

    public function updatedAccessToGpt3($status)
    {
        if($status == 1)
        {
            $this->accept_to_gpt4 = 0;
        }else {
            $this->accept_to_gpt4 = 1;
        }
    }

    public function updatedAcceptToGpt4($status)
    {
        if($status == 1)
        {
            $this->access_to_gpt3 = 0;
        }else {
            $this->access_to_gpt3 = 1;
        }
    }

    public function mount()
    {
        $this->status = 1;
        $this->ai_templates = 1;
        $this->ai_chatbot = 1;
        $this->ai_images = 1;
        $this->ai_speech_to_text = 1;
        $this->ai_code = 1;
        $this->access_to_gpt3 = 1;
    }

    public function store()
    {
        $this->validate();

        if($this->ai_templates)
        {
            $this->validate([
                'templates_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_chatbot)
        {
            $this->validate([
                'chatbot_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_images)
        {
            $this->validate([
                'image_limit' => 'required|numeric'
            ]);
        }

        if($this->ai_speech_to_text)
        {
            $this->validate([
                'speech_to_text_limit' => 'required|numeric',
                'audio_file_size' => 'required|numeric'
            ]);
        }

        $slug = Str::slug($this->name);
        $plan_check = Plan::where('slug',$slug)->first();
        if($plan_check)
        {
            $slug = Str::slug($this->name).Str::random(10);
        }

        if($this->accept_trail)
        {
            $this->validate([
                'trail_days' => 'required|numeric'
            ]);
        }

        $plan = new Plan();
        $plan->name = $this->name;
        $plan->slug = $slug;
        $plan->price = $this->price;
        $plan->duration_type = $this->durationType;
        if($this->accept_trail)
        {
           $plan->accept_trail = 1;
        }else {
           $plan->accept_trail = 0;
        }

        $data = [
            'word_limit' => $this->word_limit,
            'ai_templates' => $this->ai_templates ? 1 : 0,
            'templates_limit' => $this->templates_limit ?? 0,
            'ai_chatbot' => $this->ai_chatbot ? 1 : 0,
            'chatbot_limit' => $this->chatbot_limit ?? 0,
            'ai_images' => $this->ai_images ? 1 : 0,
            'image_limit' => $this->image_limit ?? 0,
            'ai_speech_to_text' => $this->ai_speech_to_text ? 1 : 0,
            'speech_to_text_limit' => $this->speech_to_text_limit ?? 0,
            'audio_file_size' => $this->audio_file_size ?? 0,
            'ai_code' => $this->ai_code ? 1 : 0,
            'short_content' => $this->short_content,
            'is_recommended' => $this->is_recommended ? 1 : 0,
            'trail_days' => $this->trail_days ?? 0,
            'access_to_gpt3' => $this->access_to_gpt3 ? 1 : 0,
            'accept_to_gpt4' => $this->accept_to_gpt4 ? 1 : 0
        ];

        $plan->data = json_encode($data);
        if($this->status)
        {
            $plan->status = 'approved';
        }else {
            $plan->status = 'pending';
        }
        $plan->save();

        return redirect()->route('admin.plan.index');
    }

    public function render()
    {
        return view('livewire.admin.plan.create');
    }
}
