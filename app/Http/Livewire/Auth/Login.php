<?php

namespace App\Http\Livewire\Auth;

use App\Models\User;
use Livewire\Component;
use Session;
use App\Models\Option;

class Login extends Component
{

    public $email = '';
    public $password = '';
    public $remember_me = false;
    public $logo;

    protected $rules = [
        'email' => 'required|email',
        'password' => 'required|min:6',
    ];

    //This mounts the default credentials for the admin. Remove this section if you want to make it public.
    public function mount()
    {
        if (auth()->user()) {
            if(auth()->user()->type === 'admin')
            {
                return redirect()->intended('/admin/dashboard');
            }else {
                return redirect()->intended('/user/dashboard');
            }
        }

        $settings = json_decode(Option::where('key', 'site_settings')->first()->value);

        $this->logo = $settings->site_logo;
    }

    public function login()
    {

        $credentials = $this->validate();
        if (auth()->attempt(['email' => $this->email, 'password' => $this->password], $this->remember_me)) {
            $user = User::where(['email' => $this->email])->first();
            auth()->login($user, $this->remember_me);

            return redirect()->intended('/admin/dashboard');

        } else {
            return $this->addError('email', trans('auth.failed'));
        }
    }

    public function render()
    {
        return view('livewire.auth.login')
                ->layout('layouts.root');
    }
}
