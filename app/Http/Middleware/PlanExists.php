<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanExists
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        // Exclude the "/user/plan" route from the middleware
        if ($request->is('user/plan')) {
            return $next($request);
        }

        if (Auth::user() && Auth::user()->plan_id == null) {
            return redirect('/user/plan');
        }

        $expireDate = Carbon::parse(Auth::user()->will_expire);

        if(Auth::user() && $expireDate->isPast())
        {
            return redirect('/user/plan');
        }

        return $next($request);
    }
}
