<?php

namespace App\Classes;

class Chatbot {

    public static function prompt($name, $content, $message)
    {
        return "I want you to act as a chatbot that I am having a conversation with you. Your name is ".$name.". You will chat with me as a ".$name.". I will give you some web pages and you will answer me from these web pages. If you don't find any answer from given web pages then you will say i don't know or something. Don't say 'A:' in your response.\n\n
        The web Pages: ".$content." \n

        Q: ".$message." \n
        A: ? \n
        And proceed to craft your response accordingly.
        ";
    }

    public static function conveter($num) {
        if($num>1000) {
              $x = round($num);
              $x_number_format = number_format($x);
              $x_array = explode(',', $x_number_format);
              $x_parts = array('k+', 'm+', 'b+', 't+');
              $x_count_parts = count($x_array) - 1;
              $x_display = $x;
              $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
              $x_display .= $x_parts[$x_count_parts - 1];

              return $x_display;

        }

        return $num;
    }

}
