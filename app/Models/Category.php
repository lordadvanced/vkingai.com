<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public function templates(){
        return $this->belongsToMany('App\Models\Template')->where([
            ['status', 'approved']
        ]);
    }

    public function blogs()
    {
        return $this->belongsToMany('App\Models\Term')->where([
            ['status', 'approved'],
            ['type', 'blog']
        ]);
    }
}
