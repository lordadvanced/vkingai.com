<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transcribe extends Model
{
    use HasFactory;
    protected $table = 'transcribes';
}
