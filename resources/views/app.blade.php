<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title inertia>{{ config('app.name', 'Laravel') }}</title>

        @php
            $site_info = json_decode(App\Models\Option::where('key', 'site_settings')->first()->value);
            $global = json_decode(App\Models\Option::where('key', 'global_settings')->first()->value);
        @endphp

        <link rel="icon" type="image/x-icon" href="{{ $site_info->site_favicon }}">
        <link rel="preload" href="/frontend/font/Hiragino-Kaku-Gothic-Std.otf" as="font" type="font/otf" crossorigin>

        @php
        @endphp
        <!-- Scripts -->
        @routes
        @viteReactRefresh
        @vite(['resources/js/app.jsx', "resources/js/Pages/{$page['component']}.jsx"])
        @inertiaHead
    </head>
    <body>
        @inertia
    </body>
</html>
