<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ __('404 Page Not Found.') }}</title>

    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/404.css') }}">
</head>
<body>

    <div class="container mx-auto">
        <div class=" w-full h-screen flex items-center justify-center">
            <div class=" text-center">
                <img class="h-96 w-full object-cover" src="/frontend/img/404.png" alt="">
                <h2 class="text-2xl my-8 text-gray-500 font-mono">{{ __('OPPS!! Page Not Found.') }}</h2>
                <a href="/" class="bg-gradient text-white px-10 py-3.5 text-lg rounded-full font-mono flex items-center justify-center space-x-4 max-w-max mx-auto"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
                  </svg> <span>{{ __('Go Back') }}</span>
                </a>
            </div>
        </div>
    </div>
</body>
</html>


