<div>
    @section('title', 'Basic Settings')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Site Settings') }}</li>
                </ol>
            </nav>
            <h2 class="h4">{{ __('Basic Settings') }}</h2>
            <p class="mb-0">{{ __('Here you can update basic settings for your website.') }}</p>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>
    <div>
        <div class="col-lg-12" x-data="{ isUploading: false, progress: 0 }"
        x-on:livewire-upload-start="isUploading = true"
        x-on:livewire-upload-finish="isUploading = false"
        x-on:livewire-upload-error="isUploading = false"
        x-on:livewire-upload-progress="progress = $event.detail.progress">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4">{{ __('Site Settings') }}</h2>
                <form wire:submit.prevent="update">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="name" class="w-full">{{ __('Site Logo') }} <span class="custom-percentage" x-show="isUploading" x-text="'(' +progress + '%)'"></span></label>
                                <input class="form-control @error('site_logo') error @enderror" id="site_logo" type="file" placeholder="Enter Name" wire:model.lazy="site_logo">
                                @error('site_logo') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="name" class="w-full">{{ __('Site Favicon') }}</label>
                                <input class="form-control @error('site_favicon') error @enderror" id="site_favicon" type="file" placeholder="Enter Name" wire:model.lazy="site_favicon">
                                @error('site_favicon') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="copyright">{{ __('Copyright') }}</label>
                                <input class="form-control @error('copyright') error @enderror" id="copyright" type="text" placeholder="Copyright © 2022. All Rights Reserved." wire:model.lazy="copyright">
                                @error('copyright') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Header Section') }}</h2>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="login_button_title">{{ __('Login Button Name') }}</label>
                                <input class="form-control @error('login_button_title') error @enderror" id="login_button_title" type="text"  wire:model.lazy="login_button_title">
                                @error('login_button_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="login_button_url">{{ __('Login Button Url') }}</label>
                                <input class="form-control @error('login_button_url') error @enderror" id="login_button_url" type="text"  wire:model.lazy="login_button_url">
                                @error('login_button_url') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="register_button_title">{{ __('Register Button Name') }}</label>
                                <input class="form-control @error('register_button_title') error @enderror" id="register_button_title" type="text"  wire:model.lazy="register_button_title">
                                @error('register_button_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="register_button_url">{{ __('Register Button Url') }}</label>
                                <input class="form-control @error('register_button_url') error @enderror" id="register_button_url" type="text"  wire:model.lazy="register_button_url">
                                @error('register_button_url') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>

                    <h2 class="h5 mt-5 mb-4">{{ __('Service Section') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="service_title">{{ __('Service Title') }}</label>
                                <input class="form-control @error('service_title') error @enderror" id="service_title" type="text"
                                wire:model.lazy="service_title">
                                @error('service_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="service_des">{{ __('Services Description') }}</label>
                                <textarea wire:model.lazy="service_des" id="service_des" cols="30" rows="5" class="form-control " spellcheck="false"></textarea>
                                @error('service_des') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Templates Section') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="templates_title">{{ __('Templates Title') }}</label>
                                <input class="form-control @error('templates_title') error @enderror" id="templates_title" type="text" wire:model.lazy="templates_title">
                                @error('templates_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Pricing Plan') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="plan_title">{{ __('Plan Title') }}</label>
                                <input class="form-control @error('plan_title') error @enderror" id="plan_title" type="text" placeholder="Pricing Plans" wire:model.lazy="plan_title">
                                @error('plan_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="plan_des">{{ __('Plan Description') }}</label>
                                <textarea class="form-control @error('plan_des') error @enderror" id="plan_des" rows="4" type="text" wire:model.lazy="plan_des"></textarea>
                                @error('plan_des') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Testimonial Section') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="testimonial_title">{{ __('Testimonial Title') }}</label>
                                <input class="form-control @error('testimonial_title') error @enderror" id="testimonial_title" type="text" wire:model.lazy="testimonial_title">
                                @error('testimonial_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Promotion Section') }}</h2>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="promotion_title">{{ __('Promotion Title') }}</label>
                                <input class="form-control @error('promotion_title') error @enderror" id="promotion_title" type="text" wire:model.lazy="promotion_title">
                                @error('promotion_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="promotion_sub_title">{{ __('Promotion Sub Title') }}</label>
                                <input class="form-control @error('promotion_sub_title') error @enderror" id="promotion_sub_title" type="text" wire:model.lazy="promotion_sub_title">
                                @error('promotion_sub_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="promotion_icon">{{ __('Promotion Icon') }}</label>
                                <input class="form-control @error('promotion_icon') error @enderror" id="promotion_icon" type="file" wire:model="promotion_icon">
                                @error('promotion_icon') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="promotion_button">{{ __('Promotional Button Title') }}</label>
                                <input class="form-control @error('promotion_button') error @enderror" id="promotion_button" type="text" wire:model.lazy="promotion_button">
                                @error('promotion_button') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Footer Section') }}</h2>
                    <div class="col-md-12 mb-3">
                        <div>
                            <label for="footer_des">{{ __('Footer Description') }}</label>
                            <textarea wire:model.lazy="footer_des" id="footer_des" cols="30" rows="5" class="form-control " spellcheck="false"></textarea>
                            @error('footer_des') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <h2 class="h5 mt-5 mb-4">{{ __('Contact Page') }}</h2>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="contact_subtitle">{{ __('Subtitle') }}</label>
                                <input wire:model.lazy="contact_subtitle" id="contact_subtitle" class="form-control"/>
                                @error('contact_subtitle') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="contact_title">{{ __('Title') }}</label>
                                <input wire:model.lazy="contact_title" id="contact_title" class="form-control"/>
                                @error('contact_title') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="contact_des">{{ __('Description') }}</label>
                                <textarea wire:model.lazy="contact_des" id="contact_des" cols="30" rows="5" class="form-control"></textarea>
                                @error('contact_des') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="contact_email">{{ __('Email') }}</label>
                                <input wire:model.lazy="contact_email" id="contact_email" class="form-control"/>
                                @error('contact_email') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="contact_phone">{{ __('Phone') }}</label>
                                <input wire:model.lazy="contact_phone" id="contact_phone" class="form-control"/>
                                @error('contact_phone') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="contact_location">{{ __('Location') }}</label>
                                <input wire:model.lazy="contact_location" id="contact_location" class="form-control"/>
                                @error('contact_location') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="social-links">
                            <div class="col-md-12 mt-5 mb-3">
                                <div class=" d-flex justify-content-between align-items-center">
                                    <h2 class="h5">{{ __('Social Links') }}</h2>
                                    <button type="button" wire:click="addLink" class=" btn btn-gray-600 add-link">Add</button>
                                </div>
                            </div>
                            @foreach ($social_data as $key => $data)
                                <div class="social-link">
                                    <div class="d-flex justify-content-between mb-3">
                                        <div class="d-flex items-center space-x-5 w-full">
                                            <input class="form-control mr-2" type="text" wire:model="social_data.{{ $key }}.link">
                                            <input class="form-control mr-2" type="text" wire:model="social_data.{{ $key }}.url">
                                        </div>
                                        <button type="button" wire:click="removeLink({{ $key }})" class="btn btn-red-500 remove-link"><i class="fa fa-trash text-white"></i></button>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <hr>
                    <div class="mt-4">
                        <div class="f-right">
                            <button x-show="isUploading" disabled type="submit" class="btn btn-gray-300 mt-2 animate-up-2">{{ __('Submit') }}</button>
                            <button x-show="!isUploading" type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Submit') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

