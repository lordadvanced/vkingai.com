<div>
    @section('title', 'Privacy Policy & Terms Of Condition')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Privacy Policy and Terms Of Condition Settings') }}</li>
                </ol>
            </nav>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('Privacy Policy and Terms Of Condition Settings') }}</h2>
            <p class="mb-0">{{ __('Here you can update basic site Settings.') }}</p>
        </div>
        <div class="col-lg-7">
            <div class="card card-body border-0 shadow mb-4" x-data="{ isUploading: false, progress: 0, isUploaded: false, progrs: 0 }">
                <h2 class="h5 mb-4">{{ __('Privacy Policy and Terms Of Condition Settings') }}</h2>
                <form wire:submit.prevent="update">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="login" wire:model="showLoginPage">
                                <label class="form-check-label" for="login">{{ __('Show on the Register Page') }}</label>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="privacy_policy">{{ __('Privacy Policy') }}</label>
                                <textarea class="form-control @error('privacy_policy') error @enderror" id="privacy_policy" rows="8" wire:model.lazy="privacy_policy"></textarea>
                                @error('privacy_policy') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="terms_of_condition">{{ __('Terms & Conditions') }}</label>
                                <textarea class="form-control @error('terms_of_condition') error @enderror" id="terms_of_condition" rows="8" wire:model.lazy="terms_of_condition"></textarea>
                                @error('terms_of_condition') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="f-right">
                            <button type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
