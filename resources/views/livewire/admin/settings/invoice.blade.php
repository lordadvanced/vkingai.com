<div>
    @section('title', 'Invoice Settings')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Basic Settings') }}</li>
                </ol>
            </nav>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('Invoice Settings') }}</h2>
            <p class="mb-0">{{ __('Here you can update Invoice Settings.') }}</p>
        </div>
        <div class="col-lg-7">
            <div class="card card-body border-0 shadow mb-4" x-data="{ isUploading: false, progress: 0, isUploaded: false, progrs: 0 }">
                <h2 class="h5 mb-4">{{ __('Billing Settings') }}</h2>
                <form wire:submit.prevent="update">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="invoice_name">{{ __('Invoice Name') }}</label>
                                <input class="form-control @error('invoice_name') error @enderror" id="invoice_name" type="text" wire:model.lazy="invoice_name">
                                @error('invoice_name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="invoice_address">{{ __('Address') }}</label>
                                <input class="form-control @error('invoice_address') error @enderror" rows="4" id="invoice_address" wire:model.lazy="invoice_address"></textarea>
                                @error('invoice_address') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="invoice_city">{{ __('City') }}</label>
                                <input class="form-control @error('invoice_city') error @enderror" id="invoice_city" wire:model.lazy="invoice_city"></input>
                                @error('invoice_city') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="invoice_state">{{ __('State') }}</label>
                                <input class="form-control @error('invoice_state') error @enderror" id="invoice_state" wire:model.lazy="invoice_state"></input>
                                @error('invoice_state') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="postal_code">{{ __('Postal Code') }}</label>
                                <input class="form-control @error('postal_code') error @enderror" id="postal_code" type="number" wire:model.lazy="postal_code">
                                @error('postal_code') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="country">{{ __('Country') }}</label>
                                <input class="form-control @error('country') error @enderror" id="country" type="text" wire:model.lazy="country">
                                @error('country') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="tax">{{ __('Invoice Tax(%)') }}</label>
                                <input class="form-control @error('tax') error @enderror" id="tax" type="number" wire:model.lazy="tax">
                                @error('tax') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="f-right">
                            <button type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
