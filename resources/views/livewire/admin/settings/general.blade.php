<div>
    @section('title', 'General Settings')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Basic Settings') }}</li>
                </ol>
            </nav>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('General Settings') }}</h2>
            <p class="mb-0">{{ __('Here you can update general site Settings.') }}</p>
        </div>
        <div class="col-lg-7">
            <div class="card card-body border-0 shadow mb-4" x-data="{ isUploading: false, progress: 0, isUploaded: false, progrs: 0 }">
                <h2 class="h5 mb-4">{{ __('Global Settings') }}</h2>
                <form wire:submit.prevent="update">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" type="checkbox" id="emailVerification" wire:model="emailVerification">
                                <label class="form-check-label" for="emailVerification">{{ __('Active Email Verification') }}</label>
                            </div>
                        </div>

                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="site_name">{{ __('Site Name') }}</label>
                                <input class="form-control @error('site_name') error @enderror" id="site_name" type="text" wire:model="siteName">
                                @error('siteName') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="site_url">{{ __('Site URL') }}</label>
                                <input class="form-control @error('site_url') error @enderror" id="site_url" type="text" wire:model="siteUrl">
                                @error('siteUrl') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="site_email">{{ __('Site Email') }}</label>
                                <input class="form-control @error('site_email') error @enderror" id="site_email" type="email" wire:model="siteEmail">
                                @error('siteEmail') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="default_country">{{ __('Default Country') }}</label>
                                <select class="form-control" wire:model="country">
                                    @foreach ($countries as $country)
                                    <option value="{{ $country['code'] }}">{{ $country['name'] }}</option>
                                    @endforeach
                                </select>
                                @error('default_country') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div>
                                <label for="default_currency">{{ __('Default Currency') }}</label>
                                <select class="form-control" wire:model="currency">
                                    @foreach ($currencies as $key=>$currency)
                                    <option value="{{ $currency['symbol'] }}">{{ $key }}({{ $currency['name'] }})</option>
                                    @endforeach
                                </select>
                                @error('default_currency') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <h2 class="h5 mb-4 mt-5">{{ __('Social Login') }}</h2>
                    <div class="form-check form-switch">
                        <input class="form-check-input" wire:model="facebook" type="checkbox" id="facebook">
                        <label class="form-check-label" for="facebook">{{ __('Facebook') }}</label>
                    </div>
                    @if ($facebook)
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="FACEBOOK_CLIENT_ID">{{ __('FACEBOOK_CLIENT_ID') }}</label>
                                    <input class="form-control @error('FACEBOOK_CLIENT_ID') error @enderror" id="FACEBOOK_CLIENT_ID" type="text" wire:model.lazy="FACEBOOK_CLIENT_ID">
                                    @error('FACEBOOK_CLIENT_ID') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="FACEBOOK_CLIENT_SECRET">{{ __('FACEBOOK_CLIENT_SECRET') }}</label>
                                    <input class="form-control @error('FACEBOOK_CLIENT_SECRET') error @enderror" id="FACEBOOK_CLIENT_SECRET" type="text" wire:model.lazy="FACEBOOK_CLIENT_SECRET">
                                    @error('FACEBOOK_CLIENT_SECRET') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-check form-switch">
                        <input class="form-check-input" wire:model="google" type="checkbox" id="google">
                        <label class="form-check-label" for="google">{{ __('Google') }}</label>
                    </div>
                    @if ($google)
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="GOOGLE_CLIENT_ID">{{ __('GOOGLE_CLIENT_ID') }}</label>
                                    <input class="form-control @error('GOOGLE_CLIENT_ID') error @enderror" id="GOOGLE_CLIENT_ID" type="text" wire:model.lazy="GOOGLE_CLIENT_ID">
                                    @error('GOOGLE_CLIENT_ID') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="GOOGLE_CLIENT_SECRET">{{ __('GOOGLE_CLIENT_SECRET') }}</label>
                                    <input class="form-control @error('GOOGLE_CLIENT_SECRET') error @enderror" id="GOOGLE_CLIENT_SECRET" type="text" wire:model.lazy="GOOGLE_CLIENT_SECRET">
                                    @error('GOOGLE_CLIENT_SECRET') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="form-check form-switch">
                        <input class="form-check-input" wire:model="github" type="checkbox" id="github">
                        <label class="form-check-label" for="github">{{ __('Github') }}</label>
                    </div>
                    @if ($github)
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="GITHUB_CLIENT_ID">{{ __('GITHUB_CLIENT_ID') }}</label>
                                    <input class="form-control @error('GITHUB_CLIENT_ID') error @enderror" id="GITHUB_CLIENT_ID" type="text" wire:model.lazy="GITHUB_CLIENT_ID">
                                    @error('GITHUB_CLIENT_ID') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <div>
                                    <label for="GITHUB_CLIENT_SECRET">{{ __('GITHUB_CLIENT_SECRET') }}</label>
                                    <input class="form-control @error('GITHUB_CLIENT_SECRET') error @enderror" id="GITHUB_CLIENT_SECRET" type="text" wire:model.lazy="GITHUB_CLIENT_SECRET">
                                    @error('GITHUB_CLIENT_SECRET') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                        </div>
                    @endif
                    @if (env('CUSTOM_CODE'))
                    <h2 class="h5 mb-4 mt-5">{{ __('Addvanced Settings') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="custom_css">{{ __('Custom Code In (<head />)') }}</label>
                                <textarea wire:model.lazy="custom_css" class="form-control @error('custom_css') error @enderror" id="custom_css" rows="6" wire:model.lazy="custom_css"></textarea>
                                @error('custom_css') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="custom_js">{{ __('Custom Code In (<body />)') }}</label>
                                <textarea wire:model.lazy="custom_js" class="form-control @error('custom_js') error @enderror" id="custom_js" rows="6" wire:model.lazy="custom_js"></textarea>
                                @error('custom_js') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="mt-3">
                        <div class="f-right">
                            <button type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Submit') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
