<div>
    @section('title', 'OpenAI Settings')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('OpenAi Settings') }}</li>
                </ol>
            </nav>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">

        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('OpenAi Settings') }}</h2>
            <p class="mb-0">{{ __('Here you can update OpenAi Settings.') }}</p>
        </div>
        <div class="col-lg-7">
            <div class="card card-body border-0 shadow mb-4" x-data="{ isUploading: false, progress: 0, isUploaded: false, progrs: 0 }">
                <h2 class="h5 mb-4">{{ __('OpenAi Settings') }}</h2>
                <form wire:submit.prevent="update">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="openai_api_secret">{{ __('OpenAi API Secret') }}</label>
                                <input class="form-control @error('openai_api_secret') error @enderror" id="openai_api_secret" type="text" wire:model="openai_api_secret">
                                @error('openai_api_secret') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="openai_model">{{ __('Openai Model') }}</label>
                                <select class="form-control" wire:model="openai_model">
                                    <option value="gpt-3.5-turbo">{{ __('ChatGPT') }} ({{ __('Most Expensive & Fastest & Most Capable') }})</option>
                                    <option value="gpt-3.5-turbo-16k">{{ __('ChatGPT') }} ({{ __('3.5-turbo-16k') }})</option>
                                    <option value="gpt-4">{{ __('GPT-4') }} ({{ __('Most Expensive & Fastest & Most Capable') }})</option>
                                </select>
                                @error('openai_model') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        @if ($openai_model == 'gpt-4')
                        <div class="col-lg-12">
                            <div class="alert alert-danger"><strong>{{ __('Note') }}:</strong>  {{ __('GPT-4 is not working with every api_key. You have to have an api key which can work with GPT-4.') }} </div>
                        </div>
                        @endif
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="openai_lang">{{ __('Default OpenAi Language') }}</label>
                                <select class="form-control" wire:model="lang">
                                    @foreach ($countries as $country)
                                    <option value="{{ $country['language']['code'] }}({{ $country['name'] }})">{{ $country['language']['name'] }}({{ $country['name'] }})</option>
                                    @endforeach
                                </select>
                                @error('lang') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="creativity">{{ __('Default Creativity') }}</label>
                                <select class="form-control" wire:model="creativity">
                                    <option value="0.1">Economic</option>
                                    <option value="0.5">Average</option>
                                    <option value="1.0">Good</option>
                                    <option value="1.5">Premium</option>
                                </select>
                                @error('creativity') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="maximum_input_length">{{ __('Maximum Input Length') }}</label>
                                <input class="form-control @error('maximum_input_length') error @enderror" id="maximum_input_length" type="text" wire:model.lazy="maximum_input_length">
                                @error('maximum_input_length') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="maximum_output_length">{{ __('Maximum Output Length') }}</label>
                                <input class="form-control @error('maximum_output_length') error @enderror" id="maximum_output_length" type="text" wire:model.lazy="maximum_output_length">
                                @error('maximum_output_length') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>

                    <div class="mt-3">
                        <div class="f-right">
                            <button type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Update') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
