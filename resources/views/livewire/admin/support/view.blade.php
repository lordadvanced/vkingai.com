<div>
    @section('title', 'Ticket No: '. $support->ticket_no)
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="#">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Ticket No: '. $support->ticket_no) }}</li>
                </ol>
            </nav>
            <h2 class="h4">{{ __('Ticket No: '. $support->ticket_no) }}</h2>
        </div>
    </div>
    <div class="col-12 d-flex justify-content-between flex-column flex-sm-row mt-2 mb-4">
        <a class="fw-bold text-dark hover:underline d-inline-flex align-items-center mb-2 mb-lg-0" href="{{ route('admin.support.index') }}">
            <svg class="icon icon-xs me-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M8.707 7.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l2-2a1 1 0 00-1.414-1.414L11 7.586V3a1 1 0 10-2 0v4.586l-.293-.293z"></path><path d="M3 5a2 2 0 012-2h1a1 1 0 010 2H5v7h2l1 2h4l1-2h2V5h-1a1 1 0 110-2h1a2 2 0 012 2v10a2 2 0 01-2 2H5a2 2 0 01-2-2V5z"></path></svg>
            {{ __('Back to Support Request') }}
        </a>
        </div>
    <div class="col-12">
        @foreach ($messages as $message)
            @if ($message->type == 'user')
            <div class="card border-0 shadow p-4 mb-4">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <span class="font-small">
                        <img class="avatar-sm img-fluid rounded-circle me-2" src="{{ $user['profile'] }}" alt="avatar" /> <span class="fw-bold">{{ $user['name'] }}</span> <span class="fw-normal ms-2">{{ $message->created_at->diffForHumans() }}</span>
                    </span>
                    <div class="d-none d-sm-block">
                        <svg class="icon icon-xs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                        </svg>
                    </div>
                </div>
                <p class="m-0">{{ $message->message }}</p>
            </div>
            @elseif ($message->type == 'owner')
            <div class="card bg-messages text-white border-0 shadow p-4 ms-md-5 ms-lg-6 mb-4">
                <div class="d-flex justify-content-between align-items-center mb-2">
                    <span class="font-small"><span class="fw-bold">{{ __('Your Answer') }}</span> <span class="fw-normal text-gray-300 ms-2">{{ $message->created_at->diffForHumans() }}</span></span>
                    <div class="d-none d-sm-block">
                        <svg class="icon icon-xs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 18h.01M8 21h8a2 2 0 002-2V5a2 2 0 00-2-2H8a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                        </svg>
                    </div>
                </div>
                <p class="text-gray-300 m-0">{{ $message->message }}</p>
            </div>
            @endif
        @endforeach
        <form wire:submit.prevent="store" class="mt-4 mb-5">
            <textarea wire:model="comment" class="form-control border-0 shadow mb-4" placeholder="{{ __('Your Message') }}" rows="6" maxlength="1000" required="" spellcheck="false"></textarea>
            @error('comment') <span class="error">{{ $message }}</span> @enderror
            <div class="d-flex justify-content-between align-items-center mt-3">
                <div></div>
                <div>
                    <button type="submit" class="btn btn-gray-800 d-inline-flex align-items-center text-white">
                        <svg class="icon icon-xs me-2" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path
                                fill-rule="evenodd"
                                d="M7.707 3.293a1 1 0 010 1.414L5.414 7H11a7 7 0 017 7v2a1 1 0 11-2 0v-2a5 5 0 00-5-5H5.414l2.293 2.293a1 1 0 11-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                clip-rule="evenodd"
                            ></path>
                        </svg>
                        {{ __('Reply') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

