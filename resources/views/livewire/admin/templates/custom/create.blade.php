<div>
    @section('title', 'Create Custom Template')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Create Custom Templates') }}</li>
                </ol>
            </nav>
        </div>
        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{ route('admin.custom.templates') }}" class="btn btn-sm btn-gray-800 d-inline-flex align-items-center p-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-list-check" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
                  </svg>
                  <span class="ml-2">{{ __('Custom Templates') }}</span>

            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('Create Custom Templates') }}</h2>
            <p class="mb-0">{{ __('Here you can create custom templates for your website.') }}</p>
        </div>
        <div class="col-lg-7" x-data="{ isUploading: false, progress: 0 }"
        x-on:livewire-upload-start="isUploading = true"
        x-on:livewire-upload-finish="isUploading = false"
        x-on:livewire-upload-error="isUploading = false"
        x-on:livewire-upload-progress="progress = $event.detail.progress">
            <form wire:submit.prevent="store">
                <div class="card card-body border-0 shadow mb-4">
                    <h2 class="h5 mb-4">{{ __('Create Custom Template') }}</h2>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="name">{{ __('Template Title') }}</label>
                                <input class="form-control @error('name') error @enderror" id="name" type="text"
                                wire:model.lazy="name">
                                @error('name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="description">{{ __('Template Description') }}</label>
                                <textarea wire:model.lazy="description" id="description" cols="30" rows="5" class="form-control @error('description') error @enderror"></textarea>
                                @error('description') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div wire:ignore>
                                <label>{{ __('Template Category') }}</label>
                                <select class="form-control js-multiple" multiple wire:model="category">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('category') <span class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <label>{{ __('Template Icon') }} <span class="custom-percentage" x-show="isUploading" x-text="'(' +progress + '%)'"></span></label>
                            <input type="file" class="form-control @error('templateIcon') error @enderror" wire:model="templateIcon">
                            @error('templateIcon') <span class="error">{{ $message }}</span> @enderror
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="status">{{ __('Select Status') }}</label>
                                <select wire:model="status" id="status" class="form-control">
                                    <option value="approved">{{ __('Approved') }}</option>
                                    <option value="pending">{{ __('Pending') }}</option>
                                </select>
                                @error('status') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-body border-0 shadow mb-4">
                    <div class=" d-flex justify-content-between align-items-center">
                        <h2 class="h5">{{ __('Input Groups') }}</h2>
                        <button wire:click="addGroup" type="button" class=" btn"><svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-plus-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                            <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                          </svg></button>
                    </div>
                </div>
                @foreach ($input_groups as $key=> $item)
                <div class="card card-body border-0 shadow mb-4">
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <div class=" d-flex justify-content-between align-items-center">
                                    <label for="input_type mb-0">{{ __('Select Input Type') }}</label>
                                    <button type="button" wire:click="removeGroup({{ $key }})" class=" btn text-danger"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="bi bi-dash-circle" viewBox="0 0 16 16">
                                        <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                                        <path d="M4 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 4 8z"/>
                                      </svg></button>
                                </div>
                                <select class="form-control" wire:model="input_groups.{{ $key }}.input_type">
                                    <option value="input">{{ __('Input Field') }}</option>
                                    <option value="textarea">{{ __('TextArea Field') }}</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label>{{ __('Input Name') }}</label>
                                <input class="form-control" wire:model="input_groups.{{ $key }}.input_name"  wire:change="inputNameUpdated('{{ $key }}')" type="text" />
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label>{{ __('Input Placeholder') }}</label>
                                <input class="form-control" wire:model="input_groups.{{ $key }}.input_description"></input>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="card card-body border-0 shadow mb-4">
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <label>{{ __('Created Inputs') }}</label>
                            <div class=" form-control">
                                @foreach ($created_inputs as $item)
                                <button type="button" wire:click="addInPrompt('{{ $item }}')" class=" btn btn-gray-200">{{ $item }}</button>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-md-12 mb-4">
                            <label>{{ __('Custom Prompt') }}</label>
                            <textarea wire:model="custom_prompt" rows="8" class=" form-control @error('custom_prompt') error @enderror"></textarea>
                            @error('custom_prompt') <span class="error">{{ $message }}</span> @enderror
                        </div>
                    </div>
                    <div>
                        <div class="f-right">
                            <button x-show="isUploading" disabled type="submit" class="btn btn-gray-300 mt-2 animate-up-2">{{ __('Submit') }}</button>
                            <button x-show="!isUploading" type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Submit') }}</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $(document).ready(function() {
        "use strict";

        $('.js-multiple').on('change', function() {
            Livewire.emit('categoryUpdate', $(this).val())
        });


        $('.js-multiple').select2();
    });
</script>
