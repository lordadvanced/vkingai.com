<div>
    @section('title', 'Add New Plan')
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div class="d-block mb-4 mb-md-0">
            <nav aria-label="breadcrumb" class="d-none d-md-inline-block">
                <ol class="breadcrumb breadcrumb-dark breadcrumb-transparent">
                    <li class="breadcrumb-item">
                        <a href="{{ route('admin.dashboard') }}">
                            <svg class="icon icon-xxs" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"
                                ></path>
                            </svg>
                        </a>
                    </li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ env('APP_NAME') }}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{ __('Create Plan') }}</li>
                </ol>
            </nav>

        </div>
        <div class="btn-toolbar mb-2 mb-md-0">
            <a href="{{ route('admin.plan.index') }}" class="btn btn-sm btn-gray-800 d-inline-flex align-items-center p-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" fill="currentColor" class="bi bi-list-check" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3.854 2.146a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 3.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708L2 7.293l1.146-1.147a.5.5 0 0 1 .708 0zm0 4a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>
                  </svg>
                  <span class="ml-2">{{ __('Manage Plan') }}</span>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5">
            <h2 class="h4">{{ __('Create Plan') }}</h2>
            <p class="mb-0">{{ __('Here you can create a new Plan for your website.') }}</p>
        </div>
        <div class="col-lg-7">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4">{{ __('Create New Plan') }}</h2>
                <form wire:submit.prevent="store">
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="name">{{ __('Name') }}</label>
                                <input class="form-control @error('name') error @enderror" id="name" type="text"
                                    placeholder="{{ __('Enter Name') }}" wire:model.lazy="name">
                                @error('name') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="durationType">{{ __('Duration Type') }}</label>
                                <select wire:model="durationType" id="durationType" class="form-control">
                                    <option value="monthly">{{ __('Monthly') }}</option>
                                    <option value="yearly">{{ __('Yearly') }}</option>
                                </select>
                                @error('durationType') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="short_content">{{ __('Short Description') }}</label>
                                <textarea wire:model.lazy="short_content" id="short_content" cols="30" rows="5" class="form-control " spellcheck="false"></textarea>
                                @error('short_content') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div>
                                <label for="price">{{ __('Price (0 = Free)') }}</label>
                                <input class="form-control @error('price') error @enderror" id="price" type="text"
                                    placeholder="{{ __('Enter Price') }}" wire:model="price">
                                @error('price') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="col-md-6 mb-4">
                            <div>
                                <label for="word_limit">{{ __('Total Word Limit (-1 = unlimited)') }}</label>
                                <input class="form-control @error('word_limit') error @enderror" id="word_limit" type="text"
                                     wire:model="word_limit">
                                @error('word_limit') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                        <div class="mb-4">
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <input checked class="form-check-input" checked wire:model="ai_templates" type="checkbox" id="ai_templates">
                                    <label class="form-check-label" for="ai_templates">{{ __('Ai Templates Features') }}</label>
                                </div>
                            </div>
                            @if ($ai_templates)
                            <div class="col-md-12">
                                <div>
                                    <label for="templates_limit">{{ __('Total Templates Limit (-1 = All)') }}</label>
                                    <input class="form-control @error('templates_limit') error @enderror" id="templates_limit" type="text"
                                         wire:model="templates_limit">
                                    @error('templates_limit') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="mb-4">
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <input checked class="form-check-input" checked wire:model="ai_chatbot" type="checkbox" id="ai_chatbot">
                                    <label class="form-check-label" for="ai_chatbot">{{ __('Ai Chatbot Features') }}</label>
                                </div>
                            </div>
                            @if ($ai_chatbot)
                            <div class="col-md-12">
                                <div>
                                    <label for="chatbot_limit">{{ __('Total Chatbot Limit (-1 = unlimited)') }}</label>
                                    <input class="form-control @error('chatbot_limit') error @enderror" id="chatbot_limit" type="text"
                                         wire:model="chatbot_limit">
                                    @error('chatbot_limit') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="mb-4">
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <input checked class="form-check-input" checked wire:model="ai_images" type="checkbox" id="ai_images">
                                    <label class="form-check-label" for="ai_images">{{ __('Ai Images Features') }}</label>
                                </div>
                            </div>
                            @if ($ai_images)
                            <div class="col-md-12">
                                <div>
                                    <label for="image_limit">{{ __('Total Images Limit (-1 = unlimited)') }}</label>
                                    <input class="form-control @error('image_limit') error @enderror" id="image_limit" type="text"
                                         wire:model="image_limit">
                                    @error('image_limit') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class=" mb-4">
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <input checked class="form-check-input" checked wire:model="ai_speech_to_text" type="checkbox" id="ai_speech_to_text">
                                    <label class="form-check-label" for="ai_speech_to_text">{{ __('Ai Speech To Text Features') }}</label>
                                </div>
                            </div>
                            @if ($ai_speech_to_text)
                            <div class="col-md-12 mb-2">
                                <div>
                                    <label for="speech_to_text_limit">{{ __('Speech to Text Limit (-1 = unlimited)') }}</label>
                                    <input class="form-control @error('speech_to_text_limit') error @enderror" id="speech_to_text_limit" type="text"
                                         wire:model="speech_to_text_limit">
                                    @error('speech_to_text_limit') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div>
                                    <label for="audio_file_size">{{ __('Audio File Size Limit (MB)') }}</label>
                                    <input class="form-control @error('audio_file_size') error @enderror" id="audio_file_size" type="text"
                                         wire:model="audio_file_size">
                                    @error('audio_file_size') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input checked class="form-check-input" checked wire:model="ai_code" type="checkbox" id="ai_code">
                                <label class="form-check-label" for="ai_code">{{ __('Ai Code Features') }}</label>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input checked class="form-check-input" checked wire:model="access_to_gpt3" type="checkbox" id="access_to_gpt3">
                                <label class="form-check-label" for="access_to_gpt3">{{ __('Access To GPT-3.5') }}</label>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" wire:model="accept_to_gpt4" type="checkbox" id="accept_to_gpt4">
                                <label class="form-check-label" for="accept_to_gpt4">{{ __('Access To GPT-4') }}</label>
                            </div>
                        </div>
                        <div class="mb-4">
                            <div class="col-md-12">
                                <div class="form-check form-switch">
                                    <input class="form-check-input" wire:model="accept_trail" type="checkbox" id="accept_trail">
                                    <label class="form-check-label" for="accept_trail">{{ __('Accept Trail?') }}</label>
                                </div>
                            </div>
                            @if ($accept_trail)
                            <div class="col-md-12">
                                <div>
                                    <label for="trail_days">{{ __('Trail Days') }}</label>
                                    <input class="form-control @error('trail_days') error @enderror" id="trail_days" type="number"
                                         wire:model="trail_days">
                                    @error('trail_days') <span class="error">{{ $message }}</span> @enderror
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" wire:model="is_recommended" type="checkbox" id="is_recommended">
                                <label class="form-check-label" for="is_recommended">{{ __('Is Recommended?') }}</label>
                            </div>
                        </div>
                        <div class="col-md-12 mb-3">
                            <div class="form-check form-switch">
                                <input class="form-check-input" wire:model="status" type="checkbox" id="status">
                                <label class="form-check-label" for="status">{{ __('Active This Plan?') }}</label>
                                @error('status') <span class="error">{{ $message }}</span> @enderror
                            </div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="f-right">
                            <button type="submit" class="btn btn-gray-800 mt-2 animate-up-2">{{ __('Submit') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
