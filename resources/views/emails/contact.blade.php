<x-mail::message>
# {{ __('You got a message from') }} {{ $data['name'] }}

## {{ __('Email') }}: {{ $data['email'] }}

## {{ __('Message') }}: <br>
{{ $data['message'] }}

{{ __('Thanks') }},<br>
{{ config('app.name') }}
</x-mail::message>
