@extends('layouts.mail')

@section('subject', Lang::get('Verify Email Address'))

@section('content')
    <p>
        {{ __('Please click the button below to verify your email address.') }}
    </p>

    <a class="action button button-primary text-center" href="{{ $url }}">{{ __('Verify Email Address') }}</a>

    <p>
        {{ __('If you did not create an account, no further action is required.') }}
    </p>
    <p>
        {{ __('Regards') }}, <br>
        {{ config('app.name') }}
    </p>
    <hr>
    <p>
        {{ __("If you're having trouble clicking the") }} "{{ __('Verify Email Address') }}" {{ __('button, copy and paste the URL below into your web browser') }}: <a href="{{ $url }}">{{ $url }}</a>
    </p>
@endsection
