@extends('layouts.mail')

@section('subject', Lang::get('Reset Password Notification'))

@section('content')
    <p>
        {{ __('You are receiving this email because we received a password reset request for your account.') }}
    </p>

    <a class="action button button-primary text-center" href="{{ $url }}">{{ __('Reset Password') }}</a>

    <p>
        {{ __('This password reset link will expire in') }} {{ config('auth.passwords.'.config('auth.defaults.passwords').'.expire') }} {{ __('minutes') }}.
    </p>

    <p>
        {{ __('If you did not request a password reset, no further action is required.') }}
    </p>
    <p>
        {{ __('Regards') }}, <br>
        {{ config('app.name') }}
    </p>
    <hr>
    <p>
        {{ __("If you're having trouble clicking the") }} "{{ __("Reset Password") }}" {{ __('button, copy and paste the URL below into your web browser') }}: <a href="{{ $url }}">{{ $url }}</a>
    </p>
@endsection
