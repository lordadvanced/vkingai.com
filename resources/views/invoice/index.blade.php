<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>{{ __('Invoice') }}</title>
  <style>
    body {
      font-family: Arial, sans-serif;
      line-height: 1.6;
      background-color: #f5f5f5;
    }
    .invoice {
      max-width: 800px;
      margin: 0 auto;
      padding: 50px;
      background-color: #fff;
      border-radius: 10px;
    }
    .invoice-header {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 5px;
      border-bottom: 1px solid #eeeeee;
    }
    .invoice-second-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #eeeeee;
    }
    .invoice-third-header {
        display: flex;
        justify-content: space-between;
        align-items: center;
        border-bottom: 1px solid #eeeeee;
    }
    .invoice-logo {
      max-height: 60px;
    }
    .invoice-title {
      font-size: 24px;
      font-weight: bold;
    }
    .invoice-info {
      display: flex;
      flex-direction: column;
      text-align: right;
    }
    .invoice-info span {
      display: block;
    }
    .invoice-table {
      width: 100%;
      border-collapse: collapse;
      margin-bottom: 20px;
      margin-top: 20px;
    }
    .invoice-table th, .invoice-table td {
      padding: 12px;
      text-align: left;
      border-bottom: 1px solid #ddd;
    }
    .invoice-table th {
      background-color: #f2f2f2;
    }
    .invoice-total {
      display: flex;
      justify-content: flex-end;
      font-weight: bold;
      font-size: 18px;
    }
    .invoice-footer {
      text-align: center;
      margin-top: 50px;
      color: #777;
    }
    .button {
      display: inline-block;
      padding: 10px 20px;
      background-color: #ff733b;
      color: #fff;
      text-decoration: none;
      border-radius: 4px;
      transition: background-color 0.3s;
    }
    .button:hover {
      background-color: #ff733b;
    }
  </style>
</head>
<body>
  <div class="invoice">
    <div class="invoice-header">
      <img src="{{ asset('/') . $logo }}" alt="Company Logo" class="invoice-logo">
      <h1 class="invoice-title">{{ __('Invoice') }}</h1>
    </div>
    <div class="invoice-second-header">
        <p><strong>{{ __('Date:') }} </strong>{{ $created_at }}</p>
        <p><strong>{{ __('Invoice No:') }} </strong>{{ $invoiceId }}</p>
    </div>
    <div style="padding: 15px 0; border-bottom: none;" class="invoice-third-header">
        <div>
            <strong>{{ __('Invoiced From') }}:</strong>
            <div style="margin-bottom: 10px;">
                {{ env('APP_NAME') }}
            </div>
            <strong>{{ __('Payment Method') }}:</strong>
            <div>{{ $payment_method }}</div>
        </div>
    </div>
    <table class="invoice-table">
      <thead style="border-top: 1px solid #ddd;">
        <tr>
          <th>{{ __('Subscription Name') }}</th>
          <th>{{ __('Duration') }}</th>
          <th>{{ __('Price') }}</th>
          <th>{{ __('Total') }}</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{{ $planName }}</td>
          <td>{{ $duration }}</td>
          <td>{{ $currency }}{{ round($info->price, 2) }}</td>
          <td>{{ $currency }}{{ round($info->price, 2) }}</td>
        </tr>
        <tr style="background-color: #f6f6f6;">
            <td colspan="3" style="text-align: right;"><strong>{{ __('Sub Total') }}:</strong></td>
            <td>{{ $currency }}{{ round($info->price, 2) }}</td>
          </tr>
          <tr style="background-color: #f6f6f6;">
            <td colspan="3" style="text-align: right;"><strong>{{ __('Charge') }}:</strong></td>
            <td>{{ $currency }}{{ $info->charge }}</td>
          </tr>
          <tr style="background-color: #f6f6f6;">
            <td colspan="3" style="text-align: right;"><strong>{{ __('Tax') }}:</strong></td>
            <td>{{ $currency }}{{ $info->tax }}</td>
          </tr>
          <tr style="background-color: #f6f6f6;">
            <td colspan="3" style="text-align: right;"><strong>{{ __('Total') }}:</strong></td>
            <td>{{ $currency }}{{ round($amount, 2) }}</td>
          </tr>
        <!-- Add more rows for additional products -->
      </tbody>
    </table>
    <div class="invoice-footer">
      <p><strong>{{ __('NOTE') }}:</strong> {{ __('This is computer generated invoice and does not require physical signature.') }}</p>
    </div>
  </div>
</body>
</html>
