import './bootstrap';
import '../css/app.css';
import 'remixicon/fonts/remixicon.css'
import { createRoot } from 'react-dom/client';
import { createInertiaApp } from '@inertiajs/inertia-react';
import { InertiaProgress } from '@inertiajs/progress';
import { resolvePageComponent } from 'laravel-vite-plugin/inertia-helpers';
import i18next from 'i18next';
import { I18nextProvider } from 'react-i18next';

const resources = {};

async function loadLanguageAsync(lang) {
    // Assuming your translation files are in the './translations' directory
    const timestamp = Date.now(); // Add a unique timestamp
    const url = `/translations/${lang}/global.json?t=${timestamp}`;

    try {
        const response = await fetch(url);
        const translationData = await response.json();
        resources[lang] = { global: translationData };
    } catch (error) {
        console.error(`Failed to fetch translation module for language ${lang}:`, error);
    }
}

// Fetch the list of available languages dynamically
async function fetchAvailableLanguages() {
    const response = await fetch('/translations/languages.json');
    const availableLanguages = await response.json();
    return availableLanguages;
}

fetchAvailableLanguages().then(async (availableLanguages) => {
    const promises = availableLanguages.map(loadLanguageAsync);

    Promise.all(promises).then(() => {
        const default_lang = localStorage.getItem('lang') || 'en';
        i18next.init({
            interpolation: { escapeValue: false },
            lng: default_lang,
            resources,
        });
    });
});

if (import.meta.hot) {
    import.meta.hot.accept();
}

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel';

createInertiaApp({
    title: (title) => `${title} - ${appName}`,
    resolve: (name) => resolvePageComponent(`./Pages/${name}.jsx`, import.meta.glob('./Pages/**/*.jsx')),
    setup({ el, App, props }) {
        const root = createRoot(el);

        root.render(
            <I18nextProvider i18n={i18next}>
                <App {...props} />
            </I18nextProvider>
        );
    },
});

InertiaProgress.init({ color: 'linear-gradient(90deg, #885CF6 1.73%, rgba(126, 243, 229, 0.97) 101.49%)' });
