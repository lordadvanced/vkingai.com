import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import languages from "../lang/languages.json";

const resources = {};

const loadLanguageFiles = async () => {
  const defaultLanguage = window.defaultLanguage || "en"; // Replace "en" with your fallback default language
  const promises = languages.map(async (langCode) => {
    try {
      const translation = await import(`../lang/${langCode}.json`);
      return { langCode, translation: translation.default };
    } catch (error) {
      return null;
    }
  });

  const loadedTranslations = await Promise.all(promises);
  loadedTranslations.forEach((item) => {
    if (item) {
      resources[item.langCode] = { translation: item.translation };
    }
  });

  // Set the default language for i18next
  i18n.changeLanguage(defaultLanguage);
};

loadLanguageFiles();

i18n
  .use(initReactI18next)
  .init({
    resources,
    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
