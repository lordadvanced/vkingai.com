<?php

use App\Http\Controllers\Admin\InvoiceController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\User\AccountController;
use App\Http\Controllers\User\DashboardController;
use App\Http\Controllers\WelcomeController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Livewire\Admin\Seo\Edit as SeoEdit;
use App\Http\Livewire\Admin\Blog\Edit as BlogEdit;
use App\Http\Livewire\Admin\Page\Edit as EditPage;
use App\Http\Livewire\Admin\Seo\Index as SeoIndex;
use App\Http\Livewire\Admin\User\Edit as EditUser;
use App\Http\Livewire\Admin\Blog\Index as BlogIndex;
use App\Http\Livewire\Admin\Page\Index as PageIndex;
use App\Http\Livewire\Admin\Seo\Create as SeoCreate;
use App\Http\Livewire\Admin\User\Index as UserIndex;
use App\Http\Livewire\Admin\Blog\Create as BlogCreate;
use App\Http\Livewire\Admin\Category\Index as CategoryIndex;
use App\Http\Livewire\Admin\Category\Create as CategoryCreate;
use App\Http\Livewire\Admin\Category\Edit as CategoryEdit;
use App\Http\Livewire\Admin\Page\Create as PageCreate;
use App\Http\Livewire\Admin\User\Create as UserCreate;
use App\Http\Livewire\Admin\Getway\Edit as GatewayEdit;
use App\Http\Livewire\Admin\Getway\Index as GatewayIndex;
use App\Http\Livewire\Admin\Settings\Hero as HeroSettings;
use App\Http\Livewire\Admin\Getway\Create as GatewayCreate;
use App\Http\Livewire\Admin\Settings\Index as SettingsIndex;
use App\Http\Livewire\Admin\Lang\Create as LangCreate;
use App\Http\Livewire\Admin\Lang\Customize as LangCustomize;
use App\Http\Livewire\Admin\Lang\Edit as LangEdit;
use App\Http\Livewire\Admin\Lang\Index as LangIndex;
use App\Http\Livewire\Admin\Menu\Builder as MenuBuilder;
use App\Http\Livewire\Admin\Menu\Create as MenuCreate;
use App\Http\Livewire\Admin\Menu\Edit as MenuEdit;
use App\Http\Livewire\Admin\Menu\Index as MenuIndex;
use App\Http\Livewire\Admin\Settings\Footer as FooterSettings;
use App\Http\Livewire\Admin\Transcation\Index as TranscationIndex;
use App\Http\Livewire\Admin\Settings\Profile\Index as SettingsProfileIndex;
use App\Http\Controllers\Admin\MenuOrderController;
use App\Http\Controllers\Auth\SocialiteController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\ChatbotShowController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PricingController;
use App\Http\Controllers\PrivacyController;
use App\Http\Controllers\User\AiChatsController;
use App\Http\Controllers\User\AiCodeController;
use App\Http\Controllers\User\AiImageController;
use App\Http\Controllers\User\AiSpeechToTextController;
use App\Http\Controllers\User\BillingController;
use App\Http\Controllers\User\DocumentController;
use App\Http\Controllers\User\PlanController;
use App\Http\Controllers\User\SettingsController;
use App\Http\Controllers\User\StripeController;
use App\Http\Controllers\User\SupportController;
use App\Http\Controllers\User\TemplatesController;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Plan\Create as PlanCreate;
use App\Http\Livewire\Admin\Plan\Edit as PlanEdit;
use App\Http\Livewire\Admin\Plan\Index as PlanIndex;
use App\Http\Livewire\Admin\Settings\Affilate;
use App\Http\Livewire\Admin\Settings\Testimonial\Create as testimonialCreate;
use App\Http\Livewire\Admin\Settings\Testimonial\Edit as testimonialEdit;
use App\Http\Livewire\Admin\Settings\Testimonial\Index as testimonialIndex;
use App\Http\Livewire\Admin\Settings\Features\Create as FeaturesCreate;
use App\Http\Livewire\Admin\Settings\Features\Edit as FeaturesEdit;
use App\Http\Livewire\Admin\Settings\Features\Index as FeaturesIndex;
use App\Http\Livewire\Admin\Settings\Faq\Index as FAQIndex;
use App\Http\Livewire\Admin\Settings\Faq\Create as FAQCreate;
use App\Http\Livewire\Admin\Settings\Faq\Edit as FAQEdit;
use App\Http\Livewire\Admin\Settings\General;
use App\Http\Livewire\Admin\Settings\Invoice;
use App\Http\Livewire\Admin\Settings\OpenAi;
use App\Http\Livewire\Admin\Settings\PrivacyPolicy;
use App\Http\Livewire\Admin\Settings\Smtp;
use App\Http\Livewire\Admin\Settings\System\Index as SystemIndex;
use App\Http\Livewire\Admin\Support\Index as SupportIndex;
use App\Http\Livewire\Admin\Support\View as SupportView;
use App\Http\Livewire\Auth\Login;
use App\Http\Livewire\Admin\Emailtemplate\Edit as EmailTemplateEdit;
use App\Http\Livewire\Admin\Emailtemplate\Index as EmailTemplateIndex;
use App\Http\Livewire\Admin\Templates\BuildIn\Index as BuildInTemplatesIndex;
use App\Http\Livewire\Admin\Templates\BuildIn\Edit as BuildInTemplatesEdit;
use App\Http\Livewire\Admin\Templates\Category\Index as TemplatesCategoryIndex;
use App\Http\Livewire\Admin\Templates\Category\Create as TemplatesCategoryCreate;
use App\Http\Livewire\Admin\Templates\Category\Edit as TemplatesCategoryEdit;
use App\Http\Livewire\Admin\Templates\Chat\Create as ChatTemplatesCreate;
use App\Http\Livewire\Admin\Templates\Chat\Edit as ChatTemplatesEdit;
use App\Http\Livewire\Admin\Templates\Chat\Index as ChatTemplatesIndex;
use App\Http\Livewire\Admin\Templates\Custom\Index as CustomTemplatesIndex;
use App\Http\Livewire\Admin\Templates\Custom\Create as CustomTemplatesCreate;
use App\Http\Livewire\Admin\Templates\Custom\Edit as CustomTemplatesEdit;
use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('admin/menu/order', [MenuOrderController::class, 'order'])->name('admin.menu.order');
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(['as' => 'admin.', 'prefix' => 'admin', 'middleware' => ['auth', 'admin']], function () {
    Route::get('dashboard', Dashboard::class)->name('dashboard');

    // Built-in Templates
    Route::get('templates/all', BuildInTemplatesIndex::class)->name('buildin.templates');
    Route::get('templates/build-in/{id}/edit', BuildInTemplatesEdit::class)->name('buildin.templates.edit');

    // Custom Templates
    Route::get('templates/custom', CustomTemplatesIndex::class)->name('custom.templates');
    Route::get('templates/custom/create', CustomTemplatesCreate::class)->name('custom.templates.create');
    Route::get('templates/custom/{id}/edit', CustomTemplatesEdit::class)->name('custom.templates.edit');

    // Templates Category
    Route::get('templates/category', TemplatesCategoryIndex::class)->name('templates.category');
    Route::get('templates/category/create', TemplatesCategoryCreate::class)->name('templates.category.create');
    Route::get('templates/category/{id}/edit', TemplatesCategoryEdit::class)->name('templates.category.edit');

    // Chat Templates
    Route::get('templates/chat', ChatTemplatesIndex::class)->name('chat.templates.index');
    Route::get('templates/chat/create', ChatTemplatesCreate::class)->name('chat.template.create');
    Route::get('templates/chat/{id}/edit', ChatTemplatesEdit::class)->name('chat.templates.edit');

    Route::get('transaction', TranscationIndex::class)->name('transaction.index');
    Route::get('user', UserIndex::class)->name('user.index');
    Route::get('profile/settings', SettingsProfileIndex::class)->name('profile.settings');
    Route::get('settings', SettingsIndex::class)->name('settings.index');
    Route::get('settings/hero', HeroSettings::class)->name('settings.hero');
    Route::get('settings/footer', FooterSettings::class)->name('settings.footer');
    Route::get('seo', SeoIndex::class)->name('seo.index');
    Route::get('page', PageIndex::class)->name('page.index');
    Route::get('page/create', PageCreate::class)->name('page.create');
    Route::get('page/edit/{id}', EditPage::class)->name('page.edit');
    Route::get('blog', BlogIndex::class)->name('blog.index');
    Route::get('blog/edit/{id}', BlogEdit::class)->name('blog.edit');
    Route::get('user/create', UserCreate::class)->name('user.create');
    Route::get('user/edit/{id}', EditUser::class)->name('user.edit');
    Route::get('blog/create', BlogCreate::class)->name('blog.create');
    Route::get('category', CategoryIndex::class)->name('category.index');
    Route::get('category/create', CategoryCreate::class)->name('category.create');
    Route::get('category/edit{id}', CategoryEdit::class)->name('category.edit');


    // Menu Settings
    Route::get('menu', MenuIndex::class)->name('menu.index');
    Route::get('menu/create', MenuCreate::class)->name('menu.create');
    Route::get('menu/edit/{id}', MenuEdit::class)->name('menu.edit');
    Route::get('menu/builder/{id}', MenuBuilder::class)->name('menu.builder');

    // getway Routes
    Route::get('gateway', GatewayIndex::class)->name('gateway.index');
    Route::get('gateway/create', GatewayCreate::class)->name('gateway.create');
    Route::get('gateway/edit/{id}', GatewayEdit::class)->name('gateway.edit');


    // Seo Routes
    Route::get('seo', SeoIndex::class)->name('seo.index');
    Route::get('seo/create', SeoCreate::class)->name('seo.create');
    Route::get('seo/edit/{id}', SeoEdit::class)->name('seo.edit');

    // Languages Routes
    Route::get('lang', LangIndex::class)->name('lang.index');
    Route::get('lang/create', LangCreate::class)->name('lang.create');
    Route::get('lang/{id}/edit', LangEdit::class)->name('lang.edit');
    Route::get('lang/cutomize/{id}', LangCustomize::class)->name('lang.customize');

    // Plan Routes
    Route::get('plan/index', PlanIndex::class)->name('plan.index');
    Route::get('plan/create', PlanCreate::class)->name('plan.create');
    Route::get('plan/edit/{id}', PlanEdit::class)->name('plan.edit');

    // testimonial Routes
    Route::get('settings/testimonial', testimonialIndex::class)->name('testimonial.index');
    Route::get('settings/testimonial/create', testimonialCreate::class)->name('testimonial.create');
    Route::get('settings/testimonial/{id}/edit', testimonialEdit::class)->name('testimonial.edit');

    Route::get('/download-invoice/{id}', [InvoiceController::class, 'downloadInvoice'])->name('download.invoice')->withoutMiddleware('admin');


    // features Routes
    Route::get('settings/features', FeaturesIndex::class)->name('features.index');
    Route::get('settings/features/create', FeaturesCreate::class)->name('features.create');
    Route::get('settings/features/{id}/edit', FeaturesEdit::class)->name('features.edit');

    // HowItWorks Routes
    Route::get('settings/faq', FAQIndex::class)->name('faq.index');
    Route::get('settings/faq/create', FAQCreate::class)->name('faq.create');
    Route::get('settings/faq/{id}/edit', FAQEdit::class)->name('faq.edit');

    // Support Request Routes
    Route::get('support', SupportIndex::class)->name('support.index');
    Route::get('support/view/{id}', SupportView::class)->name('support.view');

    // Settings Routes
    Route::get('setting/general', General::class)->name('setting.general');
    Route::get('setting/affiliate', Affilate::class)->name('setting.affiliate');
    Route::get('setting/invoice', Invoice::class)->name('setting.invoice');
    Route::get('setting/openai', OpenAi::class)->name('setting.openai');
    Route::get('setting/smtp', Smtp::class)->name('setting.smtp');
    Route::get('setting/privacy/policy', PrivacyPolicy::class)->name('setting.privacy.policy');
    Route::get('setting/email/templates', EmailTemplateIndex::class)->name('email.template.index');
    Route::get('setting/email/template/{id}', EmailTemplateEdit::class)->name('email.template.edit');

    // System Settings Route
    Route::get('system/settings', SystemIndex::class)->name('system.index');
});


Route::get('chats/{id}', [ChatbotShowController::class, 'index']);
Route::post('chat/reply', [ChatbotShowController::class, 'reply'])->withoutMiddleware([VerifyCsrfToken::class]);
Route::post('messages/delete', [ChatbotShowController::class, 'delete'])->withoutMiddleware([VerifyCsrfToken::class]);
Route::post('message/favorite', [ChatbotShowController::class, 'favorite'])->withoutMiddleware([VerifyCsrfToken::class]);
Route::get('demo/chatbot', [ChatbotShowController::class, 'demoBot']);
Route::get('/', [WelcomeController::class, 'index'])->name('welcome');
Route::get('/page/{slug}', [WelcomeController::class, 'page']);
Route::get('/blogs', [BlogController::class, 'index']);
Route::get('/blog/{slug}', [BlogController::class, 'show']);
Route::get('/about', [WelcomeController::class, 'index']);
Route::get('/pricing', [PricingController::class, 'index']);
Route::get('templates', [WelcomeController::class, 'templates']);
Route::get('contact', [ContactController::class, 'index']);
Route::post('contact', [ContactController::class, 'store']);
Route::get('admin/login', Login::class)->name('admin.login');
Route::get('/auth/login/{type}', [SocialiteController::class, 'auth']);
Route::get('/auth/callback/{type}', [SocialiteController::class, 'callback']);
Route::get('lang/lists', [WelcomeController::class, 'langLists']);
Route::get('privacy/policy', [PrivacyController::class, 'privacyPolicy']);
Route::get('terms/condition', [PrivacyController::class, 'termsCondition']);
Route::get('logout', function () {
    Auth::logout();
    Session::forget('currentBot');
    return redirect()->back();
});
Route::get('lang/{code}', function ($code) {
    \Session::put('locale', $code);
    return response()->json('success');
});
// Auth::routes(['verify' => true]);

Route::get('/tokens', function () {
    $prompt = "Sure! Here's a simple code in JavaScript that prints \"Hello World\" to the console:\\n\\n```javascript\\nconsole.log(\"Hello World\");\\n```\\n\\nTo run this, you can create a new JavaScript file with a .js extension (e.g., script.js) and open it in a browser or use a JavaScript runtime environment, like Node.js, to execute the code.";
    $token_array = gpt_encode($prompt);
    return count($token_array);
});

// User Panel Routes
Route::group(['as' => 'user.', 'prefix' => 'user', 'middleware' => ['auth', 'verifyEmail', 'planExists']], function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    // Documents Routes
    Route::get('documents', [DocumentController::class, 'index'])->name('document.index');
    Route::post('document/delete', [DocumentController::class, 'delete']);
    Route::post('document/bookmark', [DocumentController::class, 'bookmark']);
    Route::get('document/{slug}', [DocumentController::class, 'show']);

    // Templates Routes
    Route::get('templates', [TemplatesController::class, 'index'])->name('templates');
    Route::get('templates/{slug}', [TemplatesController::class, 'show'])->name('templates.show');
    Route::post('template/favorite', [TemplatesController::class, 'favorite'])->name('template.favorite');
    Route::get('templates/data/all', [TemplatesController::class, 'templatesData']);

    // AI Generate
    Route::get('ai/generate', [TemplatesController::class, 'aiGenerate']);
    Route::post('/ai/document/store', [TemplatesController::class, 'storeDocument']);

    // Documents Route
    Route::post('document/rename', [DocumentController::class, 'rename']);
    Route::post('document/update', [DocumentController::class, 'update']);

    // AI Images
    Route::get('ai/images', [AiImageController::class, 'index'])->name('ai.images');
    Route::post('ai/image/generate', [AiImageController::class, 'aiImageGenerate']);
    Route::post('image/delete', [AiImageController::class, 'delete']);

    // AI Chats
    Route::get('ai/chats', [AiChatsController::class, 'index'])->name('ai.chats');
    Route::get('ai/chat/{slug}', [AiChatsController::class, 'show'])->name('ai.chat.show');
    Route::post('ai/chat/messages/get', [AiChatsController::class, 'messageGets']);
    Route::post('new/conversation', [AiChatsController::class, 'newConversation']);
    Route::post('message/send', [AiChatsController::class, 'sendMessage']);
    Route::post('conversation/rename', [AiChatsController::class, 'conversationRename']);
    Route::post('conversation/delete', [AiChatsController::class, 'conversationDelete']);
    Route::get('ai/chats/generate', [AiChatsController::class, 'aiGenerate'])->name('ai.chat.generate');

    // AI Code
    Route::get('ai/code', [AiCodeController::class, 'index'])->name('ai.code');
    Route::get('ai/code/generate', [AiCodeController::class, 'generate']);

    // Speech To Text
    Route::get('ai/speech/text', [AiSpeechToTextController::class, 'index'])->name('ai.speech.text');
    Route::post('ai/audio/generate', [AiSpeechToTextController::class, 'generate']);

    // User Settings
    Route::get('settings', [SettingsController::class, 'index'])->name('settings');

    // User Settings Information Update
    Route::get('settings/information', [SettingsController::class, 'information'])->name('settings.information');

    // Support
    Route::get('support', [SupportController::class, 'index'])->name('support');
    Route::post('support/store', [SupportController::class, 'store'])->name('support.store');
    Route::get('support/view/{ticketNo}', [SupportController::class, 'view'])->name('support.view');
    Route::post('support/message/send', [SupportController::class, 'send'])->name('support.send');

    // Stripe Checkout
    Route::get('stripe/checkout/{total}', [StripeController::class, 'createPaymentIntent'])->withoutMiddleware('planExists');
    Route::get('stripe/success', [StripeController::class, 'success'])->withoutMiddleware([VerifyCsrfToken::class, 'planExists']);

    Route::post('account', [AccountController::class, 'store']);
    Route::get('plan', [PlanController::class, 'index'])->name('plan.index');
    Route::get('plan/check', [PlanController::class, 'check']);
    Route::get('subscribe/{id}', [PlanController::class, 'subscribe'])->withoutMiddleware('planExists');
    Route::get('payment/history', [BillingController::class, 'index'])->name('payment.history');
    Route::get('select/payment/{id}', [BillingController::class, 'payment'])->withoutMiddleware('planExists');
    Route::get('payment/success', [BillingController::class, 'success']);
    Route::get('payment/failed', [BillingController::class, 'failed']);
    Route::get('payment/cancel', [BillingController::class, 'cancel']);
    Route::get('dropdown', function () {
        return Inertia::render('User/Dropdown');
    });
});


Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__ . '/auth.php';
