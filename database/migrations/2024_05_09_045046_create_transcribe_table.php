<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transcribe', function (Blueprint $table) {
            $table->id();
            $table->text('file_name')->nullable();
            $table->text('path')->nullable();
            $table->text('url')->nullable();
            $table->text('youtube_url')->nullable();
            $table->text('language')->nullable();
            $table->text('transcribe')->nullable();
            $table->text('transcription')->nullable();
            $table->text('webvtt')->nullable();
            $table->string('status');
            $table->timestamps();
           // $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transcribe');
    }
};
