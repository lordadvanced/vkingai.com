<?php

namespace Database\Seeders;

use App\Models\Getway;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GetwayTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $getways = array(
            array('id' => '1','name' => 'Paypal','logo' => '/uploads/getway/6J0SDMcKbi1DPGKqCOybETM3H3H0m2B97T3L30k9.png','rate' => '1','charge' => '2','namespace' => 'paypal','currency_name' => 'USD','image_accept' => '0','is_auto' => '1','test_mode' => '1','status' => 'approved','phone_required' => '0','instruction' => 'dsdsd','data' => '{"PAYPAL_CLIENT_ID":"AW6Ko7Noz0HMaueItbyLD3wndCrsaOds1bzEoMkQc7U5AiVksSMnAiqGm60hTtrpToJbGxiKdRE8oLiH"}','created_at' => '2023-01-26 15:21:40','updated_at' => '2023-08-02 18:16:05'),
            array('id' => '2','name' => 'Stripe','logo' => '/uploads/getway/60ZyXd7HtgIUHEF3VrSb675GHBM6eyOrcJIVT3Kv.png','rate' => '1','charge' => '2','namespace' => 'stripe','currency_name' => 'USD','image_accept' => '0','is_auto' => '1','test_mode' => '1','status' => 'approved','phone_required' => '0','instruction' => '','data' => '{"STRIPE_SECRET_KEY":"sk_test_51KXuCXBtOV9TPFO6ztUTqHED51TXePk2j1pIPWm8YPXByP1iUYmYUzh80fbVhBJh2PKXSEIuHGui3NlJBzS0kxq200eS5sxItI","STRIPE_PUBLISHABLE_KEY":"pk_test_51KXuCXBtOV9TPFO6vBVu3SKLCJlktpSgf8CMkBX81Q59YwDlDRFPIjF0mZMSqSAtKZrCH8OKvL0wzGMEEmW3jWFD00Io7POnDs"}','created_at' => '2023-01-26 15:21:40','updated_at' => '2023-07-31 18:32:28')
          );



        Getway::insert($getways);


    }
}
