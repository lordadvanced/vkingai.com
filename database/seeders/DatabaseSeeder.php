<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PlanTableSeeder::class,
            UserTableSeeder::class,
            GetwayTableSeeder::class,
            ManuTableSeeder::class,
            TemplateSeeder::class,
            OptionTableSeeder::class,
            DocumentTableSeeder::class
        ]);
    }
}
