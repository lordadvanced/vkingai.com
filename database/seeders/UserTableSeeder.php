<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array('id' => '2','user_type' => 'admin','plan_id' => '8','chatbot_id' => NULL,'social_id' => NULL,'name' => 'Admin Hossain','email' => 'admin@gmail.com','email_verified_at' => NULL,'password' => '$2y$10$/5jdr8yDeWzwqWPEd8pcm.z9zZwIMWx7m3REOW3hYgSFLff7gBGVG','profile' => '/profile/img/profile.png','data' => '{"word_limit":"200000","templates_limit":"60","chatbot_limit":"0","ai_templates":1,"ai_chatbot":1,"ai_images":1,"access_to_gpt3":1,"accept_to_gpt4":0,"ai_speech_to_text":1,"ai_code":1,"image_limit":"0","speech_to_text_limit":"5","audio_file_size":"2","trail_days":0,"accept_trail":0,"use_word_limit":0,"use_image_limit":0,"use_speech_to_text_limit":0}','status' => 'active','will_expire' => '2023-11-24 00:02:16','remember_token' => 'cLU743qHtpa2dbn7E6ob3MKMMlt0KeWuDME5yDqcgmn7obXqk427cQOjP4ON','created_at' => '2023-07-24 04:20:20','updated_at' => '2023-09-15 00:03:10')
          );


        User::insert($users);
    }
}
