<?php

namespace Database\Seeders;

use App\Models\Menu;
use App\Models\Menuitems;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ManuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menus = array(
            array('id' => '1','name' => 'Header','position' => 'header','lang' => 'en','status' => 'approved','created_at' => '2023-01-20 19:29:03','updated_at' => '2023-01-20 19:29:03'),
            array('id' => '2','name' => 'Use Cases','position' => 'footer_first','lang' => 'en','status' => 'approved','created_at' => '2023-01-21 14:39:17','updated_at' => '2023-09-22 03:57:14'),
            array('id' => '3','name' => 'Quick Links','position' => 'footer_second','lang' => 'en','status' => 'approved','created_at' => '2023-01-21 14:39:44','updated_at' => '2023-10-24 11:59:27'),
            array('id' => '4','name' => 'Need Help?','position' => 'footer_third','lang' => 'en','status' => 'approved','created_at' => '2023-01-21 14:40:08','updated_at' => '2023-09-22 04:02:00')
          );

        Menu::insert($menus);


        $menuitems = array(
            array('id' => '24','menu_id' => '2','parent_id' => NULL,'order' => '100','name' => 'Blog Ideas & Outline','data' => '{"url":"\\/user\\/templates\\/blog-ideas-outline","target":"_self"}','created_at' => '2023-01-21 14:42:23','updated_at' => '2023-10-24 11:56:27'),
            array('id' => '25','menu_id' => '2','parent_id' => NULL,'order' => '100','name' => 'Google Ads Description','data' => '{"url":"\\/user\\/templates\\/google-ads-descriptionc4Nt7DKaMo","target":"_self"}','created_at' => '2023-01-21 14:43:05','updated_at' => '2023-10-24 11:56:59'),
            array('id' => '26','menu_id' => '2','parent_id' => NULL,'order' => '100','name' => 'Blog Writing Content','data' => '{"url":"\\/user\\/templates\\/article-write","target":"_self"}','created_at' => '2023-01-21 14:44:18','updated_at' => '2023-10-24 11:57:37'),
            array('id' => '27','menu_id' => '2','parent_id' => NULL,'order' => '100','name' => 'Cover Letter Write','data' => '{"url":"\\/user\\/templates\\/cover-letter-generator","target":"_self"}','created_at' => '2023-01-21 14:45:00','updated_at' => '2023-10-24 11:57:56'),
            array('id' => '28','menu_id' => '3','parent_id' => NULL,'order' => '1','name' => 'Home','data' => '{"url":"\\/","target":"_self"}','created_at' => '2023-01-21 14:45:36','updated_at' => '2023-10-24 12:01:07'),
            array('id' => '29','menu_id' => '3','parent_id' => NULL,'order' => '2','name' => 'Blogs','data' => '{"url":"\\/blogs","target":"_self"}','created_at' => '2023-01-21 14:45:47','updated_at' => '2023-10-24 11:59:53'),
            array('id' => '30','menu_id' => '3','parent_id' => NULL,'order' => '4','name' => 'Ai Image ','data' => '{"url":"\\/user\\/ai\\/images","target":"_self"}','created_at' => '2023-01-21 14:45:57','updated_at' => '2023-10-24 12:00:26'),
            array('id' => '31','menu_id' => '3','parent_id' => NULL,'order' => '5','name' => 'Speech To Text','data' => '{"url":"\\/user\\/ai\\/speech\\/text","target":"_self"}','created_at' => '2023-01-21 14:46:09','updated_at' => '2023-10-24 12:00:50'),
            array('id' => '32','menu_id' => '4','parent_id' => NULL,'order' => '4','name' => 'Contact Us','data' => '{"url":"\\/contact","target":"_self"}','created_at' => '2023-01-21 14:46:41','updated_at' => '2023-07-30 10:01:43'),
            array('id' => '33','menu_id' => '4','parent_id' => NULL,'order' => '1','name' => 'Register','data' => '{"url":"\\/register","target":"_self"}','created_at' => '2023-01-21 14:46:57','updated_at' => '2023-01-21 14:47:37'),
            array('id' => '34','menu_id' => '4','parent_id' => NULL,'order' => '2','name' => 'Login','data' => '{"url":"\\/login","target":"_self"}','created_at' => '2023-01-21 14:47:04','updated_at' => '2023-01-21 14:47:37'),
            array('id' => '35','menu_id' => '4','parent_id' => NULL,'order' => '3','name' => 'Pricing Plans','data' => '{"url":"\\/pricing","target":"_self"}','created_at' => '2023-01-21 14:47:28','updated_at' => '2023-07-30 10:01:43'),
            array('id' => '40','menu_id' => '1','parent_id' => NULL,'order' => '1','name' => 'Home','data' => '{"url":"\\/","target":"_self"}','created_at' => '2023-07-26 13:25:26','updated_at' => '2023-10-21 19:38:44'),
            array('id' => '41','menu_id' => '1','parent_id' => NULL,'order' => '2','name' => 'Templates','data' => '{"url":"\\/templates","target":"_self"}','created_at' => '2023-07-26 13:25:37','updated_at' => '2023-10-30 12:13:36'),
            array('id' => '42','menu_id' => '1','parent_id' => NULL,'order' => '3','name' => 'Pricing','data' => '{"url":"\\/pricing","target":"_self"}','created_at' => '2023-07-26 13:25:49','updated_at' => '2023-07-26 13:34:37'),
            array('id' => '43','menu_id' => '1','parent_id' => NULL,'order' => '4','name' => 'Blogs','data' => '{"url":"\\/blogs","target":"_self"}','created_at' => '2023-07-26 13:26:01','updated_at' => '2023-07-26 13:34:37'),
            array('id' => '44','menu_id' => '1','parent_id' => NULL,'order' => '5','name' => 'Contact Us','data' => '{"url":"\\/contact","target":"_self"}','created_at' => '2023-07-26 13:26:18','updated_at' => '2023-07-26 13:34:37'),
            array('id' => '45','menu_id' => '2','parent_id' => NULL,'order' => '100','name' => 'Company Name Generator','data' => '{"url":"\\/user\\/templates\\/company-name-generator","target":"_self"}','created_at' => '2023-09-22 03:58:19','updated_at' => '2023-10-24 11:58:50'),
            array('id' => '46','menu_id' => '3','parent_id' => NULL,'order' => '3','name' => 'Pricing Plans','data' => '{"url":"\\/pricing","target":"_self"}','created_at' => '2023-09-22 04:01:31','updated_at' => '2023-10-24 11:59:53')
          );

        Menuitems::insert($menuitems);

    }
}
