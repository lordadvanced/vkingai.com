<?php

namespace Database\Seeders;

use App\Models\Plan;
use App\Models\SuggestedMsg;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plans = array(
            array('id' => '3','name' => 'Free Plan','slug' => 'free-plan','price' => '0','duration_type' => 'monthly','data' => '{"word_limit":"5000","templates_limit":"37","chatbot_limit":"3","ai_templates":1,"ai_chatbot":1,"ai_images":1,"access_to_gpt3":1,"accept_to_gpt4":0,"ai_speech_to_text":1,"ai_code":1,"image_limit":"10","short_content":"For Early Startup to Start","speech_to_text_limit":"2","audio_file_size":"2","is_recommended":0,"trail_days":"14"}','status' => 'approved','accept_trail' => '1','created_at' => '2023-07-27 14:32:17','updated_at' => '2023-10-06 08:22:41'),
            array('id' => '8','name' => 'Medium Plan','slug' => 'medium-plan','price' => '14.99','duration_type' => 'monthly','data' => '{"word_limit":"30000","templates_limit":"70","chatbot_limit":"10","ai_templates":1,"ai_chatbot":1,"ai_images":1,"access_to_gpt3":1,"accept_to_gpt4":0,"ai_speech_to_text":1,"ai_code":1,"image_limit":"100","short_content":"For Medium level Startup Founder","speech_to_text_limit":"20","audio_file_size":"4","is_recommended":false,"trail_days":0}','status' => 'approved','accept_trail' => '0','created_at' => '2023-09-13 18:15:48','updated_at' => '2023-10-06 08:27:51'),
            array('id' => '10','name' => 'Premium Plan','slug' => 'premium-plan','price' => '38.99','duration_type' => 'monthly','data' => '{"word_limit":"50000","ai_templates":1,"templates_limit":"-1","ai_chatbot":1,"chatbot_limit":"-1","ai_images":1,"image_limit":"200","ai_speech_to_text":1,"speech_to_text_limit":"30","audio_file_size":"10","ai_code":1,"short_content":"For Premium Plan for Startup","is_recommended":0,"trail_days":0,"access_to_gpt3":1,"accept_to_gpt4":0}','status' => 'approved','accept_trail' => '0','created_at' => '2023-10-06 08:28:59','updated_at' => '2023-10-06 08:28:59'),
            array('id' => '11','name' => 'Free Plan','slug' => 'free-plannjhz4XlIdy','price' => '99.99','duration_type' => 'yearly','data' => '{"word_limit":"10000","ai_templates":1,"templates_limit":"40","ai_chatbot":1,"chatbot_limit":"6","ai_images":1,"image_limit":"50","ai_speech_to_text":1,"speech_to_text_limit":"10","audio_file_size":"2","ai_code":1,"short_content":"For Early Startup to Start","is_recommended":0,"trail_days":0,"access_to_gpt3":1,"accept_to_gpt4":0}','status' => 'approved','accept_trail' => '0','created_at' => '2023-10-06 08:32:47','updated_at' => '2023-10-06 08:32:47'),
            array('id' => '12','name' => 'Medium Plan','slug' => 'medium-planAIOP7JrXX1','price' => '149.99','duration_type' => 'yearly','data' => '{"word_limit":"30000","ai_templates":1,"templates_limit":"70","ai_chatbot":1,"chatbot_limit":"12","ai_images":1,"image_limit":"100","ai_speech_to_text":1,"speech_to_text_limit":"10","audio_file_size":"4","ai_code":1,"short_content":"For Medium level Startup Founder","is_recommended":0,"trail_days":0,"access_to_gpt3":1,"accept_to_gpt4":0}','status' => 'approved','accept_trail' => '0','created_at' => '2023-10-06 08:34:02','updated_at' => '2023-10-06 08:34:02'),
            array('id' => '13','name' => 'Premium Plan','slug' => 'premium-planbhKoYdPoU2','price' => '349.99','duration_type' => 'yearly','data' => '{"word_limit":"-1","ai_templates":1,"templates_limit":"-1","ai_chatbot":1,"chatbot_limit":"-1","ai_images":1,"image_limit":"200","ai_speech_to_text":1,"speech_to_text_limit":"20","audio_file_size":"10","ai_code":1,"short_content":"For Premium Plan for Startup","is_recommended":0,"trail_days":0,"access_to_gpt3":1,"accept_to_gpt4":0}','status' => 'approved','accept_trail' => '0','created_at' => '2023-10-06 08:34:47','updated_at' => '2023-10-06 08:34:47')
          );


        Plan::insert($plans);
    }
}
